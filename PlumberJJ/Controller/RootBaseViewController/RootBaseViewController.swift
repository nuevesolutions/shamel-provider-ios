//
//  RootBaseViewController.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 10/27/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit
import AVFoundation
import GradientCircularProgress

func isConnectedToNetwork() -> Bool {
    return (UIApplication.shared.delegate as! AppDelegate).IsInternetconnected
}

class RootBaseViewController: UIViewController {
    let progress = GradientCircularProgress()
    var spinnerViews : MMMaterialDesignSpinner?
    var spinnerView : MMMaterialDesignSpinner?
    var Appdel=UIApplication.shared.delegate as! AppDelegate
    
    var reachability : Reachability?
    var window: UIWindow?
    var theme:Theme=Theme()
    var url_handler : URLhandler = URLhandler()
    var sidebarMenuOpen:Bool=Bool()
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        if isConnectedToNetwork() == false {
            if((self.view.window) != nil){
                let image = UIImage(named: "NoNetworkConn")
                self.view.makeToast(message:kErrorMsg, duration: 3, position:HRToastActivityPositionDefault as AnyObject, title: "Oops !!!!", image: image!)
            }
        }
        else {
            if(theme.isUserLigin()){
                let URL_Handler:URLhandler=URLhandler()
                let objUserRecs:UserInfoRecord=theme.GetUserDetails()
                let param=["user_type":"provider","id":"\(objUserRecs.providerId)","mode":"available"]
                URL_Handler.makeCall(UserAvailableUrl, param: param as NSDictionary, completionHandler: { (responseObject, error) -> () in
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    if(appDelegate.xmppStream?.isDisconnected())!
                    {
                        //appDelegate.setXmppConnect()
                        
                    }
                })
            }
            
        }
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        //        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("applicationLanguageChangeNotification:"), name: Language_Notification as String, object: nil)
        
        
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "didClickLogOutPostNotif"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kNoNetwork), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kPaymentPaidNotif), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ReceiveChatToRootView"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ReceivePushChatToRootView"), object: nil)
        
        
        NotificationCenter.default.removeObserver(self, name:NSNotification.Name(rawValue: "logout"), object: nil)
        
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kPushNotification), object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(RootBaseViewController.methodOfReceivedNotificationForLogOut(_:)), name:NSNotification.Name(rawValue: "didClickLogOutPostNotif"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RootBaseViewController.methodOfReceivedNotificationNetworkNet(_:)), name:NSNotification.Name(rawValue: kNoNetwork), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RootBaseViewController.methodOfReceivedNotificationNetworkKKKK(_:)), name:NSNotification.Name(rawValue: kPaymentPaidNotif), object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(RootBaseViewController.methodOfReceivedMessageNotification(_:)), name:NSNotification.Name(rawValue: "ReceiveChatToRootView"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RootBaseViewController.methodOfReceivedPushMessageNotification(_:)), name:NSNotification.Name(rawValue: "ReceivePushChatToRootView"), object: nil)
        
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(RootBaseViewController.methodOfReceivedPushNotification(_:)), name:NSNotification.Name(rawValue: kPushNotification), object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(RootBaseViewController.logoutmethod(_:)), name:NSNotification.Name(rawValue: "logout"), object: nil)
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(RootBaseViewController.applicationLanguageChangeNotification(_:)), name: NSNotification.Name(rawValue: Language_Notification as String as String), object: nil)
        
        //theme.saveLanguage("en")
        //theme.SetLanguageToApp()
        
    }
    
    func applicationLanguageChangeNotification(_ notification : Notification){
        
        
    }
    
    func logoutmethod(_ notify:Notification){
        
        
       self.theme.UpdateAvailability("0")
        
                
                let objUserRecs:UserInfoRecord=self.theme.GetUserDetails()
                let providerid = objUserRecs.providerId
                
                
                
                let Param: Dictionary = ["provider_id":"\(providerid as String)","device_type":"ios"]
        self.url_handler.makeCall(Logout_url, param: Param as NSDictionary) { (responseObject, error) -> () in
                    self.DismissProgress()
                    
                    if(error != nil)
                    {
                        self.view.makeToast(message:"Network Failure", duration: 4, position: HRToastPositionDefault as AnyObject, title: "")
                        
                    }
                        
                    else
                    {
                        print("the response is \(String(describing: responseObject))")
                        if(responseObject != nil)
                        {
                            
                            
                            let responseObject = responseObject!
                            let status=self.theme.CheckNullValue(responseObject.object(forKey: "status"))
                            
                            if(status! == "0")
                            {
                                
                                
                            }
                            else
                            {
                                
                                
                            }
                            
                            SocketIOManager.sharedInstance.RemoveAllListener()
                            SocketIOManager.sharedInstance.LeaveRoom(providerid as String)
                            SocketIOManager.sharedInstance.LeaveChatRoom(providerid as String)
                            let dic: NSDictionary = NSDictionary()
                            self.theme .saveUserDetail(dic)
                            
                            //                        let loginController = self.storyboard!.instantiateViewControllerWithIdentifier("InitialVCSID") as! LoginViewController
                            //                        let navig: UINavigationController = UINavigationController.init(rootViewController: loginController)
                            //                        self.Appdel.window?.rootViewController = navig
                            //
                            //                        loginController.navigationController?.setNavigationBarHidden(true, animated: true)
                            
                            let loginController = UIStoryboard.init(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "InitialVCSID") as! InitialViewController
                            //or the homeController
                            let navController = UINavigationController(rootViewController: loginController)
                            self.Appdel.window!.rootViewController! = navController
                            loginController.navigationController!.setNavigationBarHidden(true, animated: true)
                            
                            
                            
                            
                            
                        }
                        else
                        {
                            self.view.makeToast(message: "Please try again", duration:3, position:HRToastPositionDefault as AnyObject, title: "\(appNameJJ)")
                        }
                        
                    }
                    
                }
                

        
 
        
        
        
    }
    func methodOfReceivedPushNotification(_ notification: Notification)  {
        self.progress.dismiss()
        if((self.view.window) != nil){
            // self.view.endEditing(true)
            if(self.frostedViewController==nil){
                
            }else{
                self.frostedViewController.view.endEditing(true)
                // Present the view controller
                //
                self.frostedViewController.hideMenuViewController()
            }
            
            
            let userInfo:Dictionary<String,String?> = notification.userInfo as! Dictionary<String,String?>
            
            let Action = userInfo["action"]!
            _ = userInfo["action"]!
            let orderid = userInfo ["key"]!
         
            
            if(Action=="job_request") || Action=="Accept_task" || Action=="Left_job" || Action ==  "rejecting_task" {
                if let activeController = navigationController?.visibleViewController {
                    if activeController.isKind(of: MyLeadsViewController.self){
                        
                        
                        let jobId=orderid
                        let objMyOrderVc = self.storyboard!.instantiateViewController(withIdentifier: "MyOrderDetailOpenVCSID") as! MyOrderOpenDetailViewController
                        objMyOrderVc.jobID=jobId!
                        objMyOrderVc.Getorderstatus = ""
                        self.navigationController!.pushViewController(withFlip: objMyOrderVc, animated: true)
                        
                        
                        
                        
                    }else{
                        
                        
                        
                        let jobId=orderid
                        let objMyOrderVc = self.storyboard!.instantiateViewController(withIdentifier: "MyOrderDetailOpenVCSID") as! MyOrderOpenDetailViewController
                        objMyOrderVc.jobID=jobId!
                        objMyOrderVc.Getorderstatus = ""
                        self.navigationController!.pushViewController(withFlip: objMyOrderVc, animated: true)
                        
                    }
                    
                }
            }
                
            else if(Action=="job_cancelled"){
                if let activeController = navigationController?.visibleViewController {
                    if activeController.isKind(of: MyLeadsViewController.self){
                        
                        
                        let jobId=orderid
                        let objMyOrderVc = self.storyboard!.instantiateViewController(withIdentifier: "MyOrderDetailOpenVCSID") as! MyOrderOpenDetailViewController
                        objMyOrderVc.jobID=jobId!
                        objMyOrderVc.Getorderstatus = ""
                        self.navigationController!.pushViewController(withFlip: objMyOrderVc, animated: true)
                        
                        
                        
                        
                    }else{
                        
                        
                        
                        let jobId=orderid
                        let objMyOrderVc = self.storyboard!.instantiateViewController(withIdentifier: "MyOrderDetailOpenVCSID") as! MyOrderOpenDetailViewController
                        objMyOrderVc.jobID=jobId!
                        objMyOrderVc.Getorderstatus = ""
                        self.navigationController!.pushViewController(withFlip: objMyOrderVc, animated: true)
                        
                    }
                    
                }
                
                
            }
                
            else if(Action=="receive_cash")
            {
                let pricestr  = theme.CheckNullValue(userInfo ["price"]!)
                let currencystr  = theme.CheckNullValue(userInfo["currency"]!)
                let jobId=orderid
                let price = pricestr
                let currency=currencystr
                let priceStr="\(currency) \(price)"
                let objReceiveCashvc = self.storyboard!.instantiateViewController(withIdentifier: "ReceiveCashVCSID") as! ReceiveCashViewController
                objReceiveCashvc.priceString=priceStr
                objReceiveCashvc.jobIDStr=jobId!
                self.navigationController!.pushViewController(withFlip: objReceiveCashvc, animated: true)
            }
                
            else if(Action=="payment_paid")
            {
                
                let jobId=orderid
                let objRatingsvc = self.storyboard!.instantiateViewController(withIdentifier: "RatingsVCSID") as! RatingsViewController
               
                objRatingsvc.jobIDStr=jobId!
                self.navigationController!.pushViewController(withFlip: objRatingsvc, animated: true)
                
                
                
            }
            
        }
    }
    
    
    func methodOfReceivedPushMessageNotification(_ notification: Notification){

        
        self.progress.dismiss()
        if((self.view.window) != nil){
            // self.view.endEditing(true)
            if(self.frostedViewController==nil){
                
            }else{
                self.frostedViewController.view.endEditing(true)
                // Present the view controller
                //
                self.frostedViewController.hideMenuViewController()
            }
            
            let userInfo:Dictionary<String,String> = notification.userInfo as! Dictionary<String,String>
            // or as! Sting or as! Int
            
            let check_userid = userInfo["from"]
            let Chatmessage = userInfo["message"]
            let taskid = userInfo["task"]
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            
            let providerid : String = objUserRecs.providerId
            
            
            
            if (check_userid == providerid)
            {
                
            }
            else
            {
                if let activeController = navigationController?.visibleViewController {
                    if activeController.isKind(of: MessageViewController.self){
                        
                        
                        
                    }else{
                        

                        let objChatVc = self.storyboard!.instantiateViewController(withIdentifier: "ChatVCSID") as! MessageViewController
                        
                        objChatVc.Userid = check_userid
                        objChatVc.RequiredJobid = taskid
                        self.navigationController!.pushViewController(withFlip: objChatVc, animated: true)
                        
                        
                        
                    }
                    
                }
            }
            
            
            
            
        }
        
    }
    
    func methodOfReceivedMessageNotification(_ notification: Notification){
        self.progress.dismiss()
        if((self.view.window) != nil){
            // self.view.endEditing(true)
            if(self.frostedViewController==nil){
                
            }else{
                self.frostedViewController.view.endEditing(true)
                // Present the view controller
                //
                self.frostedViewController.hideMenuViewController()
            }
            
            let userInfo:Dictionary<String,String> = notification.userInfo as! Dictionary<String,String>
            // or as! Sting or as! Int
            
            
            
            let check_userid = userInfo["from"]
            let Chatmessage = userInfo["message"]
            let taskid = userInfo["task"]
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            
            let providerid : String = objUserRecs.providerId
            
            
            
            if (check_userid == providerid)
            {
                
            }
            else
            {
                if let activeController = navigationController?.visibleViewController {
                    if activeController.isKind(of: MessageViewController.self){
                        
                        
                        
                    }else{
                      let alertView = UNAlertView(title: appNameJJ, message:Language_handler.VJLocalizedString("message_from_user", comment: nil))
                        alertView.addButton(self.theme.setLang("ok"), action: {
                            
                            if activeController.isKind(of: TaskerProfileViewController.self){
                                let myViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController (withIdentifier: "ChatVCSID") as? MessageViewController
                                myViewController!.Userid = check_userid
                                myViewController!.RequiredJobid = taskid
                                self.navigationController!.pushViewController(withFlip: myViewController!, animated: true)
                                
                            }
                            else
                            {
                            
                            let objChatVc = self.storyboard!.instantiateViewController(withIdentifier: "ChatVCSID") as! MessageViewController
                            
                            objChatVc.Userid = check_userid
                                objChatVc.RequiredJobid = taskid
                            self.navigationController!.pushViewController(withFlip: objChatVc, animated: true)
                            }
                            
                        })
                        AudioServicesPlayAlertSound(1315);

                        alertView.show()
                        
                    }
                    
                }
            }
            
            
            
            
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "didClickLogOutPostNotif"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kNoNetwork), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kPaymentPaidNotif), object: nil)
        
        NotificationCenter.default.removeObserver(self);
        
        
    }
    
    
    func methodOfReceivedNotificationForLogOut(_ notification: Notification){
        self.progress.dismiss()
        let objDict:NSDictionary=NSDictionary()
        theme.saveUserDetail(objDict)
        let objInitialVc = self.storyboard!.instantiateViewController(withIdentifier: "InitialVCSID") as! InitialViewController
        self.navigationController!.pushViewController(withFlip: objInitialVc, animated: true)
        
    }
    func showProgress()
    {
        if spinnerView == nil {
            spinnerView = MMMaterialDesignSpinner(frame: CGRect.zero)
            
        }
        
        
        
        spinnerViews = spinnerView
        self.spinnerViews!.bounds = CGRect(x: 0, y: 0, width: 75, height: 75)
        self.spinnerViews!.tintColor = PlumberThemeColor
        
        if((self is JobsClosedViewController)||(self is MyOrderViewController)||(self is JobsCancelledViewController)||(self is MyJobsViewController)||(self is MyLeadsViewController)||(self is MissedLeadsViewController)||(self is BarChartViewController)||(self is PieChartViewController)||(self is NewLeadsViewController)){
            self.spinnerViews!.center = CGPoint(x: (self.view.bounds).midX,y: (self.view.bounds).midY-107)
        }else{
            self.spinnerViews!.center = CGPoint(x: (self.view.bounds).midX, y: (self.view.bounds).midY)
        }
        
        
        
        print("display spinner frame=\(self.spinnerView?.frame)" )
        self.spinnerViews!.translatesAutoresizingMaskIntoConstraints = false
        self.view!.addSubview(self.spinnerViews!)
        
        self.spinnerView?.startAnimating();
        
    }
    func DismissProgress()
    {
        self.spinnerView?.stopAnimating();
        
        
    }
    func methodOfReceivedNotificationNetworkNet(_ notification: Notification){
        
        self.progress.dismiss()
        self.progress.dismiss()
        self.progress.dismiss()
    }
    
    func methodOfReceivedNotificationNetworkKKKK(_ notification: Notification){
        self.progress.dismiss()
        if((self.view.window) != nil){
            // self.view.endEditing(true)
            if(self.frostedViewController==nil){
                
            }else{
                self.frostedViewController.view.endEditing(true)
                // Present the view controller
                //
                self.frostedViewController.hideMenuViewController()
            }
            
            guard let url = notification.object else {
                return // or throw
            }
            
            let blob = url as! NSDictionary // or as! Sting or as! Int
            if(blob.count>0){
                
                var dictPartner:NSMutableDictionary = NSMutableDictionary()
                dictPartner = blob.object(forKey: "message") as! NSMutableDictionary
                
                let Action:NSString?=dictPartner.object(forKey: "action") as? NSString
                
                if Action=="job_request" || Action=="Accept_task" || Action=="Left_job" {
                    if let activeController = navigationController?.visibleViewController {
                        if activeController.isKind(of: MyLeadsViewController.self){
                            
                            let alertView = UNAlertView(title: appNameJJ, message:(dictPartner.object(forKey: "message") as? NSString)! as String)
                            alertView.addButton(self.theme.setLang("ok"), action: {
                                
                              
                                let jobId=dictPartner.object(forKey: "key0") as! String
                                let objMyOrderVc = self.storyboard!.instantiateViewController(withIdentifier: "MyOrderDetailOpenVCSID") as! MyOrderOpenDetailViewController
                                objMyOrderVc.jobID=jobId
                                objMyOrderVc.Getorderstatus = ""
                                self.navigationController!.pushViewController(withFlip: objMyOrderVc, animated: true)
                                
                            })
                            AudioServicesPlayAlertSound(1315);

                            alertView.show()
                            
                            
                        }else{
                            let alertView = UNAlertView(title: appNameJJ, message:(dictPartner.object(forKey: "message") as? NSString)! as String)
                            alertView.addButton(self.theme.setLang("ok"), action: {
                                
                                if activeController.isKind(of: TaskerProfileViewController.self){
                                    
                                    let jobId=dictPartner.object(forKey: "key0") as! String
                                    let objMyOrderVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController (withIdentifier: "MyOrderDetailOpenVCSID") as? MyOrderOpenDetailViewController
                                    objMyOrderVc!.jobID=jobId
                                    objMyOrderVc!.Getorderstatus = ""
                                    self.navigationController!.pushViewController(withFlip: objMyOrderVc!, animated: true)
                                    
                                }
                                else
                                {
                                let jobId=dictPartner.object(forKey: "key0") as! String
                                let objMyOrderVc = self.storyboard!.instantiateViewController(withIdentifier: "MyOrderDetailOpenVCSID") as! MyOrderOpenDetailViewController
                                objMyOrderVc.jobID=jobId
                                objMyOrderVc.Getorderstatus = ""
                                self.navigationController!.pushViewController(withFlip: objMyOrderVc, animated: true)
                                }
                            })
                            AudioServicesPlayAlertSound(1315);

                            alertView.show()
                            
                        }
                        
                    }
                }
                else if(Action=="admin_notification")
                {
                    let alertView = UNAlertView(title: appNameJJ, message:(dictPartner.object(forKey: "message") as? NSString)! as String)
                    alertView.addButton(self.theme.setLang("ok"), action: {
                        
                    })
                    AudioServicesPlayAlertSound(1315);

                    alertView.show()
                    
                }
                    

                else if(Action=="job_cancelled"){
                   let alertView = UNAlertView(title: appNameJJ, message:Language_handler.VJLocalizedString("cancel_by_user", comment: nil))
                    alertView.addButton(self.theme.setLang("ok"), action: {
                        
                        
                        if let activeController = self.navigationController?.visibleViewController {
                            if activeController.isKind(of: MyOrderOpenDetailViewController.self){
                                
                                NotificationCenter.default.post(name: Notification.Name(rawValue: kJobCancelNotif), object: nil)
                            }
                            else if activeController.isKind(of: TaskerProfileViewController.self){
                                
                                let jobId=dictPartner.object(forKey: "key0") as! String
                                let objMyOrderVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController (withIdentifier: "MyOrderDetailOpenVCSID") as? MyOrderOpenDetailViewController

                                objMyOrderVc!.jobID=jobId
                                objMyOrderVc!.Getorderstatus = ""
                                self.navigationController!.pushViewController(withFlip: objMyOrderVc!, animated: true)
                              
                            }
                            else{
                                let jobId=dictPartner.object(forKey: "key0") as! String
                                let objMyOrderVc = self.storyboard!.instantiateViewController(withIdentifier: "MyOrderDetailOpenVCSID") as! MyOrderOpenDetailViewController
                                objMyOrderVc.jobID=jobId
                                objMyOrderVc.Getorderstatus = ""
                                self.navigationController!.pushViewController(withFlip: objMyOrderVc, animated: true)
                            }
                        }
                    })
                    AudioServicesPlayAlertSound(1315);

                    alertView.show()
                }
                else if(Action=="receive_cash")
                {
                       if let activeController = self.navigationController?.visibleViewController {
                    if activeController.isKind(of: TaskerProfileViewController.self){
                        
                        let jobId=dictPartner.object(forKey: "key1") as! String
                        let price=dictPartner.object(forKey: "key3") as! String
                        let currency=self.theme.getCurrencyCode(dictPartner.object(forKey: "key4") as! String)
                        let priceStr="\(currency) \(price)"
                        let objReceiveCashvc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController (withIdentifier: "ReceiveCashVCSID") as? ReceiveCashViewController
                        objReceiveCashvc!.priceString=priceStr
                        objReceiveCashvc!.jobIDStr=jobId
                        self.navigationController!.pushViewController(withFlip: objReceiveCashvc!, animated: true)
                        
                    }
                    else{
                        
                    let jobId=dictPartner.object(forKey: "key1") as! String
                    let price=dictPartner.object(forKey: "key3") as! String
                    let currency=self.theme.getCurrencyCode(dictPartner.object(forKey: "key4") as! String)
                    let priceStr="\(currency) \(price)"
                    let objReceiveCashvc = self.storyboard!.instantiateViewController(withIdentifier: "ReceiveCashVCSID") as! ReceiveCashViewController
                    objReceiveCashvc.priceString=priceStr
                    objReceiveCashvc.jobIDStr=jobId
                    self.navigationController!.pushViewController(withFlip: objReceiveCashvc, animated: true)
                    }
                    }
                }
                else if(Action=="payment_paid")
                {
                    let alertView = UNAlertView(title: appNameJJ, message:(dictPartner.object(forKey: "message") as? NSString)! as String)
                    alertView.addButton(self.theme.setLang("ok"), action: {
                        
                        if let activeController = self.navigationController?.visibleViewController {
                            if activeController.isKind(of: TaskerProfileViewController.self){
                                
                                let jobId=dictPartner.object(forKey: "key0") as! String
                                let objRatingsvc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController (withIdentifier: "RatingsVCSID") as? RatingsViewController
                                //                        objReceiveCashvc.priceString=priceStr
                                objRatingsvc!.jobIDStr=jobId
                                self.navigationController!.pushViewController(withFlip: objRatingsvc!, animated: true)
                            }
                            
                            else{
                                let jobId=dictPartner.object(forKey: "key0") as! String
                        let objRatingsvc = self.storyboard!.instantiateViewController(withIdentifier: "RatingsVCSID") as! RatingsViewController
                        //                        objReceiveCashvc.priceString=priceStr
                        objRatingsvc.jobIDStr=jobId
                        self.navigationController!.pushViewController(withFlip: objRatingsvc, animated: true)
                            }
                        }
                        
                    })
                    AudioServicesPlayAlertSound(1315);

                    alertView.show()
                }
            
                else if(Action=="partially_paid")
                {
                    let alertView = UNAlertView(title: appNameJJ, message:(dictPartner.object(forKey: "message") as? NSString)! as String)
                    alertView.addButton(self.theme.setLang("ok"), action: {
                        
                        if let activeController = self.navigationController?.visibleViewController {
                            if activeController.isKind(of: TaskerProfileViewController.self){
                                
                                
                                let jobId=dictPartner.object(forKey: "key0") as! String
                                let objMyOrderVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController (withIdentifier: "MyOrderDetailOpenVCSID") as? MyOrderOpenDetailViewController
                                objMyOrderVc!.jobID=jobId
                                objMyOrderVc!.Getorderstatus = ""
                                self.navigationController!.pushViewController(withFlip: objMyOrderVc!, animated: true)
                            }
                            else{

                        
                            let jobId=dictPartner.object(forKey: "key0") as! String
                            let objMyOrderVc = self.storyboard!.instantiateViewController(withIdentifier: "MyOrderDetailOpenVCSID") as! MyOrderOpenDetailViewController
                            objMyOrderVc.jobID=jobId
                            objMyOrderVc.Getorderstatus = ""
                            self.navigationController!.pushViewController(withFlip: objMyOrderVc, animated: true)
                            }
                        }
                        
                    })
                    AudioServicesPlayAlertSound(1315);
                    
                    alertView.show()
                }

                else if(Action=="job_assigned"){
                   let alertView = UNAlertView(title:appNameJJ, message: Language_handler.VJLocalizedString("assigned_to_job", comment: nil))
                    alertView.addButton(self.theme.setLang("ok"), action: {
                        
                    })
                    alertView.addButton("View", action: {
                        if let activeController = self.navigationController?.visibleViewController {
                            if activeController.isKind(of: MyOrderOpenDetailViewController.self){
                                
                                NotificationCenter.default.post(name: Notification.Name(rawValue: kJobCancelNotif), object: nil)
                            }else{
                                
                                    if activeController.isKind(of: TaskerProfileViewController.self){
                                        let jobId=dictPartner.object(forKey: "key1") as! String
                                        let objMyOrderVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController (withIdentifier: "MyOrderDetailOpenVCSID") as? MyOrderOpenDetailViewController
                                        objMyOrderVc!.jobID=jobId
                                        objMyOrderVc!.Getorderstatus = ""
                                        self.navigationController!.pushViewController(withFlip: objMyOrderVc!, animated: true)
                                    }
                                    else{
                                let jobId=dictPartner.object(forKey: "key1") as! String
                                let objMyOrderVc = self.storyboard!.instantiateViewController(withIdentifier: "MyOrderDetailOpenVCSID") as! MyOrderOpenDetailViewController
                                objMyOrderVc.jobID=jobId
                                objMyOrderVc.Getorderstatus = ""
                                self.navigationController!.pushViewController(withFlip: objMyOrderVc, animated: true)
                                    }
                                    
                            }
                        }
                    })
                    AudioServicesPlayAlertSound(1315);

                    alertView.show()
                }
            }
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    deinit {
        
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: - Custom UISwitch
    
    func switchTransform(isSwitch: UISwitch)
    {
        isSwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        
    }
    
    
   
    
}

extension UITextField {
    func isMandatory(){
        let label = UILabel()
        label.frame = CGRect(x: 0, y: 0, width: 10,height: self.frame.height)
        label.text = "*"
        label.textColor = UIColor.red
        self.rightView = label
        self.rightViewMode = UITextFieldViewMode .always
    }
}


