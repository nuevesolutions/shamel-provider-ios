//
//  EditProfileViewController.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 11/25/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit
import MobileCoreServices
import AssetsLibrary
import Foundation
import WDImagePicker
import GradientCircularProgress
import Alamofire

extension NSMutableData {
    
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
        
        
    }
}

class EditProfileViewController: RootBaseViewController,UIActionSheetDelegate,UITextViewDelegate,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITableViewDelegate,UITableViewDataSource,WDImagePickerDelegate,UIScrollViewDelegate {
    @IBOutlet var Placesearch_tableview: UITableView!
    
    @IBOutlet var Location_tableview: UITableView!
    @IBOutlet var place_searchview: UIView!
    @IBOutlet var editAvailabilitytableview: UITableView!
    @IBOutlet var working_loca: UITextField!
    @IBOutlet weak var bannerImg: UIImageView!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var editProfileScrollView: UIScrollView!
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var phoneTxtField: UITextField!
    @IBOutlet weak var addressTxtField: UITextField!
    @IBOutlet weak var cCodeTxtField: UITextField!
    let progressbar = GradientCircularProgress()
    @IBOutlet weak var countryTxtField: UITextField!
    @IBOutlet weak var postalCodeTxtField: UITextField!
    @IBOutlet weak var stateTxtField: UITextField!
    @IBOutlet weak var cityTxtField: UITextField!
    @IBOutlet weak var lastView: UIView!
    @IBOutlet var category_TblView: UITableView!
    @IBOutlet weak var category_View: UIView!
    var selectedBtn = NSMutableArray()
    var getTextfieldtag : NSString = NSString()
    var availabilityStorage:NSArray!
    var category_Array:NSMutableArray = NSMutableArray()
    var editCatDtls_Array:NSMutableArray = NSMutableArray()
    var normalimagePicker = UIImagePickerController()
    var imagePicker = WDImagePicker()
    
    @IBOutlet weak var lblMyProfile: UILabel!
    @IBOutlet weak var btnUpdate1: UIButton!
    
    @IBOutlet weak var btnUpdate3: UIButton!
    
    @IBOutlet weak var btnUpdate2: UIButton!
    @IBOutlet weak var btnUpdate4: UIButton!
    
    @IBOutlet weak var btnUpdate5: UIButton!
    
    @IBOutlet weak var btnUpdate6: UIButton!
    
    @IBOutlet  var worklbl: UILabel!
    @IBOutlet  var categorylbl: UIButton!
    @IBOutlet  var avail: UILabel!
    
    
    @IBOutlet weak var lblEmail: SMIconLabel!
    
    @IBOutlet weak var lblMobile: SMIconLabel!
    
    @IBOutlet weak var lblAddress: SMIconLabel!
    @IBOutlet weak var lbluserName: SMIconLabel!
    
    @IBOutlet weak var lblRadius: SMIconLabel!
    
    var availInc = Int()
    
    @IBOutlet var usernameTxtfield: UITextField!
    var weekDay = ["Days","Sun","Mon","Tue","Wed","Thu","Fri","Sat"]
    var weekFullDay = ["Days","Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]
    var weakTitle = ["Morning","Afternoon","Evening"]
    
    @IBOutlet var radiusTxtfield: UITextField!
    var tField: UITextField!
    var otpStr : String = String()
    var otp_status : String = String()
    var imagedata : Data = Data()
    var Contact:String=String()
    var Addaddress_Data : Addaddress = Addaddress()
    var categoryArray: NSMutableArray = NSMutableArray()
    
    enum PlaceType: CustomStringConvertible {
        case all
        case geocode
        case address
        case establishment
        case regions
        case cities
        
        var description : String {
            switch self {
            case .all: return ""
            case .geocode: return "geocode"
            case .address: return "address"
            case .establishment: return "establishment"
            case .regions: return "regions"
            case .cities: return "cities"
            }
        }
    }
    
    struct Place {
        let id: String
        let description: String
    }
    
    var places = [Place]()
    var setstatus:Bool=Bool()
    
    var placeType: PlaceType = .all
    
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        normalimagePicker = UIImagePickerController()
        normalimagePicker.delegate = self
        //normalimagePicker.sourceType = .camera
        editProfileScrollView.delegate = self
        
        editProfileScrollView.minimumZoomScale = 1.0
        editProfileScrollView.maximumZoomScale = 10.0
        
        lbluserName.text = Language_handler.VJLocalizedString("user_name", comment: nil)
        lblRadius.text = Language_handler.VJLocalizedString("radius", comment: nil)
        
        lblMyProfile.text = Language_handler.VJLocalizedString("edit_profile", comment: nil)
        lblEmail.text = Language_handler.VJLocalizedString("email", comment: nil)
        lblMobile.text = Language_handler.VJLocalizedString("mobile", comment: nil)
        lblAddress.text = Language_handler.VJLocalizedString("address", comment: nil)
        btnUpdate1.setTitle(Language_handler.VJLocalizedString("update", comment: nil), for: UIControlState())
        btnUpdate2.setTitle(Language_handler.VJLocalizedString("update", comment: nil), for: UIControlState())
        btnUpdate3.setTitle(Language_handler.VJLocalizedString("update", comment: nil), for: UIControlState())
        btnUpdate4.setTitle(Language_handler.VJLocalizedString("update", comment: nil), for: UIControlState())
        
        btnUpdate5.setTitle(Language_handler.VJLocalizedString("update", comment: nil), for: UIControlState())
        btnUpdate6.setTitle(Language_handler.VJLocalizedString("update", comment: nil), for: UIControlState())
        
        worklbl.text=theme.setLang("working_loc")
        avail.text=theme.setLang("availability")
        
        categorylbl.setTitle(theme.setLang("category"), for: UIControlState())
        imagePicker.delegate = self
        self.blurBannerImg()
        decorateTxtField()
        GetDatasForBanking()
        editAvailabilitytableview.register(UINib(nibName: "EditAvailabilityTableViewCell", bundle: nil), forCellReuseIdentifier: "availabledayscell")
        category_TblView.register(UINib(nibName: "CategoryListTableViewCell", bundle: nil), forCellReuseIdentifier: "CategoryListTableViewCell")
        editAvailabilitytableview.delegate = self
        editAvailabilitytableview.dataSource = self
        editAvailabilitytableview.reload()
        
        
        Placesearch_tableview.isHidden=true
        Placesearch_tableview.tableFooterView=UIView()
        Placesearch_tableview.layer.borderWidth=1.0
        Placesearch_tableview.layer.borderColor=UIColor.black.cgColor
        Placesearch_tableview.layer.cornerRadius=5.0
        
        
        Location_tableview.isHidden=true
        Location_tableview.tableFooterView=UIView()
        Location_tableview.layer.borderWidth=1.0
        Location_tableview.layer.borderColor=UIColor.black.cgColor
        Location_tableview.layer.cornerRadius=5.0
        // Do any additional setup after loading the view.
        
        addressTxtField.addTarget(self, action: #selector(TextfieldDidChange(_:)), for: UIControlEvents.editingChanged)
        working_loca.addTarget(self, action: #selector(TextfieldDidChange(_:)), for: UIControlEvents.editingChanged)
        
        
        
        Placesearch_tableview.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        Location_tableview.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        
        
        self.view.layer.transform = CATransform3DMakeScale(0.1,0.1,1)
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layer.transform = CATransform3DMakeScale(1.05,1.05,1)
        },completion: { finished in
            UIView.animate(withDuration: 0.1, animations: {
                self.view.layer.transform = CATransform3DMakeScale(1,1,1)
            })
        })
        
        
    }
    
    
    
    @objc private func TextfieldDidChange(_ textField:UITextField)
    {
        if(textField == addressTxtField)
        {
            if(addressTxtField.text != "")
            {
                if(places.count == 0)
                {
                    self.Placesearch_tableview.isHidden=true
                    
                }
                    
                else
                {
                    self.Placesearch_tableview.isHidden=false
                    
                }
                
                getTextfieldtag = "1"
                getPlaces(addressTxtField.text!)
                
                
                
            }
            else
            {
                
                places.removeAll()
                self.Placesearch_tableview.isHidden=true
            }
        }
            
        else if (textField == working_loca)
        {
            if(working_loca.text != "")
            {
                if(places.count == 0)
                {
                    self.Location_tableview.isHidden=true
                    
                }
                else
                {
                    self.Location_tableview.isHidden=false
                    
                }
                getTextfieldtag = "2"
                getPlaces(working_loca.text!)
                
                
            }
            else
            {
                
                places.removeAll()
                self.Location_tableview.isHidden=true
            }
            
        }
    }
    
    
    
    func getPlaces(_ searchString: String) {
        
        let request = requestForSearch(searchString)
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request, completionHandler: { data, response, error in
            
            self.handleResponse(data, response: response as? HTTPURLResponse, error: error)
            
        })
        
        
        
        task.resume()
        
    }
    
    
    
    
    func handleResponse(_ data: Data!, response: HTTPURLResponse!, error: Error!) {
        
        if let error = error {
            
            print("GooglePlacesAutocomplete Error: \(error.localizedDescription)")
            
            return
            
        }
        
        
        
        if response == nil {
            
            print("GooglePlacesAutocomplete Error: No response from API")
            
            return
            
        }
        
        
        
        if response.statusCode != 200 {
            
            print("GooglePlacesAutocomplete Error: Invalid status code \(response.statusCode) from API")
            
            return
            
        }
        
        
        do {
            
            let json: NSDictionary = try JSONSerialization.jsonObject(
                
                with: data,
                
                options: JSONSerialization.ReadingOptions.mutableContainers
                
                ) as! NSDictionary
            
            DispatchQueue.main.async(execute: {
                
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                
                
                
                if let predictions = json["predictions"] as? Array<AnyObject> {
                    
                    self.places = predictions.map { (prediction: AnyObject) -> Place in
                        
                        return Place(
                            
                            id: prediction["id"] as! String,
                            
                            description: prediction["description"] as! String
                            
                        )
                        
                    }
                    self.Placesearch_tableview.reload()
                    self.Location_tableview.reload()
                    
                    
                }
                
            })
        }
            
            
        catch let error as NSError {
            // Catch fires here, with an NSErrro being thrown from the JSONObjectWithData method
            print("A JSON parsing error occurred, here are the details:\n \(error)")
        }
        
        
        // Perform table updates on UI thread
        
        
        
    }
    
    func requestForSearch(_ searchString: String) -> URLRequest {
        
        
        
        let params = [
            
            "input": searchString,
            
            // "type": "(\(placeType.description))",
            
            //"type": "",
            
            "key": "\(googleApiKey)"
            
        ]
        print("the url is https://maps.googleapis.com/maps/api/place/autocomplete/json?\(query(params as [String : AnyObject]))")
        
        
        
        return NSMutableURLRequest(
            
            url: URL(string: "https://maps.googleapis.com/maps/api/place/autocomplete/json?\(query(params as [String : AnyObject]))")!
            
            
            ) as URLRequest
        
    }
    
    
    
    
    
    
    
    func query(_ parameters: [String: AnyObject]) -> String {
        var components: [(String, String)] = []
        for key in  (Array(parameters.keys).sorted(by: <)) {
            let value: AnyObject! = parameters[key]
            
            components += [(escape(key), escape("\(value!)"))]
        }
        
        return components.map{"\($0)=\($1)"}.joined(separator: "&")
    }
    
    
    
    func escape(_ string: String) -> String {
        
        let legalURLCharactersToBeEscaped: CFString = ":/?&=;+!@#$()',*" as CFString
        
        return CFURLCreateStringByAddingPercentEscapes(nil, string as CFString, nil, legalURLCharactersToBeEscaped, CFStringBuiltInEncodings.UTF8.rawValue) as String
        
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        var coun = Int()
        if tableView == Placesearch_tableview{
            coun = places.count
        }else if tableView == Location_tableview
        {
            coun = places.count
            
        }
        else if tableView == editAvailabilitytableview{
            coun = weekDay.count
            //            coun = 0
        }
        else if tableView == category_TblView{
            coun = categoryArray.count
        }
        
        return coun
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView == Placesearch_tableview || tableView == Location_tableview{
            
            return 40.0
            
        }
        return 50
    }
    
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = UITableViewCell()
        if tableView == Placesearch_tableview{
            
            let Placecell = (tableView.dequeueReusableCell(withIdentifier: "cell") )!
            let place = self.places[indexPath.row]
            Placecell.textLabel?.textColor = UIColor.black
            Placecell.textLabel?.text=place.description
            return Placecell
            
        }else if tableView == Location_tableview
        {
            
            let cell:UITableViewCell = (tableView.dequeueReusableCell(withIdentifier: "cell") )!
            
            
            let place = self.places[indexPath.row]
            
            cell.textLabel?.text=place.description
            
            
            return cell
            
            
        }
        else if tableView == editAvailabilitytableview{
            let availcell = (tableView.dequeueReusableCell(withIdentifier: "availabledayscell")) as! EditAvailabilityTableViewCell
            
            availcell.btn1.addTarget(self, action: #selector(EditProfileViewController.didClickenable(_:)), for: UIControlEvents.touchUpInside)
            availcell.btn2.addTarget(self, action: #selector(EditProfileViewController.didClickenable(_:)), for: UIControlEvents.touchUpInside)
            availcell.btn3.addTarget(self, action: #selector(EditProfileViewController.didClickenable(_:)), for: UIControlEvents.touchUpInside)
            // print(availabilityStorage)
            if indexPath.row == 0{
                availInc = 0
                availcell.btn1.isHidden = true
                availcell.btn2.isHidden = true
                availcell.btn3.isHidden = true
                availcell.lbl1.text = "\(weekDay[indexPath.row])"
                availcell.lbl2.text = "Morning"
                availcell.lbl3.text = "Afternoon"
                availcell.lbl4.text = "Evening"
                
            }
            else {
                let record = self.availabilityStorage.object(at: indexPath.row-1) as! AvailableRecord
                availcell.btn1.isHidden = false
                availcell.btn2.isHidden = false
                availcell.btn3.isHidden = false
                availcell.lbl2.isHidden = true
                availcell.lbl3.isHidden = true
                availcell.lbl4.isHidden = true
                
               availInc += 1
                availcell.btn1.tag = availInc
                if record.AvailMornigtime == "1"{
                    availcell.btn1.setBackgroundImage(UIImage.init(named: "tick"), for: UIControlState())
                    if !selectedBtn.contains(availInc){
                        selectedBtn.add(availInc)
                    }
                    
                }else{
                    availcell.btn1.setBackgroundImage(UIImage.init(named: "tick_gray"), for: UIControlState())
                    
                }
                
               availInc += 1
                availcell.btn2.tag = availInc
                if record.AvailAftertime == "1"{
                    availcell.btn2.setBackgroundImage(UIImage.init(named: "tick"), for: UIControlState())
                    if !selectedBtn.contains(availInc){
                        selectedBtn.add(availInc)
                    }
                    
                }else{
                    availcell.btn2.setBackgroundImage(UIImage.init(named: "tick_gray"), for: UIControlState())
                    
                }
                
                
                availInc += 1
                availcell.btn3.tag = availInc
                if record.Availeveningtime == "1"{
                    availcell.btn3.setBackgroundImage(UIImage.init(named: "tick"), for: UIControlState())
                    if !selectedBtn.contains(availInc){
                        selectedBtn.add(availInc)
                    }
                    
                }else{
                    availcell.btn3.setBackgroundImage(UIImage.init(named: "tick_gray"), for: UIControlState())
                    
                }
                
                
                availcell.lbl1.text = "\(weekDay[indexPath.row])"
                
            }
            return availcell
            
            
        }
            
        else if tableView == category_TblView{
            let cat_Cell = (tableView.dequeueReusableCell(withIdentifier: "CategoryListTableViewCell")) as! CategoryListTableViewCell
            let currentCat_Dtl = categoryArray[indexPath.row] as! CategoryList
            cat_Cell.categotyName_Lbl.text = currentCat_Dtl.categoryName
            cat_Cell.edit_Btn.tag = indexPath.row
            cat_Cell.delete_Btn.tag = indexPath.row
            cat_Cell.delete_Btn.addTarget(self, action: #selector(EditProfileViewController.category_DltAction), for: .touchUpInside)
            cat_Cell.edit_Btn.addTarget(self, action: #selector(EditProfileViewController.category_EditAction), for: .touchUpInside)
            category_TblView.frame = CGRect(x: tableView.frame.origin.x, y: 46, width: tableView.frame.size.width, height: tableView.contentSize.height+18)
            //category_View.frame = category_TblView.frame
            category_View.frame = CGRect(x: category_View.frame.origin.x , y: category_View.frame.origin.y , width: category_View.frame.width, height: category_TblView.frame.height+100)
            
            return cat_Cell
        }
        return cell
        
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if tableView .isEqual(Placesearch_tableview)
        {
            
            if  getTextfieldtag == "1"
                
            {
                let place = self.places[indexPath.row]
                
                self.showProgress()
                Addaddress_Data.YourLocality = "\(place.description)" as NSString
                
                let geocoder: CLGeocoder = CLGeocoder()
                
                geocoder.geocodeAddressString(place.description, completionHandler: {(placemarks: [CLPlacemark]?, error: Error?) in
                    self.DismissProgress()
                    if (error != nil) {
                        print("Error \(error!)")
                    } else if let placemark = placemarks?[0] {
                        
                        
                        print("the responsadsase is .......\(placemark)")
                        
                        //                let CityName:String?=placemark.subAdministrativeArea
                        let _:String?=placemark.subLocality
                        
                        let ZipCode:String?=placemark.postalCode
                        
                        
                        if (placemark.administrativeArea != nil)
                        {
                            self.stateTxtField.text = placemark.administrativeArea!
                        }
                        
                        if(placemark.country != nil){
                            self.countryTxtField.text = placemark.country!
                        }
                        
                        if (placemark.subAdministrativeArea != nil)
                        {
                            self.cityTxtField.text = self.theme.CheckNullValue(placemark.subAdministrativeArea! as AnyObject)
                        }
                        
                        if(ZipCode != nil)
                        {
                            self.postalCodeTxtField.text=""
                            
                            self.postalCodeTxtField.text=ZipCode!
                            
                        }
                        else
                        {
                            self.postalCodeTxtField.text=""
                            
                        }
                    }
                })
                
                
                
                url_handler.makeGetCall("https://maps.google.com/maps/api/geocode/json?sensor=false&key=\(googleApiKey)&address=\(place.description.addingPercentEscapes(using: String.Encoding.utf8)!)" as NSString) { (responseObject) -> () in
                    
                    
                    if(responseObject != nil)
                    {
                        let responseObject = responseObject!
                        
                        let results:NSArray=(responseObject.object(forKey: "results"))! as! NSArray
                        if results.count > 0
                        {
                            let firstItem: NSDictionary = results.object(at: 0) as! NSDictionary
                            let geometry: NSDictionary = firstItem.object(forKey: "geometry") as! NSDictionary
                            let locationDict:NSDictionary = geometry.object(forKey: "location") as! NSDictionary
                            let lat:NSNumber = locationDict.object(forKey: "lat") as! NSNumber
                            let lng:NSNumber = locationDict.object(forKey: "lng") as! NSNumber
                            
                            
                            self.Addaddress_Data.Latitude="\(lat)" as NSString
                            
                            self.Addaddress_Data.Longitude="\(lng)" as NSString
                        }
                        
                        
                    }
                    else
                    {
                        self.Addaddress_Data.Latitude="0"
                        self.Addaddress_Data.Longitude="0"
                        
                    }
                    
                    
                    
                }
                
                addressTxtField.text=Addaddress_Data.YourLocality as String
                
                
                Placesearch_tableview.isHidden=true
                
            }
        }
            
        else if tableView == Location_tableview{
            
            let place = self.places[indexPath.row]
            
            self.showProgress()
            
            let geocoder: CLGeocoder = CLGeocoder()
            
            geocoder.geocodeAddressString(place.description, completionHandler: { (placemarks: [CLPlacemark]?, error : Error?) in
                self.DismissProgress()
                self.DismissProgress()
                if (error != nil) {
                    print("Error \(error!)")
                } else if let placemark = placemarks?[0] {
                    print(placemark)
                }
            })
            //            geocoder.geocodeAddressString(place.description, completionHandler: {(placemarks: [CLPlacemark]?, error: NSError?) -> Void in
            //                self.DismissProgress()
            //                if (error != nil) {
            //                    print("Error \(error!)")
            //                } else if let placemark = placemarks?[0] {
            //
            //                }
            //                } as! CLGeocodeCompletionHandler)
            
            
            
            url_handler.makeGetCall("https://maps.google.com/maps/api/geocode/json?sensor=false&key=\(googleApiKey)&address=\(place.description.addingPercentEscapes(using: String.Encoding.utf8)!)" as NSString) { (responseObject) -> () in
                
                
                if(responseObject != nil)
                {
                    let responseObject = responseObject!
                    
                    
                    let results:NSArray=(responseObject.object(forKey: "results"))! as! NSArray
                    if results.count > 0
                    {
                        let firstItem: NSDictionary = results.object(at: 0) as! NSDictionary
                        let geometry: NSDictionary = firstItem.object(forKey: "geometry") as! NSDictionary
                        let locationDict:NSDictionary = geometry.object(forKey: "location") as! NSDictionary
                        let lat:NSNumber = locationDict.object(forKey: "lat") as! NSNumber
                        let lng:NSNumber = locationDict.object(forKey: "lng") as! NSNumber
                        
                        
                        self.Addaddress_Data.working_Latitude="\(lat)" as NSString
                        
                        self.Addaddress_Data.working_Longitude="\(lng)" as NSString
                    }
                    
                    
                }
                else
                {
                    self.Addaddress_Data.working_Latitude="0"
                    self.Addaddress_Data.working_Longitude="0"
                    
                }
                
            }
            
            working_loca.text="\(place.description)"
            Location_tableview.isHidden=true
            
        }
        
    }
    
    
    
    
    
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        
        if (NSStringFromClass((touch.view?.classForCoder)!)=="UITableViewCellContentView")
        {
            
            
            
            return false
        }
            
        else{
            return true
        }
    }
    
    func category_DltAction(_ sender:UIButton){
        self.showProgress()
        print(sender.tag)
        let getCurrent_Val = categoryArray[sender.tag] as! CategoryList
        let getCat_ID =  getCurrent_Val.category_ID
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["tasker":"\(objUserRecs.providerId)","category":"\(String(describing: getCat_ID))"]
        url_handler.makeCall(deleteCategory, param: Param as NSDictionary) {
            (responseObject, error) -> () in
            self.DismissProgress()
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
            }
            else
            {
                if(responseObject != nil)
                {
                    let responseObject = responseObject!
                    let status=self.theme.CheckNullValue(responseObject.object(forKey: "status"))!
                    
                    if(status == "1") {
                        self.view.makeToast(message:self.theme.CheckNullValue(responseObject.object(forKey: "response"))!, duration: 3, position: HRToastPositionDefault as AnyObject, title: "\(appNameJJ)")
                        self.categoryArray.removeObject(at: sender.tag)
                        self.category_TblView.reload()
                        
                    }else{//alertImg
                        self.view.makeToast(message:self.theme.CheckNullValue(responseObject.object(forKey: "message"))!, duration: 3, position: HRToastPositionDefault as AnyObject, title: "\(appNameJJ)")
                    }
                    
                }
            }
        }
    }
    
    func category_EditAction(_ sender:UIButton){
        print(sender.tag)
        
        // getSubCategory_Dtl
        let getSelectCat_Dtl = categoryArray[sender.tag] as! CategoryList
        let subCat_ID = getSelectCat_Dtl.category_ID
        getSubcategory_Dtls(subCat_ID)
        //let categoryVC = storyboard?.instantiateViewControllerWithIdentifier("CategoryEditVC") as! CategoryEditVC
        //categoryVC.checkCategory = "Edit Category"
        // self.navigationController?.pushViewController(withFlip: categoryVC, animated: true)
        
        
    }
    
    func getSubcategory_Dtls(_ subCat_ID:String){
        self.showProgress()
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["tasker":"\(objUserRecs.providerId)","category":"\(subCat_ID)"]
        url_handler.makeCall(getEditCategory_Detail, param: Param as NSDictionary) {
            (responseObject, error) -> () in
            
            self.DismissProgress()
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: "Network Failure !!!")
            }
            else
            {
                if(responseObject != nil && (responseObject?.count)!>0)
                {
                    let responseObject = responseObject!
                    let status=self.theme.CheckNullValue(responseObject.object(forKey: "status"))!
                    
                    if(status == "1")
                    {
                        self.editCatDtls_Array.removeAllObjects()
                        let getReponse = responseObject.object(forKey: "response") as! NSDictionary
                        let subCat_ID = self.theme.CheckNullValue(getReponse.object(forKey: "child_id"))
                        let subCateg_Name = self.theme.CheckNullValue(getReponse.object(forKey: "child_name"))
                        let experience_ID = self.theme.CheckNullValue(getReponse.object(forKey: "experience_id"))
                        let experience_Name = self.theme.CheckNullValue(getReponse.object(forKey: "experience_name"))
                        let hour_Rate = self.theme.CheckNullValue(getReponse.object(forKey: "hour_rate"))
                        let minHour_Rate = self.theme.CheckNullValue(getReponse.object(forKey: "min_hourly_rate"))
                        let mainCat_ID = self.theme.CheckNullValue(getReponse.object(forKey: "parent_id"))
                        let mainCate_Name = self.theme.CheckNullValue(getReponse.object(forKey: "parent_name"))
                        let quick_Pitch = self.theme.CheckNullValue(getReponse.object(forKey: "quick_pitch"))
                        let category_type = self.theme.CheckNullValue(getReponse.object(forKey: "category_type"))
                        self.editCatDtls_Array.add(EditCategoryDetails.init(experience_Lvl:experience_Name! , subCat_ID: subCat_ID!, subCat_Name: subCateg_Name!, experience_ID: experience_ID!, expereince_Name: experience_Name!, hour_Rate: hour_Rate!, min_Rate: minHour_Rate!, mainCat_ID: mainCat_ID!, mainCat_Name: mainCate_Name!, quickPinch: quick_Pitch!))
                        
                        let categoryVC = self.storyboard?.instantiateViewController(withIdentifier: "CategoryEditVC") as! CategoryEditVC
                        categoryVC.checkCategory = "Edit Category"
                        categoryVC.editCatDtl_Array = self.editCatDtls_Array
                        self.navigationController?.pushViewController(withFlip: categoryVC, animated: true)
                        
                    }
                    else
                    {
                        self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: "Network Failure !!!")
                    }
                }
                
            }
            
        }
        
    }
    func GetDatasForBanking(){
        self.showProgress()
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)"]
        
        url_handler.makeCall(GetEditInfoUrl , param: Param as NSDictionary) {
            (responseObject, error) -> () in
            self.DismissProgress()
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
            }
            else
            {
                if(responseObject != nil)
                {
                    let responseObject = responseObject!
                    
                    self.setDatasToEditView(responseObject)
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
                }
            }
        }
    }
    func setDatasToEditView(_ Dict:NSDictionary){
        let status:NSString=Dict["status"] as! NSString
        
        if(status == "1")
        {
            let resDict: NSDictionary = Dict.object(forKey: "response") as! NSDictionary
            
            self.userImg.sd_setImage(with: URL(string:(resDict.object(forKey: "image")as! NSString as String)), placeholderImage: UIImage(named: "PlaceHolderSmall"))
            
            self.bannerImg.sd_setImage(with: URL(string:(resDict.object(forKey: "image")as! NSString as String)), placeholderImage: UIImage(named: "PlaceHolderBig"))
            
            
            phoneTxtField.text=theme.CheckNullValue(resDict.object( forKey: "mobile_number" ))!
            phoneTxtField.placeholder = Language_handler.VJLocalizedString("mobile", comment: nil)
            cCodeTxtField.text=theme.CheckNullValue(resDict.object( forKey: "dial_code" ))!
            emailTxtField.text=theme.CheckNullValue(resDict.object( forKey: "email" ) )!
            emailTxtField.placeholder = Language_handler.VJLocalizedString("email", comment: nil)
            
            addressTxtField.text=theme.CheckNullValue(resDict.object( forKey: "address" ))!
            addressTxtField.placeholder =  Language_handler.VJLocalizedString("address", comment: nil)
            working_loca.text=theme.CheckNullValue(resDict.object( forKey: "working_location" ))!
            working_loca.placeholder =  Language_handler.VJLocalizedString("working_loc", comment: nil)
            
            
            stateTxtField.text=theme.CheckNullValue(resDict.object( forKey: "state" ))!
            stateTxtField.placeholder = Language_handler.VJLocalizedString("state", comment: nil)
            cityTxtField.text=theme.CheckNullValue(resDict.object( forKey: "city" ))!
            cityTxtField.placeholder = Language_handler.VJLocalizedString("city", comment: nil)
            
            countryTxtField.text=theme.CheckNullValue(resDict.object( forKey: "country" ))!
            countryTxtField.placeholder = Language_handler.VJLocalizedString("country", comment: nil)
            postalCodeTxtField.text=theme.CheckNullValue(resDict.object( forKey: "postal_code" ))!
            postalCodeTxtField.placeholder = Language_handler.VJLocalizedString("pincode", comment: nil)
            usernameTxtfield.text = theme.CheckNullValue(resDict.object( forKey: "username" ))!
            usernameTxtfield.placeholder =
                Language_handler.VJLocalizedString("pincode", comment: nil)
            radiusTxtfield.text = theme.CheckNullValue(resDict.object( forKey: "radius" ))!
            radiusTxtfield.placeholder = Language_handler.VJLocalizedString("radius", comment: nil)
            
        }
        else
        {
            self.view.makeToast(message:kErrorMsg, duration: 5, position: HRToastPositionDefault as AnyObject, title: kNetworkFail)
        }
    }
    
    
    @IBAction func EditProfileClickOption(_ sender: AnyObject) {
        
        
        
        
        let ImagePicker_Sheet = UIAlertController(title: nil, message: theme.setLang("select_image"), preferredStyle: .actionSheet)
        
        let Camera_Picker = UIAlertAction(title: theme.setLang("camera") , style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.Camera_Pick()
        })
        let Gallery_Picker = UIAlertAction(title:theme.setLang("gallery") , style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            //
            self.Gallery_Pick()
            
        })
        
        let cancelAction = UIAlertAction(title: theme.setLang("cancel_small"), style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        
        ImagePicker_Sheet.addAction(Camera_Picker)
        ImagePicker_Sheet.addAction(Gallery_Picker)
        ImagePicker_Sheet.addAction(cancelAction)
        
        self.present(ImagePicker_Sheet, animated: true, completion: nil)
    }
    
    
    func Camera_Pick()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            
            self.present(normalimagePicker, animated: true, completion: nil)
        }
        else
        {
            Gallery_Pick()
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        
        normalimagePicker.dismiss(animated: true, completion: nil)
        userImg.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        let pickimage = info[UIImagePickerControllerOriginalImage] as? UIImage
        let pickedimage = theme.rotateImage(pickimage!)
        imagedata = UIImageJPEGRepresentation(pickedimage, 0.1)!;
        self.uploadImageAndData()
        
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        normalimagePicker.dismiss(animated: true, completion: nil)
        
    }
    func Gallery_Pick()
    {
        
        self.imagePicker = WDImagePicker()
        self.imagePicker.cropSize = CGSize(width: 280, height: 280)
        self.imagePicker.delegate = self
        self.imagePicker.resizableCropArea = true
        self.imagePicker.sourceType = .photoLibrary
        self.present(self.imagePicker.imagePickerController, animated: true, completion: nil)
        
    }
    
    
    func imagePicker(_ imagePicker: WDImagePicker, pickedImage: UIImage) {
        let image = UIImage.init(cgImage: pickedImage.cgImage!, scale: 0.25 , orientation:.up)
        imagedata = UIImageJPEGRepresentation(image, 0.1)!;
        self.userImg.image = image
        self.hideImagePicker()
        self.uploadImageAndData()
    }
    
    func hideImagePicker() {
        self.imagePicker.imagePickerController.dismiss(animated: true, completion: nil)
    }
    
    
    func blurBannerImg(){
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            bannerImg.backgroundColor = UIColor.clear
            
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            //always fill the view
            blurEffectView.frame = bannerImg.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            bannerImg.addSubview(blurEffectView) //if you have more UIViews, use an insertSubview API to place it where needed
        }
        else {
            bannerImg.backgroundColor = UIColor.black
        }
        userImg.layer.cornerRadius=userImg.frame.size.width/2
        //userImg.layer.borderWidth=0.75
        //userImg.layer.borderColor=PlumberThemeColor.cgColor
        userImg.layer.masksToBounds=true
    }
    
    func decorateTxtField(){
        NotificationCenter.default.addObserver(self, selector: #selector(EditProfileViewController.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(EditProfileViewController.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        phoneTxtField=roundTheViews(phoneTxtField) as! UITextField
        emailTxtField=roundTheViews(emailTxtField) as! UITextField
        
        emailTxtField.isUserInteractionEnabled = false
        usernameTxtfield=roundTheViews(usernameTxtfield) as! UITextField
        working_loca=roundTheViews(working_loca) as! UITextField
        
        
        radiusTxtfield=roundTheViews(radiusTxtfield) as! UITextField
        
        radiusTxtfield.isUserInteractionEnabled = true
        
        
        cCodeTxtField=roundTheViews(cCodeTxtField) as! UITextField
        addressTxtField=roundTheViews(addressTxtField) as! UITextField
        countryTxtField=roundTheViews(countryTxtField) as! UITextField
        postalCodeTxtField=roundTheViews(postalCodeTxtField) as! UITextField
        stateTxtField=roundTheViews(stateTxtField) as! UITextField
        cityTxtField=roundTheViews(cityTxtField) as! UITextField
        
        theme.SetPaddingView(usernameTxtfield)
        theme.SetPaddingView(radiusTxtfield)
        theme.SetPaddingView(working_loca)
        
        
        setMandatoryFields()
        
    }
    func setMandatoryFields(){
        for subView:UIView in editProfileScrollView.subviews{
            if(subView.isKind(of: SMIconLabel.self)){
                //                let lbl: SMIconLabel = (subView as? SMIconLabel)!
                //               // if !lbl.isEqual(routingLbl) {
                //                    lbl.icon = UIImage(named: "MandatoryImg")
                //                    lbl.iconPadding = 5
                //                    lbl.iconPosition = SMIconLabelPosition.Right
                //              //  }
            }else if(subView.isKind(of: UITextField.self)){
                let txtField: UITextField = (subView as? UITextField)!
                let arrow: UIView = UILabel()
                arrow.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
                txtField.leftView = arrow
                txtField.leftViewMode = UITextFieldViewMode.always
            }
        }
    }
    
    func roundTheViews(_ oTxt:AnyObject)->AnyObject{
        oTxt.layer.cornerRadius=3
        oTxt.layer.borderWidth=0.5
        oTxt.layer.borderColor=UIColor.lightGray.cgColor
        oTxt.layer.masksToBounds=true
        return oTxt
    }
    
    @IBAction func didClickEditImage(_ sender: AnyObject) {
        //        let actionSheet = UIActionSheet(title: "Select Image from", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Camera", "Gallery")
        //        actionSheet.showInView(self.view)
    }
    
    //    @IBAction func didClickBioUpdate(sender: AnyObject) {
    //        if(validateTxtFields("1")){
    //            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
    //            let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)",
    //                "bio":"\(bioTxtView.text!)"]
    //            updateUserProfile(UpdateBioUrl, withDict: Param)
    //        }
    //    }
    
    @IBAction func didClickEmailUpdate(_ sender: AnyObject) {
        if(validateTxtFields("2")){
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)",
                "email":"\(emailTxtField.text!)"]
            updateUserProfile(UpdateEmailUrl as NSString, withDict: Param as NSDictionary)
        }
    }
    
    @IBAction func didClickMobileUpdate(_ sender: AnyObject) {
        Contact=phoneTxtField.text!
        
        if(validateTxtFields("3")){
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)",
                "country_code":"\(cCodeTxtField.text!)",
                "mobile_number":"\(phoneTxtField.text!)"]
            updateUserProfile(UpdateMobileUrl as NSString, withDict: Param as NSDictionary)
        }
    }
    
    @IBAction func didClickAddressUpdate(_ sender: AnyObject) {
        if(validateTxtFields("4")){
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId )",
                "address":"\(addressTxtField.text!)",
                "city":"\(cityTxtField.text!)",
                "state":"\(stateTxtField.text!)",
                "country":"\(countryTxtField.text!)",
                "postal_code":"\(postalCodeTxtField.text!)"]
            updateUserProfile(UpdateAddressUrl as NSString, withDict: Param as NSDictionary)
        }
    }
    
    @IBAction func didclickWorkingLocUpdate(_ sender: AnyObject) {
        
        if(validateTxtFields("7")){
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)",
                "lat":"\(Addaddress_Data.working_Latitude)",
                "long":"\(Addaddress_Data.working_Longitude)"]
            updateUserProfile(UpdateWorkingLocation as NSString, withDict: Param as NSDictionary)
        }
        
    }
    
    @IBAction func didClickAvailabilityUpdate(_ sender: AnyObject) {
            var daysCount:Int = 0
        var availabilityDict: NSMutableDictionary = NSMutableDictionary()
        let availArray = NSMutableArray()
        for j in 1...7 {
            let availObj = Availability()
            availObj.day = "\(weekFullDay[j])"
            availObj.isAfternoon = false
            availObj.isMorning = false
            availObj.isEvening = false
            availArray.add(availObj)
        }
        
        var count : Int = 0
                for var i in 1...7 {
                    count += 1
                    let first = count
                    let second = count+1
                    let third = count+2
                    count =  third
                    var arr = [Int]()
                    if(selectedBtn.contains(first)){
                        arr.append(first)
                    }
                    if(selectedBtn.contains(second)){
                        arr.append(second)
                    }
                    if(selectedBtn.contains(third)){
                        arr.append(third)
                    }
                    daysCount += 1
                    for (element,item) in arr.enumerated(){
                        switch (item%3){
                        case 0:
                            let obj = availArray.object(at: daysCount-1) as! Availability
                            obj.isEvening = true
                            break;
                        case 1:
                            let obj = availArray.object(at: daysCount-1) as! Availability
                            obj.isMorning = true
                            break;
        
                        case 2:
                            let obj = availArray.object(at: daysCount-1) as! Availability
                            obj.isAfternoon = true
                            break;
                        default:
                            break;
                        }
                    }
                }
        
//        for item in selectedBtn
//        {
//            var quo = (item as! Int) / 3
//            let reminder = (item as! Int) % 3
//            if(reminder > 0)
//            {
//                quo += 1
//            }
//
//            switch ((item as! Int) % 3){
//            case 0:
//                let obj = availArray.object(at: quo-1) as! Availability
//                obj.isEvening = true
//                break;
//            case 1:
//                let obj = availArray.object(at: quo-1) as! Availability
//                obj.isMorning = true
//                break;
//
//            case 2:
//                let obj = availArray.object(at: quo-1) as! Availability
//                obj.isAfternoon = true
//                break;
//            default:
//                break;
//            }
//        }
         var daysDict : NSDictionary = NSDictionary()
        var workingdaysarray : NSMutableArray = NSMutableArray()
     
        var increment = 0
        for (element,item)  in availArray.enumerated(){
              var TimingDict : Dictionary = [String : String]()
            let obj1 = availArray.object(at:element) as! Availability
            
            if obj1.isMorning == true || obj1.isAfternoon == true || obj1.isEvening == true{
                
                if obj1.isMorning == true{
                    TimingDict["morning"] = "1"
               
                }
                if obj1.isEvening == true{
                     TimingDict ["evening"] = "1"
   
                }
                if obj1.isAfternoon == true{
                     TimingDict ["afternoon"] = "1"
 
                }
            
                daysDict = ["day":obj1.day,"hour":TimingDict]
                workingdaysarray.add(daysDict)
                increment += 1
            }
        }
        let objUserRecs:UserInfoRecord=self.theme.GetUserDetails()
        availabilityDict.setValue(workingdaysarray, forKey:"working_days")
availabilityDict.setValue(objUserRecs.providerId, forKey:"provider_id")

        
        print(availabilityDict)
       updateWorkingDays(availabilityDict as NSDictionary)
        
        
    }
    
//    func showProgressbar()
//    {
//        progressbar.show(message: "", style: BlueIndicatorStyle())
//    }
    
    func updateWorkingDays(_ param:NSDictionary){
        self.showProgress()
        url_handler.makeCall(UpdateWorkingDays  , param: param as NSDictionary) {
            (responseObject, error) -> () in
            self.DismissProgress()
            if(error != nil) {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: "Network Failure !!!")
            }
            else {
                if(responseObject != nil && (responseObject?.count)!>0)
                {
                    let responseObject = responseObject!
                    
                    let status=self.theme.CheckNullValue(responseObject.object(forKey: "status"))!
                    
                    if(status == "1") {
                        self.view.makeToast(message:self.theme.CheckNullValue(responseObject.object(forKey: "message"))!, duration: 3, position: HRToastPositionDefault as AnyObject, title: "\(appNameJJ)")
                        
                    }else{//alertImg
                        self.view.makeToast(message:self.theme.CheckNullValue(responseObject.object(forKey: "message"))!, duration: 3, position: HRToastPositionDefault as AnyObject, title: "\(appNameJJ)")
                    }
                    
                }else{
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: "Network Failure !!!")
                }
            }
            
        }
        
    }
    
//    func DismissProgressbar()
//    {
//        progressbar.dismiss()
//    }
    
    
    
    func uploadImageAndData(){
        let objUserRecs:UserInfoRecord=self.theme.GetUserDetails()
        
        let param : NSDictionary =  ["provider_id"  :objUserRecs.providerId]
        let imageData = imagedata
        self.showProgress()
        
        let URL = try! URLRequest(url: UpdateImageUrlUrl, method: .post, headers: ["apptype": "ios", "apptoken":"\(theme.GetDeviceToken())", "providerid":"\(objUserRecs.providerId)"])
        
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            multipartFormData.append(imageData, withName: "file", fileName: "file.png", mimeType: "")
            
            for (key, value) in param {
                
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
            }
            
        }, with: URL, encodingCompletion: { encodingResult in
            switch encodingResult {
                
            case .success(let upload, _, _):
                
                
                upload.responseJSON { response in
                    
                    
                    if let js = response.result.value {
                        let JSON = js as! NSDictionary
                        self.DismissProgress()
                        print("JSON: \(JSON)")
                        
                        let Status = self.theme.CheckNullValue(JSON.object(forKey: "status"))!
                        let response = JSON.object(forKey: "response") as! NSDictionary
                        if(Status == "1")
                        {
                            
                            self.theme.AlertView("\(appNameJJ)", Message: self.theme.CheckNullValue(response.object(forKey: "msg"))!, ButtonTitle: kOk)
                            
                            let URL = try! URLRequest(url: self.theme.CheckNullValue(response.object(forKey: "image"))!, method: .get, headers: nil)
                            
                            self.userImg.sd_setImage(with: URL.url , placeholderImage: #imageLiteral(resourceName: "PlaceHolderSmall"))
                            self.theme.saveUserImage("\(URL.url)" as NSString)
                        }
                        else
                        {
                            
                            self.theme.AlertView("\(appNameJJ)", Message: self.theme.CheckNullValue(response.object(forKey: "msg"))!, ButtonTitle: kOk)
                            
                            
                            
                        }
                        
                    }
                }
                
            case .failure(let encodingError):
                
                //                      self.themes.AlertView("Image Upload Failed", Message: "Please try again", ButtonTitle: "Ok")
                print(" the encodeing error is \(encodingError)")
            }
        })
    }
    
    
    
    
    
    
    
    
    func uploadImg(_ dict: NSDictionary){
        updateUserProfile(UpdateImageUrlUrl as NSString, withDict: dict)
    }
    
    
    @IBAction func didClickBackBtn(_ sender: AnyObject) {
        self.navigationController?.popViewControllerWithFlip(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func validateTxtFields (_ condition:NSString ) -> Bool{
        self.view.endEditing(true)
        var isOK:Bool=true
        if(condition=="1"){
            /*if(bioTxtView.text.count==0){
             ValidationAlert(Language_handler.VJLocalizedString("bio_mand", comment: nil))
             isOK=false
             return isOK
             }*/
        }
        if(condition=="2"){
            if(emailTxtField.text!.count==0){
                self.theme.AlertView("\(appNameJJ)", Message:Language_handler.VJLocalizedString("email_mand", comment: nil), ButtonTitle: kOk)
                
                isOK=false
                return isOK
            }else if(theme.isValidEmail(emailTxtField.text!)){
                
                self.theme.AlertView("\(appNameJJ)", Message:Language_handler.VJLocalizedString("valid_email_alert", comment: nil), ButtonTitle: kOk)
                
                isOK=false
                return isOK
            }
        }
        if(condition=="3"){
            if(cCodeTxtField.text!.count==0){
                self.theme.AlertView("\(appNameJJ)", Message:Language_handler.VJLocalizedString("country_code_mand", comment: nil), ButtonTitle: kOk)
                
                isOK=false
                return isOK
            }
            else if(phoneTxtField.text!.count==0){
                
                self.theme.AlertView("\(appNameJJ)", Message:Language_handler.VJLocalizedString("mobile_number_mand", comment: nil), ButtonTitle: kOk)
                
                
                isOK=false
                return isOK
            }
            
        }
        if(condition=="4"){
            if(addressTxtField.text!.count==0){
                self.theme.AlertView("\(appNameJJ)", Message:Language_handler.VJLocalizedString("address_mand", comment: nil), ButtonTitle: kOk)
                isOK=false
                return isOK
            }
            if(cityTxtField.text!.count==0){
                self.theme.AlertView("\(appNameJJ)", Message:Language_handler.VJLocalizedString("city_mand", comment: nil), ButtonTitle: kOk)
                
                
                
                isOK=false
                return isOK
            }
            if(stateTxtField.text!.count==0){
                self.theme.AlertView("\(appNameJJ)", Message:Language_handler.VJLocalizedString("state_mand", comment: nil), ButtonTitle: kOk)
                
                
                isOK=false
                return isOK
            }
            if(countryTxtField.text!.count==0){
                self.theme.AlertView("\(appNameJJ)", Message:Language_handler.VJLocalizedString("country_mand", comment: nil), ButtonTitle: kOk)
                
                isOK=false
                return isOK
            }
            if(postalCodeTxtField.text!.count==0){
                self.theme.AlertView("\(appNameJJ)", Message:Language_handler.VJLocalizedString("postal_code_mand", comment: nil), ButtonTitle: kOk)
                isOK=false
                return isOK
            }
        }
        
        if (condition == "5"){
            if(usernameTxtfield.text!.count==0){
                self.theme.AlertView("\(appNameJJ)", Message:Language_handler.VJLocalizedString("username_mand", comment: nil), ButtonTitle: kOk)
                isOK=false
                return isOK
            }
            
        }
        if (condition == "6")
        {
            if(radiusTxtfield.text!.count==0){
                self.theme.AlertView("\(appNameJJ)", Message:Language_handler.VJLocalizedString("radius_mand", comment: nil), ButtonTitle: kOk)
                isOK=false
                return isOK
            }
            if(condition=="7"){
                
                if(working_loca.text!.count==0){
                    self.theme.AlertView("\(appNameJJ)", Message:Language_handler.VJLocalizedString("working_mand", comment: nil), ButtonTitle: kOk)
                    isOK=false
                    return isOK
                }
                
                
            }
            
            
            
            
        }
        return isOK
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func keyboardWillShow(_ notification:Notification){
        
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = editProfileScrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height+150
        editProfileScrollView.contentInset = contentInset
    }
    
    func keyboardWillHide(_ notification:Notification){
        
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        editProfileScrollView.contentInset = contentInset
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(range.location==0 && string==" "){
            return false
        }
        return true
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(range.location==0 && text==" "){
            return false
        }
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    
    
    
    func didClickenable(_ sender: UIButton) {
        
        if (selectedBtn.contains(sender.tag) ){
            selectedBtn.remove(sender.tag)
            sender.setBackgroundImage(UIImage.init(named: "tick_gray"), for: UIControlState())
        }else{
            selectedBtn.add(sender.tag)
            sender.setBackgroundImage(UIImage.init(named: "tick"), for: UIControlState())
        }
        print(selectedBtn)
        
    }
    @IBAction func didclickusername(_ sender: AnyObject)
    {
        if(validateTxtFields("5")){
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)",
                "user_name":"\(usernameTxtfield.text!)"]
            updateUserProfile(UpdateUsernameUrl as NSString, withDict: Param as NSDictionary)
        }
        
    }
    
    @IBAction func didupdateradius(_ sender: AnyObject) {
        if(validateTxtFields("6")){
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)",
                "radius":"\(radiusTxtfield.text!)"]
            updateUserProfile(UpdateRadiusUrl as NSString, withDict: Param as NSDictionary)
        }
        
    }
    
    
    
    @IBAction func didclickCategoryview(_ sender: AnyObject) {
        
        let categoryVC = storyboard?.instantiateViewController(withIdentifier: "CategoryEditVC") as! CategoryEditVC
        categoryVC.checkCategory = "Add Category"
        self.navigationController?.pushViewController(withFlip: categoryVC, animated: true)
        
        
    }
    
    func updateUserProfile(_ statusType:NSString, withDict:NSDictionary){
        
        NSLog("Getsataus type=%@", withDict)
        self.showProgress()
        
        // print(Param)
        
        url_handler.makeCall(statusType as String, param: withDict) {
            (responseObject, error) -> () in
            self.DismissProgress()
            
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
            }
            else
            {
                if(responseObject != nil && (responseObject?.count)!>0)
                {
                    let responseObject = responseObject!
                    let status=self.theme.CheckNullValue(responseObject.object(forKey: "status"))!
                    
                    if(status == "1")
                    {
                        
                        if(statusType as String == UpdateImageUrlUrl){
                            
                            let URL = try! URLRequest(url: self.theme.CheckNullValue(responseObject.object(forKey: "response"))!, method: .get, headers: nil)
                            
                            self.userImg.sd_setImage(with: URL.url , placeholderImage: #imageLiteral(resourceName: "PlaceHolderSmall"))
                            
                            let URL1 = try! URLRequest(url: self.theme.CheckNullValue(responseObject.object(forKey: "response"))!, method: .get, headers: nil)
                            
                            self.bannerImg.sd_setImage(with: URL1.url , placeholderImage: #imageLiteral(resourceName: "PlaceHolderBig"))
                            
                            
                            self.theme.AlertView(Language_handler.VJLocalizedString("success", comment: nil), Message: Language_handler.VJLocalizedString("profile_image_update", comment: nil), ButtonTitle: kOk)
                            
                            
                            self.theme.saveUserImage(responseObject.object( forKey: "response" ) as! NSString)
                            
                        }
                        if(statusType as String == UpdateBioUrl){
                            
                            self.theme.AlertView(Language_handler.VJLocalizedString("success", comment: nil), Message: Language_handler.VJLocalizedString("bio_update", comment: nil), ButtonTitle: kOk)
                            
                            
                        }
                        if(statusType as String == UpdateEmailUrl){
                            
                            self.theme.AlertView(Language_handler.VJLocalizedString("success", comment: nil), Message: Language_handler.VJLocalizedString("email_update", comment: nil), ButtonTitle: kOk)
                            
                        }
                        if(statusType as String == UpdateWorkingLocation){
                            
                            self.theme.AlertView(Language_handler.VJLocalizedString("success", comment: nil), Message: Language_handler.VJLocalizedString("location_update", comment: nil), ButtonTitle: kOk)
                            
                        }
                        if(statusType as String == UpdateAddressUrl){
                            
                            self.theme.AlertView(Language_handler.VJLocalizedString("success", comment: nil), Message: Language_handler.VJLocalizedString("address_update", comment: nil), ButtonTitle: kOk)
                            
                        }
                        if(statusType as String == UpdateMobileUrl){
                            
                            self.updateMob(responseObject)
                            
                            
                            
                        }
                        
                        if (statusType as String == UpdateUsernameUrl)
                        {
                            self.theme.AlertView(Language_handler.VJLocalizedString("success", comment: nil), Message: Language_handler.VJLocalizedString("username_update", comment: nil), ButtonTitle: kOk)
                            
                        }
                        if (statusType as String == UpdateRadiusUrl)
                        {
                            self.theme.AlertView(Language_handler.VJLocalizedString("success", comment: nil), Message: Language_handler.VJLocalizedString("radius_update", comment: nil), ButtonTitle: kOk)
                        }
                    }
                    else
                    {//alertImg
                        let response = self.theme.CheckNullValue(responseObject.object(forKey: "response"))!
                        if response == "Latitude is Required"
                        {
                            self.theme.AlertView("\(appNameJJ)", Message:self.theme.setLang("Select the Address from Suggestion"), ButtonTitle: kOk)
                        }
                        else{
                            self.theme.AlertView("\(appNameJJ)", Message: self.theme.CheckNullValue(responseObject.object(forKey: "response"))!, ButtonTitle: kOk)
                        }
                    }
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
                }
            }
            
        }
    }
    
    
    
    func  ValidateOTP() {
        self.showProgress()
        
        // print(Param)
        
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)",
            "country_code":"\(cCodeTxtField.text!)",
            "mobile_number":"\(phoneTxtField.text!)",
            "otp":"\(tField.text!)"]
        
        url_handler.makeCall(UpdateMobileUrl, param: Param as NSDictionary) {
            (responseObject, error) -> () in
            
            self.DismissProgress()
            
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
            }
            else
            {
                if(responseObject != nil && (responseObject?.count)!>0)
                {
                    let responseObject = responseObject!
                    let status=self.theme.CheckNullValue(responseObject.object(forKey: "status"))!
                    
                    if(status == "1")
                    {
                        self.theme.AlertView(Language_handler.VJLocalizedString("success", comment: nil), Message: Language_handler.VJLocalizedString("mobile_number_update", comment: nil), ButtonTitle: kOk)
                        
                    }
                    else{
                        
                    }
                }
            }
        }
        
        
    }
    
    func updateMob(_ responseObject:NSDictionary){
        
        otpStr = self.theme.CheckNullValue(responseObject.object( forKey: "otp" ))!
        otp_status = self.theme.CheckNullValue(responseObject.object( forKey: "otp_status" ))!
        
        //        let alertView = SCLAlertView()
        //        let txt = alertView.addTextField(Language_handler.VJLocalizedString("otp_placeholder")!)
        //        alertView.addButton(Language_handler.VJLocalizedString("ok", comment: nil)) {
        //            if txt.text == ""
        //            {
        //                self.view.makeToast(message:Language_handler.VJLocalizedString("OTP is mandatory", comment: nil), duration: 3, position: HRToastPositionDefault as AnyObject, title: "\(appNameJJ)");
        //            }
        //
        //            else if txt.text.text == self.otpStr as String
        //            {
        //                self.ValidateOTP()
        //
        //
        //            }
        //            else{
        //
        //                self.theme.AlertView("\(appNameJJ)", Message: Language_handler.VJLocalizedString("otp_alert", comment: nil), ButtonTitle: kOk)
        //            }
        //        }
        //        alertView.addButton(Language_handler.VJLocalizedString("ok", comment: nil)) {
        //            print("Text value: \(txt.text)")
        //        }
        //
        //        alertView.showEdit(Language_handler.VJLocalizedString("otp_placeholder"), subTitle: Language_handler.VJLocalizedString("otp_mobileupdate_desc")
        
        
        let alert = UIAlertController(title:Language_handler.VJLocalizedString("otp_placeholder", comment: nil), message: Language_handler.VJLocalizedString("otp_mobileupdate_desc", comment: nil), preferredStyle: .alert)
        
        alert.addTextField(configurationHandler: configurationTextField)
        alert.addAction(UIAlertAction(title:Language_handler.VJLocalizedString("cancel", comment: nil), style: .cancel, handler:handleCancel))
        alert.addAction(UIAlertAction(title:Language_handler.VJLocalizedString("ok", comment: nil), style: .default, handler:{ (UIAlertAction) in
            if self.tField.text == ""
            {
                self.view.makeToast(message:Language_handler.VJLocalizedString("OTP is mandatory", comment: nil), duration: 3, position: HRToastPositionDefault as AnyObject, title: "\(appNameJJ)");
            }
                
            else if self.tField.text == self.otpStr as String
            {
                self.ValidateOTP()
                
                
            }
            else{
                
                self.theme.AlertView("\(appNameJJ)", Message: Language_handler.VJLocalizedString("otp_alert", comment: nil), ButtonTitle: kOk)
            }
            
        }))
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
    }
    
    
    
    func getProviderCategory_Dtl(){
        self.showProgress()
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)"]
        url_handler.makeCall(viewProfile, param: Param as NSDictionary) {
            (responseObject, error) -> () in
            
            self.DismissProgress()
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: "Network Failure !!!")
            }
            else
            {
                if(responseObject != nil && (responseObject?.count)!>0)
                {
                    let responseObject = responseObject!
                    
                    let status=self.theme.CheckNullValue(responseObject.object(forKey: "status"))!
                    
                    if(status == "1")
                    {
                        
                        self.categoryArray.removeAllObjects()
                        
                        if(((responseObject.object(forKey: "response") as AnyObject).object(forKey: "details") as AnyObject).count>0){
                            let categoryArr = (responseObject.object(forKey: "response") as AnyObject).object(forKey: "category_Details") as! NSArray
                            
                            for category in categoryArr{
                                let category_Arr = category as! NSDictionary
                                let id = self.theme.CheckNullValue(category_Arr["_id"])
                                let name = self.theme.CheckNullValue(category_Arr["categoryname"])
                                self.categoryArray.add(CategoryList.init(categoryName:name!,category_ID:id!))
                                //
                            }
                            self.category_TblView.reload()
                        }
                        else
                        {
                            self.view.makeToast(message:kErrorMsg, duration: 5, position: HRToastPositionDefault as AnyObject, title: "Network Failure !!!")
                        }
                    }
                    else
                    {
                        self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: "Network Failure !!!")
                    }
                }
                
            }
            
        }
    }
    
    
    func configurationTextField(_ textField: UITextField!)
    {
        
        textField.placeholder = Language_handler.VJLocalizedString("otp_placeholder", comment: nil)
        
        tField = textField
        if otp_status == "development"
        {
            tField.text = self.otpStr as String
            tField.isUserInteractionEnabled = false
        }
        else{
            tField.text = ""
            tField.isUserInteractionEnabled = true
        }
    }
    
    
    
    
    func handleCancel(_ alertView: UIAlertAction!)
    {
        print("Cancelled !!")
    }
    override func viewDidAppear(_ animated: Bool) {
        category_TblView.reload()
        self.editProfileScrollView.frame.size.height = self.view.frame.size.height - self.editProfileScrollView.frame.origin.y
        editProfileScrollView.contentSize=CGSize(width: editProfileScrollView.frame.size.width,height: self.category_View.frame.origin.y+category_View.frame.size.height)
        self.view.addSubview(editProfileScrollView)
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getProviderCategory_Dtl()
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    //    - (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    //
    ////    CGFloat y = -scrollView.contentOffset.y;
    ////    if (y > 0) {
    ////    self.imageView.frame = CGRectMake(0, scrollView.contentOffset.y, self.cachedImageViewSize.size.width+y, self.cachedImageViewSize.size.height+y);
    ////    self.imageView.center = CGPointMake(self.view.center.x, self.imageView.center.y);
    ////    }
    ////
    ////    }
    
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let yPosision: CGFloat = -scrollView.contentOffset.y
        if yPosision > 0 {
            self.bannerImg.center = CGPoint(x: self.bannerImg.center.x, y: self.bannerImg.center.y)
            UIView.animate(withDuration: 0.2, animations: {
                //   self.userImg.isHidden = true
            })
        }
        
        
        
    }
    
    
}


