//
//  CategoryEditVC.swift
//  PlumberJJ
//
//  Created by Casperon on 19/05/17.
//  Copyright © 2017 Casperon Technologies. All rights reserved.
//

import UIKit



class CategoryEditVC: RootBaseViewController,NIDropDownDelegate,UITextViewDelegate,UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate {
    // @IBOutlet weak var edit_cat: UILabel!
    @IBOutlet weak var parent_cat: CustomLabel!
    @IBOutlet weak var cat_name: CustomLabel!
    @IBOutlet weak var exe_lev: CustomLabel!
    @IBOutlet weak var quik_pinch: CustomLabel!
    @IBOutlet var category_scroll: UIScrollView!
    @IBOutlet var quickPinch_TxtView: UITextView!
    @IBOutlet var expLevel_Btn: UIButton!
    @IBOutlet var hourly_Txt: UITextField!
    @IBOutlet var subCat_Btn: UIButton!
    @IBOutlet var mainCat_Btn: UIButton!
    @IBOutlet var addCatgry_Btn: CustomButton!
    @IBOutlet var title_Lbl: UILabel!
    
    @IBOutlet var hourlyRate_Lbl: CustomLabel!
    
    @IBOutlet weak var dynamicFieldTableVC: UITableView!
    @IBOutlet var selectCat_tbl: UITableView!
    var dropDown :  NIDropDown = NIDropDown()
    var category_Array : NSMutableArray = NSMutableArray()
    var category_IdArray: NSMutableArray = NSMutableArray()
    var experience_Array:NSMutableArray = NSMutableArray()
    var experienceID_Array:NSMutableArray = NSMutableArray()
    var subCategory_Array : NSMutableArray = NSMutableArray()
    var subCategory_IdArray : NSMutableArray = NSMutableArray()
    var checkCategory:String = String()
    var selectedCat_ID :String = String()
    var selectSubCat_ID:String = String()
    var selectExp_ID:String = String()
    var drop_Selection:String = String()
    var min_Rate:Int = Int()
    var editCatDtl_Array:NSMutableArray = NSMutableArray()
    var quickpitch_Array:NSMutableArray = NSMutableArray()
    var dynamicFieldsArray : NSMutableArray = NSMutableArray()
    var dynamicFieldValues : NSMutableArray = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        title_Lbl.text =  theme.setLang("edit_category")
        
        parent_cat.text=theme.setLang("Main_Category")
        selectCat_tbl.isHidden = true
        category_scroll.contentSize = CGSize(width: self.view.frame.size.width, height: self.expLevel_Btn.frame.origin.y+self.expLevel_Btn.frame.size.height+150)
        self.dynamicFieldTableVC.isScrollEnabled = false
        self.dynamicFieldTableVC.isHidden = true
        
        cat_name.text = theme.setLang("Sub_Category")
        //quik_pinch.text = theme.setLang("Quick_Pinch")
        exe_lev.text = theme.setLang("Experience_level")
        hourlyRate_Lbl.text = theme.setLang("Hourly_Rate")
        
        subCat_Btn.setTitle(theme.setLang("Select_Sub_Category"), for: UIControlState())
        mainCat_Btn.setTitle(theme.setLang("Select_Category"), for: UIControlState())
        expLevel_Btn.setTitle(theme.setLang("Select_Level"), for: UIControlState())
        hourly_Txt.placeholder = theme.setLang("Enter_Hourly_Rate")
        
        self.Done_Toolbar()
        //        self.showProgress()
        self.GetCategoryList()
        selectCat_tbl.layer.borderWidth = 1.0
        selectCat_tbl.layer.cornerRadius = 5
        selectCat_tbl.layer.borderColor = UIColor.darkGray.cgColor
        
        selectCat_tbl.register(UINib(nibName: "CategorySelectTableViewCell", bundle: nil), forCellReuseIdentifier: "CategorySelect")
        selectCat_tbl.delegate = self
        selectCat_tbl.dataSource = self
        dynamicFieldTableVC.register(UINib(nibName: "ExtraFieldsCell", bundle: nil), forCellReuseIdentifier: "extraFieldCell")
        dynamicFieldTableVC.delegate = self
        dynamicFieldTableVC.dataSource = self
        dynamicFieldTableVC.rowHeight = UITableViewAutomaticDimension
        dynamicFieldTableVC.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    func Done_Toolbar()
    {
        
        //ADD Done button for Contatct Field
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        doneToolbar.backgroundColor=UIColor.white
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: theme.setLang("done"), style: UIBarButtonItemStyle.done, target: self, action: #selector(CategoryEditVC.doneButtonAction))
        doneToolbar.items = [flexSpace,done]
        
        doneToolbar.sizeToFit()
        
        hourly_Txt.inputAccessoryView = doneToolbar
        
    }
    
    
    func doneButtonAction()
    {
        hourly_Txt.resignFirstResponder()
        
        
    }
    
    func GetCategoryList(){
        let parameters:Dictionary=["":""]
        
        // print(Param)
        
        url_handler.makeCall(get_mainCategory , param: parameters as NSDictionary) {
            (responseObject, error) -> () in
            self.DismissProgress()
            
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
            }
            else
            {
                if(responseObject != nil)
                {
                    let responseObject = responseObject!
                    let Dict:NSDictionary=responseObject
                    
                    
                    let Status=self.theme.CheckNullValue(Dict.object(forKey: "status") as AnyObject)! as NSString
                    if Status == "1"
                    {
                        self.category_Array.removeAllObjects()
                        self.category_IdArray.removeAllObjects()
                        self.experience_Array.removeAllObjects()
                        self.experienceID_Array.removeAllObjects()
                        
                        let ResponseArr = responseObject.object(forKey: "response")as! NSArray
                        let experience_Arr = responseObject.object(forKey: "experiencelist")as! NSArray
                        for experience in experience_Arr{
                            
                            let exp_ID  = self.theme.CheckNullValue((experience as AnyObject).object(forKey: "id") as AnyObject)
                            
                            let exp_Lvl = self.theme.CheckNullValue((experience as AnyObject).object(forKey: "name") as AnyObject)
                            
                            self.experience_Array.add(exp_Lvl!)
                            self.experienceID_Array.add(exp_ID!)
                            
                            //                        self.experience_Array.addObject(CategoryExperience.init(experience_Lvl: exp_Lvl!, experience_ID: exp_ID!))
                        }
                        
                        for (_, element) in ResponseArr.enumerated() {
                            
                            
                            let category = self.theme.CheckNullValue((element as AnyObject).object(forKey: "name") as AnyObject)!
                            let category_id = self.theme.CheckNullValue((element as AnyObject).object(forKey: "id") as AnyObject)!
                            self.category_Array.add(category)
                            self.category_IdArray.add(category_id)
                            
                        }
                    }
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
                    
                }
            }
            
        }
    }
    
    
    // pragma mark - MKDropdownMenuDataSource
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func didclick_BtnAction(_ sender: AnyObject) {
        self.navigationController?.popViewControllerWithFlip(animated: true)
    }
    
    //MARK:- Text Field Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        let textfield : UITextField = textField
        textfield.resignFirstResponder()
        
        return true
    }
    
    
    //MARK:- Text View Delegate
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    
    @IBAction func categoryBtn_Act(_ sender: AnyObject) {
        
        
        
        drop_Selection = "Main Category"
        if category_Array.count>0
        {
            selectCat_tbl.isHidden = false
            selectCat_tbl.reload()
            
            
            selectCat_tbl.frame = CGRect(x: self.mainCat_Btn.frame.origin.x,y: self.mainCat_Btn.frame.origin.y+self.subCat_Btn.frame.size.height+5, width: self.mainCat_Btn.frame.size.width, height: self.expLevel_Btn.frame.minY - 20)
            let tableCellHeight = 44
            selectCat_tbl.frame.size.height = CGFloat(category_Array.count*tableCellHeight)
            if selectCat_tbl.frame.size.height > 250{
                selectCat_tbl.frame.size.height = 250
            }
            
        }
        else{
            self.view.makeToast(message: "There are no main category to show", duration: 3, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
        }
        
    }
    
    func niDropDownDelegateMethod(_ sender: NIDropDown!, _ position: Int) {
        
        if drop_Selection == "Main Category"{
            print("\(sender.tag)")
            //        subCat_Btn.setTitle("Select Sub Category", forState: .Normal)
            selectedCat_ID =  self.category_IdArray[position] as! String
            subCat_API(selectedCat_ID as String)
        }
        else if drop_Selection == "Sub Category"{
            
            print("\(sender.tag)")
            // subCat_Btn.setTitle("Select Sub Category", forState: .Normal)
            selectSubCat_ID =  self.subCategory_IdArray[position]  as! String
            getSubcategory_Dtls(selectSubCat_ID)
            // subCat_API(selectedCat_ID as String)
            
            
        }
        else if drop_Selection == "Experience Category"{
            selectExp_ID =  self.experienceID_Array[position]  as! String
        }
    }
    
    
    //    func niDropDownDelegateMethod(sender: NIDropDown!,position:Int) {
    //
    //        print("\(sender.tag)")
    //        print(mainCat_Btn.titleLabel!.text!)
    //
    //        category_Array.containsObject(mainCat_Btn.titleLabel!.text!)
    //        selectedCat_ID =  self.category_IdArray[sender.tag] as! NSString
    //        subCat_API(selectedCat_ID as String)
    //
    //    //
    //    }
    
    func getSubcategory_Dtls(_ subCat_ID:String){
        let _:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["subcategory_id":"\(subCat_ID)"]
        url_handler.makeCall(getSubCategory_Dtl, param: Param as NSDictionary) {
            (responseObject, error) -> () in
            
            self.DismissProgress()
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: "Network Failure !!!")
            }
            else
            {
                if(responseObject != nil && (responseObject?.count)!>0)
                {
                    let responseObject = responseObject!
                    let status=self.theme.CheckNullValue(responseObject.object(forKey: "status") as AnyObject)! as NSString
                    
                    if(status == "1")
                    {
                        let response = responseObject.object(forKey: "response") as! NSArray
                        if  response.count>0{
                            for responseDtl in response{
                                let getMin_Rate = self.theme.CheckNullValue((responseDtl as AnyObject).object(forKey: "minrate") as AnyObject)
                                let quickpitch = self.theme.CheckNullValue((responseDtl as AnyObject).object(forKey: "quickpitch") as AnyObject)
                                self.min_Rate = Int(getMin_Rate!)!
                                self.hourlyRate_Lbl.text = "Hourly rate (Min rate \(self.theme.getappCurrencycode())\(self.min_Rate))"
                                self.quickpitch_Array.add(quickpitch)
                            }
                            self.updateDynamicFields()
                        }
                    }
                    else
                    {
                        self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: "Network Failure !!!")
                    }
                }
                
            }
            
        }
        
    }
    
    func updateDynamicFields() {
        let quickPitch = self.quickpitch_Array.object(at: 0) as! NSString
        self.quickpitch_Array.removeAllObjects()
        self.dynamicFieldsArray.removeAllObjects()
        let arr : NSArray = quickPitch.components(separatedBy: "\n") as NSArray
        dynamicFieldsArray.addObjects(from: arr as! [Any])
        if self.dynamicFieldsArray.count > 0 {
            self.dynamicFieldTableVC.isHidden = false
            category_scroll.contentSize = CGSize(width: self.view.frame.size.width, height: CGFloat(self.dynamicFieldTableVC.frame.origin.y +  CGFloat(self.dynamicFieldsArray.count*80) + 20.0 ))
            self.dynamicFieldTableVC.frame = CGRect(x: 0, y: self.dynamicFieldTableVC.frame.origin.y, width: self.view.frame.size.width, height: CGFloat(self.dynamicFieldsArray.count*80))
            self.dynamicFieldTableVC.reloadData()
        }
    }
    
    
    
    func subCat_API(_ selectedCat_ID:String){
        
        let parameters:Dictionary=["category_id":"\(selectedCat_ID)"]
        
        // print(Param)
        
        url_handler.makeCall(Get_SubCategory , param: parameters as NSDictionary) {
            (responseObject, error) -> () in
            self.DismissProgress()
            
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
            }
            else
            {
                if(responseObject != nil)
                {
                    let responseObject = responseObject!
                    let Dict:NSDictionary=responseObject
                    
                    
                    let Status=self.theme.CheckNullValue(Dict.object(forKey: "status") as AnyObject)! as NSString
                    if Status == "1"
                    {
                        self.subCategory_Array.removeAllObjects()
                        self.subCategory_IdArray.removeAllObjects()
                        let ResponseArr = responseObject.object(forKey: "response")as! NSArray
                        
                        for (_, element) in ResponseArr.enumerated() {
                            
                            let category = self.theme.CheckNullValue((element as AnyObject).object(forKey: "name") as AnyObject)!
                            let category_id = self.theme.CheckNullValue((element as AnyObject).object(forKey: "id") as AnyObject)!
                            self.subCategory_Array.add(category)
                            self.subCategory_IdArray.add(category_id)
                        }
                        
                    }
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
                    
                }
            }
            
        }
        
        
    }
    @IBAction func subCategoryBtn_Act(_ sender: AnyObject) {
        //         sender.hideDropDown(mainCat_Btn)
        //        sender.hideDropDown(subCat_Btn)
        //        if  dropDown.hidden != true {
        //             dropDown.hideDropDown(sender as! UIButton)
        //        }
        drop_Selection = "Sub Category"
        if subCategory_Array.count>0
        {
            selectCat_tbl.isHidden = false
            selectCat_tbl.reload()
            
            selectCat_tbl.frame = CGRect(x: self.subCat_Btn.frame.origin.x,y: self.subCat_Btn.frame.origin.y+self.subCat_Btn.frame.size.height, width: self.subCat_Btn.frame.size.width, height: self.subCat_Btn.frame.maxY)
            let tableViewCellHeight = 44
            selectCat_tbl.frame.size.height = CGFloat(subCategory_Array.count*tableViewCellHeight)
            if selectCat_tbl.frame.size.height > 250{
                selectCat_tbl.frame.size.height = 250
            }
        }
        else{
            self.view.makeToast(message: "There are no subcategory to show", duration: 3, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
        }
    }
    
    @IBAction func expLevel_Action(_ sender: UIButton) {
        //        if  dropDown.hidden != true {
        //            dropDown.hideDropDown(sender as! UIButton)
        //        }
        drop_Selection = "Experience Category"
        if experience_Array.count>0
        {
            selectCat_tbl.isHidden = false
            selectCat_tbl.reload()
            
            selectCat_tbl.frame = CGRect(x: self.expLevel_Btn.frame.origin.x,y: self.expLevel_Btn.frame.origin.y+self.subCat_Btn.frame.size.height, width: self.expLevel_Btn.frame.size.width, height: self.subCat_Btn.frame.maxY-40)
            
        }
    }
    
    
    @IBAction func addCatgry_Action(_ sender: UIButton) {
        
       
        var fullString : String = ""
        
        for (index, val) in dynamicFieldsArray.enumerated() {
            
            print("value\(val as! String), Index : \(index)")
            let indexPath : NSIndexPath = NSIndexPath(row: index, section: 0)
            let cell : ExtraFieldsCell = self.dynamicFieldTableVC.cellForRow(at: indexPath as IndexPath) as! ExtraFieldsCell
            guard let data : String = cell.extraField.text as! String else {
                continue
            }
            if fullString == "" {
                fullString = "\(val as! String) \(data)"
            } else {
                fullString += "\r\n \(val as! String) \(data)"
            }
        }
        
        let rate = Int(hourly_Txt.text!)
        if mainCat_Btn.titleLabel!.text! == "Select Category"{
            
            self.theme.AlertView("\(appNameJJ)", Message:theme.setLang("Please select main category"), ButtonTitle: kOk)
            
            
        }
            
        else if subCat_Btn.titleLabel!.text! == "Select Sub Category"{
            self.theme.AlertView("\(appNameJJ)", Message:theme.setLang("Please select sub category"), ButtonTitle: kOk)
            
            
        }/*
        else if quickPinch_TxtView.text == "" ||  quickPinch_TxtView.text == "Enter quick pitch"{
            self.theme.AlertView("\(appNameJJ)", Message:theme.setLang("Quick Pinch field was empty"), ButtonTitle: kOk)
            
        }*/
        else if hourly_Txt.text == "" {
            self.theme.AlertView("\(appNameJJ)", Message:theme.setLang("Hourly Rate is required"), ButtonTitle: kOk)
        }
        else if rate!<min_Rate{
            self.theme.AlertView("\(appNameJJ)", Message:"\(theme.setLang("Minimun hourly rate is")) \(min_Rate)", ButtonTitle: kOk)
            
        }
        else if expLevel_Btn.titleLabel!.text! == "Select Level"{
            self.theme.AlertView("\(appNameJJ)", Message:theme.setLang("Kindly Select your Experience Level."), ButtonTitle: kOk)
        }
        else{
            // let parameters:Dictionary=["category_id":"\(selectedCat_ID)"]
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            let parameters:Dictionary=["tasker":"\(objUserRecs.providerId)","quickpitch":"\(fullString)","childid":"\(selectSubCat_ID)","parentcategory":"\(selectedCat_ID)","hourrate":"\(hourly_Txt.text!)","experience":"\(selectExp_ID)"]
            
            // print(Param)
            var url:String = String()
            if  checkCategory ==  "Edit Category"{
                url =  update_Category
            }
            else{
                url =  add_Category
            }
            url_handler.makeCall(url , param: parameters as NSDictionary) {
                (responseObject, error) -> () in
                self.DismissProgress()
                
                if(error != nil)
                {
                    self.theme.AlertView("\(appNameJJ)"  , Message:HRToastPositionDefault, ButtonTitle: kOk)
                }
                else
                {
                    if(responseObject != nil)
                    {
                        let responseObject = responseObject!
                        let Dict:NSDictionary=responseObject
                        
                        
                        let Status=self.theme.CheckNullValue(Dict.object(forKey: "status") as AnyObject)! as NSString
                        if Status == "1"
                        {
                            let getResponse = responseObject.object(forKey: "response") as! String
                            self.theme.AlertView("\(appNameJJ)", Message:getResponse, ButtonTitle: kOk)
                            
                            self.navigationController?.popViewControllerWithFlip(animated: true)
                            
                            
                        }
                        else{
                            let getResponse = responseObject.object(forKey: "response") as! String
                            self.theme.AlertView("\(appNameJJ)", Message:getResponse, ButtonTitle: kOk)
                        }
                    }
                    else
                    {
                        self.theme.AlertView("\(appNameJJ)", Message:kErrorMsg, ButtonTitle: kOk)
                    }
                }
                
            }
        }
        
    }
    
    
    func setEditCat_Details(){
        
        mainCat_Btn.isUserInteractionEnabled = false
        subCat_Btn.isUserInteractionEnabled = false
        addCatgry_Btn.setTitle("\(Language_handler.VJLocalizedString("update", comment: nil))", for: UIControlState())
        let getCurrentEdit_Dtl = editCatDtl_Array[0] as! EditCategoryDetails
        mainCat_Btn.setTitle(getCurrentEdit_Dtl.mainCat_Name, for: UIControlState())
        subCat_Btn.setTitle(getCurrentEdit_Dtl.subCat_Name, for: UIControlState())
        //quickPinch_TxtView.text = getCurrentEdit_Dtl.quickPinch
        min_Rate = Int(getCurrentEdit_Dtl.min_Rate)!
        self.hourlyRate_Lbl.text = "Hourly rate (Min rate \(theme.getappCurrencycode())\(self.min_Rate))"
        //  hourly_Txt.placeholder = "Min rate $\(getCurrentEdit_Dtl.min_Rate)"
        
        let quickpitch  = getCurrentEdit_Dtl.quickPinch
        if quickpitch.count != 0 {
            self.dynamicFieldValues.removeAllObjects()
            self.dynamicFieldsArray.removeAllObjects()
            let quickarry : NSArray = quickpitch.components(separatedBy: "\n") as NSArray
            for val in quickarry.enumerated() {
                let quickVal : String = val.element as! String
                let separeatedVals : NSArray = quickVal.components(separatedBy: ":") as NSArray
                if separeatedVals.count >= 2 {
                  self.dynamicFieldsArray.add("\(separeatedVals.object(at: 0)):")
                  self.dynamicFieldValues.add(separeatedVals.object(at: 1))
                }
            }
            if self.dynamicFieldsArray.count > 0 {
                self.dynamicFieldTableVC.isHidden = false
                category_scroll.contentSize = CGSize(width: self.view.frame.size.width, height: CGFloat(self.dynamicFieldTableVC.frame.origin.y +  CGFloat(self.dynamicFieldsArray.count*80) + 20.0 ))
                self.dynamicFieldTableVC.frame = CGRect(x: 0, y: self.dynamicFieldTableVC.frame.origin.y, width: self.view.frame.size.width, height: CGFloat(self.dynamicFieldsArray.count*80))
                self.dynamicFieldTableVC.reloadData()
            }
            
        }
        
        hourly_Txt.text = getCurrentEdit_Dtl.hour_Rate
        expLevel_Btn.setTitle(getCurrentEdit_Dtl.expereince_Name, for: UIControlState())
        selectExp_ID = getCurrentEdit_Dtl.experience_ID
        selectSubCat_ID = getCurrentEdit_Dtl.subCat_ID
        selectedCat_ID = getCurrentEdit_Dtl.mainCat_ID
        
    }
    
    func setAddCatTxtAlign(){
        
        addCatgry_Btn.setTitle(Language_handler.VJLocalizedString("submit", comment: nil), for: UIControlState())
        mainCat_Btn.isUserInteractionEnabled = true
        subCat_Btn.isUserInteractionEnabled = true
        //  title_Lbl.text = "Add Category"
       // quickPinch_TxtView.text = Language_handler.VJLocalizedString("pinch_manditory", comment: nil)
        
        //quickPinch_TxtView.textColor = UIColor.lightGray
        
    }
    
    func setUI_Alignments(){
        
        // quickPinch_TxtView.layer.borderWidth = 1
        //quickPinch_TxtView.layer.cornerRadius = 8
        hourly_Txt.layer.cornerRadius = 8
        hourly_Txt.layer.borderWidth = 1
        mainCat_Btn.layer.cornerRadius = 8
        mainCat_Btn.layer.borderWidth = 1
        mainCat_Btn.layer.borderColor = PlumberThemeColor.cgColor
        subCat_Btn.layer.cornerRadius = 8
        subCat_Btn.layer.borderWidth = 1
        subCat_Btn.layer.borderColor = PlumberThemeColor.cgColor
        
        
        expLevel_Btn.layer.borderWidth = 1
        expLevel_Btn.layer.cornerRadius = 8
        expLevel_Btn.layer.borderColor = PlumberThemeColor.cgColor
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUI_Alignments()
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: hourly_Txt.frame.size.height))
        let curency_lbl: UILabel = UILabel(frame: CGRect(x: 10, y: 0, width: 15, height: hourly_Txt.frame.size.height))
        
        curency_lbl.text = theme.getappCurrencycode()
        paddingView.addSubview(curency_lbl)
        
        hourly_Txt.leftView = paddingView
        hourly_Txt.leftViewMode = UITextFieldViewMode.always;
        if checkCategory ==  "Edit Category"{
            
            setEditCat_Details()
            
        }
        else{
            setAddCatTxtAlign()
        }
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = Language_handler.VJLocalizedString("pinch_manditory", comment: nil)
            
            textView.textColor = UIColor.black
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == dynamicFieldTableVC {
            
            return dynamicFieldsArray.count
            
        } else {
            
            if  drop_Selection == "Main Category"
            {
                if category_Array.count>0{
                    return self.category_Array.count
                }
                else{
                    return 0
                }
                
            }
            else if drop_Selection == "Sub Category"{
                if subCategory_Array.count>0{
                    return  self.subCategory_Array.count
                }
                else{
                    return 0
                }
            }
            else if  drop_Selection == "Experience Category"
            {
                
                if experience_Array.count>0{
                    return  self.experience_Array.count
                }
                else{
                    return 0
                }
            }
            else{
                return 0
                
            }
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == dynamicFieldTableVC {
            let Cell = tableView.dequeueReusableCell(withIdentifier: "extraFieldCell") as! ExtraFieldsCell
            Cell.selectionStyle = UITableViewCellSelectionStyle.none
            Cell.extraField.layer.borderWidth = 1.0
            Cell.extraField.layer.cornerRadius = 3
            Cell.extraField.layer.borderColor = PlumberThemeColor.cgColor
            Cell.extraField.layer.masksToBounds = true
            Cell.titleLbl.text = self.dynamicFieldsArray.object(at: indexPath.row) as! String
            Cell.extraField.delegate = self
            if self.dynamicFieldValues.count > 0 {
                Cell.extraField.text = self.dynamicFieldValues.object(at: indexPath.row) as! String
            }
            
            return Cell
            
        } else {
            
            let Cell = tableView.dequeueReusableCell(withIdentifier: "CategorySelect") as! CategorySelectTableViewCell
            if  drop_Selection == "Main Category"
            {
                if category_Array.count>0{
                    Cell.categorylable.text = category_Array.object(at: indexPath.row) as? String
                }
                else{
                    return Cell
                }
            }
            else if drop_Selection == "Sub Category"{
                if subCategory_Array.count>0{
                    Cell.categorylable.text = subCategory_Array.object(at: indexPath.row) as? String
                }
                else{
                    return Cell
                }
            }
            else if  drop_Selection == "Experience Category"
            {
                if experience_Array.count>0{
                    Cell.categorylable.text = experience_Array.object(at: indexPath.row) as? String
                }
                else{
                    return Cell
                }
                
            }
            return Cell
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        if  drop_Selection == "Main Category"
        {
            mainCat_Btn.setTitle("\(category_Array.object(at: indexPath.row))", for:UIControlState())
            selectCat_tbl.isHidden = true
            selectedCat_ID = category_IdArray.object(at: indexPath.row) as! String
            subCat_API(category_IdArray.object(at: indexPath.row) as! String)
        }
        else if drop_Selection == "Sub Category"{
            subCat_Btn.setTitle("\(subCategory_Array.object(at: indexPath.row))", for:UIControlState())
            selectCat_tbl.isHidden = true
            selectSubCat_ID = subCategory_IdArray.object(at: indexPath.row) as! String
            getSubcategory_Dtls(subCategory_IdArray.object(at: indexPath.row) as! String)
            
            
        }
        else if  drop_Selection == "Experience Category"
        {
            expLevel_Btn.setTitle("\(experience_Array.object(at: indexPath.row))", for:UIControlState())
            selectExp_ID = experienceID_Array.object(at: indexPath.row) as! String
            selectCat_tbl.isHidden = true
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == dynamicFieldTableVC {
            return 80
        } else {
            return 40
        }
        return 0.0
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view : UIView = UIView()
        view.backgroundColor = UIColor.white
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    //    func textViewDidBeginEditing(textView: UITextView) {
    //        code
    //    }
    /*
     
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
