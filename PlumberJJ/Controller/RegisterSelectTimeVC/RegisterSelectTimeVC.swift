//
//  RegisterSelectTimeVC.swift
//  PlumberJJ
//
//  Created by casperon_macmini on 19/07/17.
//  Copyright © 2017 Casperon Technologies. All rights reserved.
//

import UIKit

class RegisterSelectTimeVC: UIViewController {
    
    @IBOutlet var continue_Btn: CustomButton!
    @IBOutlet var day_View: UIView!
    @IBOutlet var sunday_Btn:UIButton!
      @IBOutlet var monday_Btn:UIButton!
      @IBOutlet var tuesday_Btn:UIButton!
      @IBOutlet var wed_Btn:UIButton!
      @IBOutlet var thur_Btn:UIButton!
      @IBOutlet var fri_Btn:UIButton!
    
      @IBOutlet var sat_Btn:UIButton!
   @IBOutlet var scroll_View: UIScrollView!
    @IBOutlet weak var title_lbl: CustomLabelHeader!
    
    
    var selectArray:NSMutableArray = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title_lbl.text = theme.setLang("register")
        //selectArray = ["SUN","MON","","","","",""]
        // Do any additional setup after loading the view.
    }
    
    func setView(){
        
        sunday_Btn.layer.cornerRadius =  sunday_Btn.frame.width/10
        monday_Btn.layer.cornerRadius =  monday_Btn.frame.width/10
        tuesday_Btn.layer.cornerRadius =  tuesday_Btn.frame.width/10
        wed_Btn.layer.cornerRadius =  wed_Btn.frame.width/10
        thur_Btn.layer.cornerRadius =  thur_Btn.frame.width/10
        fri_Btn.layer.cornerRadius =  fri_Btn.frame.width/10
        sat_Btn.layer.cornerRadius =  sat_Btn.frame.width/10
    }
    
    @IBAction func selectBtn_Action(_ sender: UIButton) {
        
        if selectArray.contains(sender.titleLabel!.text!){
            
            sender.backgroundColor = UIColor.lightGray
            sender.setTitleColor(UIColor.black, for: UIControlState())
            selectArray.remove(sender.titleLabel!.text!)
        }
            
        else{
            
            sender.backgroundColor = PlumberBlueColor
            //sender.setTitleColor(UIColor.white, forState: .Normal)
            selectArray.add(sender.titleLabel!.text!)

        }
        
    }
    @IBAction func continueBtn_Action(_ sender: UIButton) {
  
    }
    
    @IBAction func backBtn_Action(_ sender: UIButton) {
         self.navigationController?.popViewControllerWithFlip(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        setView()
        scroll_View.contentSize = CGSize(width: UIScreen.main.bounds.width,height: continue_Btn.frame.maxY)

    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
