//
//  HomeViewController.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 10/27/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit
import CoreLocation
import MessageUI




class HomeViewController: RootBaseViewController,MFMailComposeViewControllerDelegate {
   
    @IBOutlet var btn_leads: UIButton!
    @IBOutlet var btn_jobs: UIButton!
    @IBOutlet var btn_statistics: UIButton!
    @IBOutlet var btn_support: UIButton!
    
    @IBOutlet var chaticon: UIButton!
    @IBOutlet var availability: UISwitch!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var panicBtn: UIButton!
    @IBOutlet weak var bottomView: CSAnimationView!
    @IBOutlet weak var top_View: CSAnimationView!
    @IBOutlet weak var homeScrollView: UIScrollView!
    @IBOutlet weak var topView: SetColorView!
   
    @IBOutlet weak var bgImgView: UIImageView!
    
    @IBOutlet weak var lblStatistic: UILabel!
    @IBOutlet weak var lblMyLeads: UILabel!
    @IBOutlet weak var lblMyJob: UILabel!
    @IBOutlet weak var lblSupport: UILabel!
    
    @IBOutlet weak var title_lbl: UILabel!
    // var theme:Theme=Theme()
    var lat:String = ""
    var lon:String = ""
    var available_status : String = ""
    var GetReceipientMail : String = ""
    
    var themes:Theme = Theme()
    
    
    @IBAction func didClickMyOrdersBtn(_ sender: UIButton) {
        
        let objMyJobsVc = self.storyboard!.instantiateViewController(withIdentifier: "MyJobsVCSID") as! MyJobsViewController
        self.navigationController!.pushViewController(withFlip: objMyJobsVc, animated : true)
 
    }
    
    @IBAction func didClickChatBtn(_ sender: AnyObject) {
        let objChatVc = self.storyboard!.instantiateViewController(withIdentifier: "ChatListVcSID") as! ChatListViewController
        self.navigationController!.pushViewController(withFlip: objChatVc, animated : true)

//        self.navigationController!.pushViewController(withFlip: objChatVc, animated: true)
    }
    
    
    @IBAction func didClickSupport(_ sender: AnyObject) {
       
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
 
    }
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["\(self.GetReceipientMail)"])
        mailComposerVC.setSubject(appNameJJ)
        
        
        return mailComposerVC
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    func  getAppinformation()
    {
        
        let URL_Handler:URLhandler=URLhandler()
        URL_Handler.makeCall(Appinfo_url, param: [:]) {
            (responseObject, error) -> () in
            
            if(error != nil)
            {
                
            }
            else
            {
                if(responseObject != nil && (responseObject?.count)!>0)
                {
                    let responseObject = responseObject!
                    let status=self.theme.CheckNullValue(responseObject.object(forKey: "status"))!
                    if(status == "1")
                    {
                        
                        self.GetReceipientMail = self.theme.CheckNullValue(responseObject.object(forKey: "email_address"))!
                        
                        
                    }
                    else
                    {
                    }
                    
                    
                }
            }
        }
        
    }
    
    
    
    
    func showSendMailErrorAlert() {
        
        self.theme.AlertView(Language_handler.VJLocalizedString("not_send_email_title", comment: nil), Message: Language_handler.VJLocalizedString("not_send_email", comment: nil), ButtonTitle: kOk)
        
        
    }
    
    @IBAction func didClickStatisticsBtn(_ sender: UIButton) {
        
        let objMyOrderVc = self.storyboard!.instantiateViewController(withIdentifier: "StatisticsVCSID") as! StatisticsViewController
        self.navigationController!.pushViewController(withFlip: objMyOrderVc, animated: true)
 
    }
    @IBAction func didClickNotificationBtn(_ sender: UIButton) {
        
        let objMyOrderVc = self.storyboard!.instantiateViewController(withIdentifier: "NewLeadsVCSID") as! NewLeadsViewController
        self.navigationController!.pushViewController(withFlip: objMyOrderVc, animated: true)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.UpdateAvailabilityStatus()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getAppinformation()
        self.bottomView.SpringAnimations()
        self.top_View.SpringAnimations()
        

        self.title_lbl.expand(into: self.view, finished: nil)
        
        lblMyJob.text = Language_handler.VJLocalizedString("my_jobs", comment: nil)
        lblStatistic.text = Language_handler.VJLocalizedString("statistics", comment: nil)
        lblMyLeads.text = Language_handler.VJLocalizedString("new_leads", comment: nil)
        lblSupport.text = Language_handler.VJLocalizedString("support", comment: nil)
        
        btn_leads.pulse {
            self.btn_jobs.pulse {
                self.btn_statistics.pulse {
                    self.btn_support.pulse {
                    }
                }
            }
        }
        lblMyLeads.pulse {
            self.lblMyJob.pulse {
                self.lblStatistic.pulse {
                    self.lblSupport.pulse {
                    }
                }
            }
        }
        
        
        
        



       
        
       


        self.title_lbl.text=Language_handler.VJLocalizedString( (appNameJJ), comment: nil)
        
        menuButton.addTarget(self, action: #selector(HomeViewController.openmenu), for: .touchUpInside)
        
        
        homeScrollView.contentSize=CGSize(width: homeScrollView.frame.size.width, height: bottomView.frame.origin.y + bottomView.frame.size.height + 50)
        
        
        /*  let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.ExtraLight)
         let blurView = UIVisualEffectView(effect: blurEffect)
         blurView.frame = CGRectMake(topView.frame.origin.x, 0, bgImgView.frame.size.width, homeScrollView.frame.size.height);
         self.bgImgView.addSubview(blurView)
         if theme.getAvailable_satus() == ""
         {
         //self.avialabilitybtn.setTitle("GO ONLINE", forState:.Normal)
         availability.on = false
         }
         else
         {
         if theme.getAvailable_satus() == "GO ONLINE"
         {
         //  self.avialabilitybtn.setTitle("GO ONLINE", forState:.Normal)
         availability.on = false
         }
         else
         {
         availability.on = true
         //self.avialabilitybtn.setTitle("GO OFFLINE", forState:.Normal)
         }
         }*/
    }
    
    override func applicationLanguageChangeNotification(_ notification: Notification) {
        
        lblMyJob.text = Language_handler.VJLocalizedString("my_jobs", comment: nil)
        lblStatistic.text = Language_handler.VJLocalizedString("statistics", comment: nil)
        lblMyLeads.text = Language_handler.VJLocalizedString("new_leads", comment: nil)
        lblSupport.text = Language_handler.VJLocalizedString("support", comment: nil)
        
    }
    
    func openmenu(){
        self.view.endEditing(true)
        self.frostedViewController.view.endEditing(true)
        // Present the view controller
        //
        self.frostedViewController.presentMenuViewController()
    }
   
    
   
    
    
    
    func UpdateAvailabilityStatus(){
        
        
     
        
        
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)"]
        // print(Param)
        
        url_handler.makeCall(GettingAvailablty, param: Param as NSDictionary) {
            (responseObject, error) -> () in
            
            
            
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
            }
            else
            {
                if(responseObject != nil && (responseObject?.count)!>0)
                {
                    let responseObject = responseObject!
                    let status=self.theme.CheckNullValue(responseObject.object(forKey: "status"))!
                    let CheckAvailability=self.theme.CheckNullValue(responseObject.object(forKey: "availability"))!
                    if(status == "1")
                    {
                        if CheckAvailability ==  "1"
                            
                        {
                            self.availability.setOn(true, animated: true)
                        }
                        else
                        {

                            self.availability.setOn(false, animated: true)
                            
                        }
                        
                    }
                    else
                    {
                        
                    }
                    
                    
                }
            }
        }
        
    }
    func upDateLocation(){
        
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)",
                                 "latitude":"\(lat)",
                                 "longitude":"\(lon)"]
        // print(Param)
        
        url_handler.makeCall(updateProviderLocation, param: Param as NSDictionary) {
            (responseObject, error) -> () in
            
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
            }
            else
            {
                if(responseObject != nil && (responseObject?.count)!>0)
                {
                    let responseObject = responseObject!
                    let status = self.theme.CheckNullValue(responseObject.object(forKey: "status"))!
                    if(status == "1")
                    {
                        
                        
                    }
                    else
                    {
                        self.view.makeToast(message:kErrorMsg, duration: 5, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
                    }
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
                }
            }
            
        }
    }
    
    
    @IBAction func didclickAvailablityStatus(_ sender: UISwitch) {
        
        if sender.isOn {
            available_status = "1"
        } else {
            available_status = "0"
        }
        showProgress()
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["tasker":"\(objUserRecs.providerId)","availability" :available_status]
        // print(Param)
        
        url_handler.makeCall(EnableAvailabilty, param: Param as NSDictionary) {
            (responseObject, error) -> () in
            
            self.DismissProgress()
            
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
            }
            else
            {
                if(responseObject != nil && (responseObject?.count)!>0)
                {
                    let responseObject = responseObject!
                    let status=self.theme.CheckNullValue(responseObject.object(forKey: "status"))!
                    if(status == "1")
                    {
                        let resDict: NSDictionary = responseObject.object(forKey: "response") as! NSDictionary
                        let tasker_status : String = self.theme.CheckNullValue(resDict.object(forKey: "tasker_status"))!
                        
                        
                        if (tasker_status == "1")
                        {
                            self.view.makeToast(message:Language_handler.VJLocalizedString("your_avilability_on", comment: nil), duration: 3, position: HRToastPositionDefault as AnyObject, title: "\(appNameJJ)")

                            self.availability.isOn = true
                            // self.avialabilitybtn.setTitle("GO OFFLINE", forState:.Normal)
                            self.theme.saveAvailable_satus("GO OFFLINE")
                            
                            
                        }
                        else
                        {self.availability.isOn = false
                            self.view.makeToast(message:Language_handler.VJLocalizedString("your_avilability_off", comment: nil), duration: 3, position: HRToastPositionDefault as AnyObject, title: "\(appNameJJ)")

                            //  self.avialabilitybtn.setTitle("GO ONLINE", forState:.Normal)
                            self.theme.saveAvailable_satus("GO ONLINE")
                            
                            
                        }
                        
                    }
                    else
                    {
                        self.view.makeToast(message:kErrorMsg, duration: 5, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
                    }
                    
                    
                }
            }
        }
        
        
    }
    
    
    @IBAction func didclickavailability(_ sender: AnyObject) {
        
        
        if  sender.titleLabel?!.text == "GO ONLINE"
        {
            available_status = "1"
        }
        else
        {
            available_status = "0"
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

//    func updateViewAnimations(){
//        self.bottomView.transform = CGAffineTransform(scaleX: 0, y: 0);
//        self.top_View.transform = CGAffineTransform(scaleX: 0, y: 0);
//        UIView.animate(withDuration: 1.5, delay: 0.05, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
//            self.top_View.transform = CGAffineTransform(scaleX: 1, y: 1);
//            self.bottomView.transform = CGAffineTransform(scaleX: 1, y: 1);
//        }) { (finished) in
//            print("animation completed")
//        }
//
//    }



}
