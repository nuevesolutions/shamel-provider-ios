//
//  SelectCategoryViewController.swift
//  PlumberJJ
//
//  Created by Casperon iOS on 04/10/17.
//  Copyright © 2017 Casperon Technologies. All rights reserved.
//

import UIKit
protocol SelectCategoryDelegate: class {
    func changeBackgroundColor(_ array:NSMutableArray)
   
}

class SelectCategoryViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
   
    @IBOutlet weak var title_lbl: CustomLabelHeader!
    
    @IBOutlet weak var parent_Category_lbl: UILabel!
    @IBOutlet weak var parentCategory_textField: UITextField!
    
    @IBOutlet weak var sub_Category_lbl: UILabel!
    
    @IBOutlet weak var sub_Category_txtField: UITextField!
    
    @IBOutlet weak var quickPitch_lbl: UILabel!
    
    @IBOutlet weak var quikPitch_textField: UITextField!
    
    @IBOutlet weak var setHourlyRate_lbl: UILabel!
    
    @IBOutlet weak var setHourlyRate_textField: UITextField!
    @IBOutlet weak var levelOfExperience_lbl: UILabel!
    
    @IBOutlet weak var levelOfExperience_textField: UITextField!
    @IBOutlet weak var categorylist_tableView: UITableView!
    
    @IBOutlet weak var selectExperience: UITableView!
     @IBOutlet var scroll_View: UIScrollView!
    
    
     weak var delegate: SelectCategoryDelegate?
     var URL_handler:URLhandler=URLhandler()
    var theme:Theme=Theme()
    var spinnerView : MMMaterialDesignSpinner?
    var spinnerViews : MMMaterialDesignSpinner?

    
    
    var category:String = String()
    var categoryArray:[String]=[String]()
    var categoryArrayID:[String] = [String]()
    var subCategoryArray:[String] = [String]()
    var subCategoryArrayID:[String] = [String]()
    var searchArray = [String]()
    var selectExperienceArray=[String]()
    var selectExperienceArrayID=[String]()
    var down_btn:UIButton!
    var down_btn1:UIButton!
    var parent_id: String = String()
    var childId:String=String()
    var catArray:NSMutableArray = NSMutableArray()
    var subCatArray:NSMutableArray = NSMutableArray()
     
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.theme.SetPaddingView(parentCategory_textField)
         self.theme.SetPaddingView(sub_Category_txtField)
         self.theme.SetPaddingView(quikPitch_textField)
         self.theme.SetPaddingView(setHourlyRate_textField)
         self.theme.SetPaddingView(levelOfExperience_textField)
        
        
        categorylist_tableView.isHidden = true
        selectExperience.isHidden = true
        parentCategory_textField.addTarget(self, action: #selector(TextfieldDidChange(_:)), for: UIControlEvents.editingChanged)
        
//        down_btn.setImage(UIImage(named: "DownArrow.png"), forState: .Normal)
//        down_btn.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0)
//        down_btn.frame = CGRect(x: parentCategory_textField.frame.maxX-25, y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
//        down_btn.addTarget(self, action: #selector(self.down), forControlEvents: .TouchUpInside)
//        parentCategory_textField.rightView = down_btn
//        parentCategory_textField.rightViewMode = .Always
        sub_Category_txtField.addTarget(self, action: #selector(TextfieldDidChange(_:)), for: UIControlEvents.editingChanged)
//        down_btn1.setImage(UIImage(named: "DownArrow.png"), forState: .Normal)
//        down_btn1.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0)
//        down_btn1.frame = CGRect(x: parentCategory_textField.frame.maxX-25, y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
//        down_btn1.addTarget(self, action: #selector(self.down), forControlEvents: .TouchUpInside)
//        sub_Category_txtField.rightView = down_btn
//        sub_Category_txtField.rightViewMode = .Always
        
        // geting value of Category and SubCategory
        let parameter : NSDictionary = NSDictionary()
        self.showProgress()
        URL_handler.makeCall("\(catergory_Url)" , param: parameter, completionHandler: { (responseObject, error) -> () in
            
            if(error != nil){
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault as AnyObject, title: Appname)
            } else {
                if(responseObject != nil){
                    let dict:NSDictionary=responseObject!
                    let status = self.theme.CheckNullValue(dict.object(forKey: "status") as AnyObject)!
                    let response = dict.object(forKey: "response") as! NSArray
                    if(status == "1")
                    {
                        for i in response {
                            let qestion = i as! NSMutableDictionary
                            let cat: Catergory = Catergory()
                            self.categoryArray.append("\(qestion.object(forKey: "name") as! String)")
                            cat.name = "\(qestion.object(forKey: "name") as! String)"
                            self.categoryArrayID.append("\(qestion.object(forKey: "_id") as! String)")
                            cat.id = "\(qestion.object(forKey: "_id") as! String)"
                            self.catArray.add(cat)
                        }
                        
                        
                        self.categorylist_tableView.reload()
                        
                        
                    }
                    else{
                        self.theme.AlertView("\(Appname)", Message: "\(self.theme.CheckNullValue(dict.object(forKey: "response") as AnyObject)!)", ButtonTitle: "OK")
                    }
                    
                    
                    
                }
            }
            
        })
        URL_handler.makeCall("\(registerExperience)" , param: parameter, completionHandler: { (responseObject, error) -> () in
            self.DismissProgress()
            
            if(error != nil){
                self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault as AnyObject, title: Appname)
            } else {
                if(responseObject != nil){
                    let dict:NSDictionary=responseObject!
                    let status = self.theme.CheckNullValue(dict.object(forKey: "status") as AnyObject)!
                    let response = dict.object(forKey: "response") as! NSArray
                    if(status == "1")
                    {
                        for i in response
                        {
                            let qestion = i as! NSMutableDictionary
                            
                            self.selectExperienceArray.append("\(qestion.object(forKey: "name") as! String)")
                            self.selectExperienceArrayID.append("\(qestion.object(forKey: "_id") as! String)")
                        }
                        self.selectExperience.frame = CGRect(x: self.levelOfExperience_textField.frame.origin.x, y: self.levelOfExperience_textField.frame.maxY, width: self.levelOfExperience_textField.frame.size.width, height: CGFloat(self.selectExperienceArray.count*44))
                        self.scroll_View.frame = CGRect(x: self.scroll_View.frame.origin.x, y: self.scroll_View.frame.origin.y, width: self.scroll_View.frame.size.width, height: self.selectExperience.frame.maxY+5)
                        
                        self.selectExperience.reload()
                        
                        
                    }
                    else{
                        self.theme.AlertView("\(Appname)", Message: "\(self.theme.CheckNullValue(dict.object(forKey: "response") as AnyObject)!)", ButtonTitle: "OK")
                    }
                    
                    
                    
                }
            }
            
        })

        

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func backBtn_Action(_ sender: UIButton) {
        self.navigationController?.popViewControllerWithFlip(animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if tableView == categorylist_tableView{
        if category == "mainCategory"{
            if searchArray.count == 0 {
            
            return categoryArray.count
            }
            else {
                return searchArray.count
            }
        }
        else if category == "subCategory"{
            if searchArray.count == 0 {
            return subCategoryArray.count
            }
            else{
             return searchArray.count
            }
        }
    }
    else
    {
     return selectExperienceArray.count
    }
    return selectExperienceArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = UITableViewCell()
        if tableView == categorylist_tableView{

        if category == "mainCategory"{
           
            if searchArray.count == 0{
            let Placecell = categorylist_tableView.dequeueReusableCell(withIdentifier: "cell") as! CategoryTableViewCell
            Placecell.textLabel?.text = categoryArray[indexPath.row] 
                return Placecell
            }
            else{
                let Placecell = categorylist_tableView.dequeueReusableCell(withIdentifier: "cell") as! CategoryTableViewCell
                Placecell.textLabel?.text = searchArray[indexPath.row] 
                return Placecell
            }
            
            
            
        }
        
        if category == "subCategory"{
            if searchArray.count == 0{
                let Placecell = categorylist_tableView.dequeueReusableCell(withIdentifier: "cell") as! CategoryTableViewCell
                Placecell.textLabel?.text = subCategoryArray[indexPath.row] 
                return Placecell
            }
            else{
                let Placecell = categorylist_tableView.dequeueReusableCell(withIdentifier: "cell") as! CategoryTableViewCell
                Placecell.textLabel?.text = searchArray[indexPath.row] 
                return Placecell
            }
        }
        }
        else if tableView == selectExperience {
            let Placecell = selectExperience.dequeueReusableCell(withIdentifier: "cell") as! SelectExperienceTableViewCell
            Placecell.textLabel?.text = selectExperienceArray[indexPath.row] 
            return Placecell
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
       
        if tableView == categorylist_tableView{
            
            if category == "mainCategory"{
                 if searchArray.count == 0{
                
             parentCategory_textField.text = categoryArray[indexPath.row] 
                    parent_id = categoryArrayID[indexPath.row] 
                
                    categorylist_tableView.isHidden = true
                    
                    
                 }else{
                    parentCategory_textField.text = searchArray[indexPath.row] 
                    parent_id = parentid(parentCategory_textField.text!)
                    categorylist_tableView.isHidden = true
                    
                }
                
            }
            else if category == "subCategory"{
                
                if searchArray.count == 0{
                    
                    sub_Category_txtField.text = subCategoryArray[indexPath.row] 
                    categorylist_tableView.isHidden = true
                }else{
                    sub_Category_txtField.text = searchArray[indexPath.row] 
                    childId = childid(sub_Category_txtField.text!)
                    categorylist_tableView.isHidden = true
                }
                
        }
        }
        else {
           levelOfExperience_textField.text = selectExperienceArray[indexPath.row] 
            selectExperience.isHidden = true
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
       
        if textField == parentCategory_textField{
            parentCategory_textField.text = ""
            category = "mainCategory"
            categorylist_tableView.isHidden = false
            categorylist_tableView.frame = CGRect(x: self.categorylist_tableView.frame.origin.x, y: self.parentCategory_textField.frame.maxY, width: self.parentCategory_textField.frame.size.width, height: 150)
            sub_Category_txtField.text = ""
            searchArray.removeAll()
             categorylist_tableView.reload()
            

        }
        else if textField == sub_Category_txtField {
            category = "subCategory"
            subCategoryArray.removeAll()
            subCatArray.removeAllObjects()
            categorylist_tableView.isHidden = false
            categorylist_tableView.frame = CGRect(x: self.categorylist_tableView.frame.origin.x, y: self.sub_Category_txtField.frame.maxY, width: self.parentCategory_textField.frame.size.width, height: 150)
            

            let parameter = ["category": parent_id]
            self.showProgress()
            URL_handler.makeCall("\(subCatergory_Url)" , param: parameter as NSDictionary, completionHandler: { (responseObject, error) -> () in
                self.DismissProgress()
                
                if(error != nil){
                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault as AnyObject, title: Appname)
                } else {
                    self.categorylist_tableView.isHidden = false
                     self.categorylist_tableView.frame = CGRect(x: self.categorylist_tableView.frame.origin.x, y: self.sub_Category_txtField.frame.maxY, width: self.sub_Category_txtField.frame.size.width, height: 150)
                    if(responseObject != nil){
                        let dict:NSDictionary=responseObject!
                        var status = self.theme.CheckNullValue(dict.object(forKey: "status"))!
                        var response = dict.object(forKey: "response") as! NSArray
                        if(status == "1")
                        {
                            for i in response {
                                var qestion = i as! NSMutableDictionary
                                let subCat:SubCategory = SubCategory()
                                
                                self.subCategoryArray.append("\(qestion.object(forKey: "name") as! String)")
                                subCat.name = "\(qestion.object(forKey: "name") as! String)"
                                
                                self.subCategoryArrayID.append("\(qestion.object(forKey: "_id") as! String)")
                                subCat.id = "\(qestion.object(forKey: "_id") as! String)"
                                self.subCatArray.add(subCat)
                            }
                            
                            self.categorylist_tableView.reload()
                            
                            
                        }
                        else{
                            self.theme.AlertView("\(Appname)", Message: "\(self.theme.CheckNullValue(dict.object(forKey: "response"))!)", ButtonTitle: "OK")
                        }
                        
                        
                        
                    }
                }
                
            })
        }
            else if textField == levelOfExperience_textField{
            categorylist_tableView.isHidden = true
                selectExperience.isHidden = false
            }

            searchArray.removeAll()
            
        categorylist_tableView.reload()
        
        
        }
    
    func TextfieldDidChange(_ textField:UITextField)
    {
        if textField == parentCategory_textField
    {
            category = "mainCategory"
            
            
           
            
            
        }

        else if textField == sub_Category_txtField
        {
            category = "subCategory"
            
            
            
                    }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        if textField == parentCategory_textField{
            category = "mainCategory"
            
            
            let NewText = (textField.text! as NSString).replacingCharacters(in: range, with:string)
            print(NewText);
            let range = (NewText as String).characters.startIndex ..< (NewText as String).characters.endIndex
            var searchString = String()
            (NewText as String).enumerateSubstrings(in: range, options: .byComposedCharacterSequences, { (substring, substringRange, enclosingRange, success) in
                searchString.append(substring!)
                searchString.append("*")
            })
            let searchPredicate = NSPredicate(format: "SELF LIKE[c] %@", searchString)
            let array = (categoryArray as NSArray).filtered(using: searchPredicate)
            searchArray = array as! [String]
            categorylist_tableView.reload()
            
            
        }
            
        else if textField == sub_Category_txtField {
            category = "subCategory"
            
            
            
            let NewText = (textField.text! as NSString).replacingCharacters(in: range, with:string)
            print(NewText);
            let range = (NewText as String).characters.startIndex ..< (NewText as String).characters.endIndex
            var searchString = String()
            (NewText as String).enumerateSubstrings(in: range, options: .byComposedCharacterSequences, { (substring, substringRange, enclosingRange, success) in
                searchString.append(substring!)
                searchString.append("*")
            })
            let searchPredicate = NSPredicate(format: "SELF LIKE[c] %@", searchString)
            let array = (subCategoryArray as NSArray).filtered(using: searchPredicate)
            searchArray = array as! [String]
            categorylist_tableView.reload()
            
        }

        
        
        
        return true
        
    }

    func showProgress()
    {
        if spinnerView == nil {
            spinnerView = MMMaterialDesignSpinner(frame: CGRect.zero)
            
        }
        
        
        
        spinnerViews = spinnerView
        self.spinnerViews!.bounds = CGRect(x: 0, y: 0, width: 75, height: 75)
        self.spinnerViews!.tintColor = PlumberThemeColor
        
        if((self is JobsClosedViewController)||(self is MyOrderViewController)||(self is JobsCancelledViewController)||(self is MyJobsViewController)||(self is MyLeadsViewController)||(self is MissedLeadsViewController)||(self is BarChartViewController)||(self is PieChartViewController)||(self is NewLeadsViewController)){
            self.spinnerViews!.center = CGPoint(x: (self.view.bounds).midX,y: (self.view.bounds).midY-107)
        }else{
            self.spinnerViews!.center = CGPoint(x: (self.view.bounds).midX, y: (self.view.bounds).midY)
        }
        
        
        
        print("display spinner frame=\(String(describing: self.spinnerView?.frame))" )
        self.spinnerViews!.translatesAutoresizingMaskIntoConstraints = false
        self.view!.addSubview(self.spinnerViews!)
        
        self.spinnerView?.startAnimating();
        
    }
    func DismissProgress()
    {
        self.spinnerView?.stopAnimating();
        
        
    }

    @IBAction func save(_ sender: AnyObject) {
      if parentCategory_textField.text == ""{
        theme.AlertView("\(Appname)", Message: "Kindly Choose the Category", ButtonTitle: "ok")
        }
       else if sub_Category_txtField.text == ""{
        theme.AlertView("\(Appname)", Message: "Kindly Choose the SubCategory", ButtonTitle: "ok")
        }
      else if setHourlyRate_textField.text == ""{
        theme.AlertView("\(Appname)", Message: "Kindly Choose the Hourly Rate", ButtonTitle: "ok")
        }
      else if levelOfExperience_textField.text == ""{
        theme.AlertView("\(Appname)", Message: "Kindly Select the Experience", ButtonTitle: "ok")
        }
      else if jobDetail.checkSubCategoryArray.contains(sub_Category_txtField.text!){
        theme.AlertView("\(Appname)", Message: "This Category is Already Selected", ButtonTitle: "ok")
      }
      else {
        record()
        self.delegate?.changeBackgroundColor(jobDetail.categoryEditArray)
        self.navigationController?.popViewControllerWithFlip(animated: true)
       
        
        }
        
    }
    func parentid(_ element:String) -> String
    {
        let index = categoryArray.index(of: element)
        let id = categoryArrayID[index!]
        return id
        
    }
    func childid(_ element:String) -> String
    {
        let index = subCategoryArray.index(of: element)
        let id = subCategoryArrayID[index!]
        return id
    }
    func down(){
        
    }
    func record(){
        var rec:CategoryEditRecord = CategoryEditRecord()
        var rec1:categorydata = categorydata()
        rec.catergory = parentCategory_textField.text!
        rec1.categoryid = parent_id 
        rec1.childid = childId 
        
        rec.subCategory = sub_Category_txtField.text!
       jobDetail.checkSubCategoryArray.add(sub_Category_txtField.text!)
        rec.quickPitch = theme.CheckNullValue(quikPitch_textField.text! as AnyObject)!
        rec1.quick_pitch = theme.CheckNullValue(quikPitch_textField.text! as AnyObject)!
        rec.hourlyRate = setHourlyRate_textField.text!
        rec1.hour_rate = setHourlyRate_textField.text!
        rec.experience = levelOfExperience_textField.text!
        rec1.experience = levelOfExperience_textField.text!
       jobDetail.categoryEditArray.add(rec)
        registerData.category_details.add(rec1)
        
        
        
    
    }
}
