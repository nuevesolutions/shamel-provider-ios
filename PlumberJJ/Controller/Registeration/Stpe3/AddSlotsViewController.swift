//
//  AddSlotsViewController.swift
//  PlumberJJ
//
//  Created by Casperon iOS on 27/07/17.
//  Copyright © 2017 Casperon Technologies. All rights reserved.
//

import UIKit

class AddSlotsViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource, UICollectionViewDelegate,UICollectionViewDataSource {
    var no_Of_Slots:Int = 0
    var time_Interval:Double = 0.0
    var timeInterval_Array1:[String] = [String]()
    var timeInterval_Array2:[String] = [String]()
    var nonReptimeInterval_Array1:[String] = [String]()
    var nonReptimeInterval_Array2:[String] = [String]()
    
    @IBOutlet weak var back_Btn: UIButton!
    
    @IBOutlet weak var addslots_lbl: UILabel!
    @IBOutlet weak var location_lbl: UILabel!
    @IBOutlet weak var Address_view: UIView!
    @IBOutlet weak var Address_lbl: UILabel!
    
    @IBOutlet weak var Address_btn: UIButton!
    
    @IBOutlet weak var slots_view: UIView!
    
    @IBOutlet weak var RadiusofOperation_lbl: UILabel!
    
    @IBOutlet weak var setRadius_lbl: UILabel!
    @IBOutlet weak var noOfSlot_lbl: UILabel!
    @IBOutlet weak var slotTime_lbl: UILabel!
    @IBOutlet weak var miles_lbl: UILabel!
    
    @IBOutlet weak var rateSymbol_lbl: UILabel!
    @IBOutlet weak var slotMin_lbl: UILabel!
    
    @IBOutlet weak var miles_textView: UITextField!
    
    @IBOutlet weak var setRate_textview: UITextField!
    @IBOutlet weak var slots_textview: UITextField!
    
    @IBOutlet weak var minSelection_btn: UIButton!
    @IBOutlet weak var slotMinSet_tableview: UITableView!
    
    @IBOutlet weak var ConfigureSchedule_View: UIView!
    
    @IBOutlet weak var CofigureSchedule_lbl: UILabel!
    
   
    
    @IBOutlet weak var setDateTime_view: UIView!
    @IBOutlet weak var setDate_lbl: UILabel!
    
    @IBOutlet weak var end_date_lbl: UILabel!
    
    @IBOutlet weak var starttime_lbl: UILabel!
    @IBOutlet weak var startDate_btn: UIButton!
    
    @IBOutlet weak var endDate_btn: UIButton!
    
    @IBOutlet weak var startTime_btn: UIButton!
    
    @IBOutlet weak var DaysSelection_view: UIView!
    @IBOutlet weak var everyDay_btn: UIButton!
    @IBOutlet weak var weekDay_btn: UIButton!
    
    @IBOutlet weak var weekEnd_Btn: UIButton!
    
    @IBOutlet weak var slotCreated_View: UIView!
    
    @IBOutlet weak var slotCreated_lbl: UILabel!
    
    
    
    @IBOutlet weak var repeatMode_view: UIView!
    
    @IBOutlet weak var nonReapeatMode_view: UIView!
    
    @IBOutlet weak var nonRptStartDate_lbl: UILabel!
    @IBOutlet weak var nonRptStartTime_lbl: UILabel!
    @IBOutlet weak var nonRptSlotTime_view: UIView!
    
    @IBOutlet weak var nonRptSlotCreated_view: UIView!
    
    @IBOutlet weak var nonRptStartDate_btn: UIButton!
    
    @IBOutlet weak var nonRptStartTime_btn: UIButton!
    @IBOutlet weak var nonRptSlotCreated_lbl: UILabel!
    
    @IBOutlet weak var nonRptSlot_collectionview: UICollectionView!
   
    @IBOutlet weak var upper_View: UIView!
    
    @IBOutlet weak var segment_Controller: CustomSegmentControl!
    @IBOutlet weak var datePicker_view: UIView!
    
    @IBOutlet weak var nonRepeat_collectionView: UICollectionView!
    @IBOutlet weak var slotSelect_view: UIView!
    @IBOutlet weak var repeatMode_collectionView: UICollectionView!
    @IBOutlet weak var scroll_view: UIScrollView!
    @IBOutlet weak var calender_view: UIView!
    @IBOutlet weak var date_picker: UIDatePicker!
    @IBOutlet weak var done_btn: UIButton!
    @IBOutlet weak var confirm_btn: UIButton!
    var slotTime:[String] = [String]()
    
    var start_Time_repeatMode:String = String()
    var checkdate:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        slots_textview.addTarget(self, action: #selector(TextfieldDidChange(_:)), for: UIControlEvents.editingChanged)
        
        
        
        nonRepeat_collectionView.delegate = self
        repeatMode_collectionView.delegate = self
        
        let nib = UINib(nibName: "TimeSlotCollectionViewCell", bundle: nil)
        self.nonRepeat_collectionView.register(nib, forCellWithReuseIdentifier: "cell")
        self.repeatMode_collectionView.register(nib, forCellWithReuseIdentifier: "cell")
        
        
        
        
        calender_view.isHidden = true
        date_picker.isHidden = true
        done_btn.isHidden = true
        datePicker_view.isHidden = true
        _ = Foundation.Date()
        
        // create an instance of calendar view with
        // base date (Calendar shows 12 months range from current base date)
        // selected date (marked dated in the calendar)
       
       
        

      self.miles_textView.delegate=self
        self.setRate_textview.delegate=self
        self.slots_textview.delegate = self
                
        segment_Controller.setTitle(theme.setLang("first_segment"), forSegmentAt: 0)
        segment_Controller.setTitle(theme.setLang("second_segment"), forSegmentAt: 1)
        segment_Controller.selectedSegmentIndex=0
        segment_Controller.tintColor=theme.additionalThemeColour()
        
        
        segment_Controller.setTitleTextAttributes([NSFontAttributeName: UIFont(name: "HelveticaNeue", size: 14.0)!, NSForegroundColorAttributeName: PlumberThemeColor], for: UIControlState())
        segment_Controller.frame = CGRect(x: self.segment_Controller.frame.minX, y: segment_Controller.frame.minY, width: segment_Controller.frame.size.width, height: 40)
        

        
        
        
        
        
        
addslots_lbl.text = theme.setLang("step_3")
  location_lbl.text = " "+theme.setLang("location")
    RadiusofOperation_lbl.text = theme.setLang("radius_of_operation")
    setRadius_lbl.text = theme.setLang("set_rate")
    noOfSlot_lbl.text = theme.setLang("no_of_slot")
        slotTime_lbl.text = theme.setLang("slot_time_lbl")
        miles_lbl.text = theme.setLang("miles")
        rateSymbol_lbl.text = theme.setLang("$")
        miles_textView.placeholder = theme.setLang("miles")
        setRate_textview.placeholder = theme.setLang("$")
        slots_textview.placeholder = theme.setLang("slot")
        
    CofigureSchedule_lbl.text = theme.setLang("con_schedule")
     setDate_lbl.text = theme.setLang("start_date")
     end_date_lbl.text = theme.setLang("end_date")
     starttime_lbl.text = theme.setLang("start_time")
     nonRptStartDate_lbl.text = theme.setLang("start_time")
      nonRptStartTime_lbl.text = theme.setLang("start_date")
       
        
    
        slotMinSet_tableview.isHidden = true
        scroll_view.contentSize = CGSize(width: scroll_view.layer.bounds.width, height: UIScreen.main.bounds.height+upper_View.frame.size.height)
        
        
    }
    @IBAction func backButton(_ sender: AnyObject) {
        self.navigationController?.popViewControllerWithFlip(animated: true)
    
    }

    @IBAction func Adress_select_btn(_ sender: AnyObject) {
    }
    
  
    @IBAction func tblViewShow_btn(_ sender: AnyObject) {
        
    slotMinSet_tableview.isHidden = false
        
        
    }
    @IBAction func didClickConfrim_btn(_ sender: AnyObject)
    {
                Add_address()
        
    }
    @IBAction func didSelectRepaetmode_startDate(_ sender: AnyObject) {
        
        checkdate = "Repaetmode_startDate"
        calender_view.isHidden = false
        
    }
    @IBAction func didSelectRepeatMode_endDate(_ sender: AnyObject) {
        checkdate = "RepeatMode_endDate"
        calender_view.isHidden = false
    }
    
    @IBAction func didSelectRepeatMode_StartTime(_ sender: AnyObject) {
        checkdate = "RepeatMode_StartTime"
        date_picker.isHidden = false
        done_btn.isHidden = false
        datePicker_view.isHidden = false
        
    }
    
    @IBAction func didSelectNonRepeatMode_StartDate(_ sender: AnyObject) {
        checkdate = "NonRepeatMode_StartDate"
        calender_view.isHidden = false
    }
    @IBAction func didSelectNonRepeatMode_StartTime(_ sender: AnyObject) {
      checkdate = "NonRepeatMode_StartTime"
        date_picker.isHidden = false
        done_btn.isHidden = false
        datePicker_view.isHidden = false
        
    }
    
    
    
    
    @IBAction func daySchedule_Every_Day(_ sender: AnyObject) {
    everyDay_btn.layer.borderWidth = 2
    everyDay_btn.layer.borderColor = UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: 1).cgColor
        weekDay_btn.layer.borderWidth = 2
        weekDay_btn.layer.borderColor = UIColor.white.cgColor
        weekEnd_Btn.layer.borderWidth = 2
        weekEnd_Btn.layer.borderColor = UIColor.white.cgColor
        
    }
    
    @IBAction func daySchedule_Week_Day(_ sender: AnyObject) {
        everyDay_btn.layer.borderWidth = 2
        everyDay_btn.layer.borderColor = UIColor.white.cgColor
        weekDay_btn.layer.borderWidth = 2
        weekDay_btn.layer.borderColor = UIColor.white.cgColor
        weekEnd_Btn.layer.borderWidth = 2
        weekEnd_Btn.layer.borderColor = UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: 1).cgColor
        
    }
    @IBAction func daySchedule_WeekEnd(_ sender: AnyObject) {
        everyDay_btn.layer.borderWidth = 2
        everyDay_btn.layer.borderColor = UIColor.white.cgColor
        weekDay_btn.layer.borderWidth = 2
        weekDay_btn.layer.borderColor = UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: 1).cgColor
        weekEnd_Btn.layer.borderWidth = 2
        weekEnd_Btn.layer.borderColor = UIColor.white.cgColor
        
    }
    
    @IBAction func done(_ sender: AnyObject) {
        timeInterval_Array1.removeAll()
        timeInterval_Array2.removeAll()
        nonReptimeInterval_Array1.removeAll()
        nonReptimeInterval_Array2.removeAll()
        
        date_picker.isHidden = true
        done_btn.isHidden = true
        datePicker_view.isHidden = true
      
        let dateFormatter = DateFormatter()
        
//        dateFormatter.dateStyle = NSDateFormatterStyle.ShortStyle
        dateFormatter.timeStyle = DateFormatter.Style.short
        
        let strDate = dateFormatter.string(from: date_picker.date)
        
        print(strDate)
        
        if checkdate == "RepeatMode_StartTime"
        {
            
          startTime_btn.setTitle(strDate, for: UIControlState())
            var str: String = strDate
            let addTime: Int = 30  //minutes
            let formatter = DateFormatter()
            formatter.dateFormat = "hh:mm a"
            var dateInput: Foundation.Date = formatter.date(from: str)!
            
            for i in 0..<no_Of_Slots{
            timeInterval_Array1.append(str)
            dateInput = dateInput.addingTimeInterval(time_Interval*60)
                str = formatter.string(from: dateInput)
               timeInterval_Array2.append(str)
                
                
           
            }
            
          repeatMode_collectionView.reloadData()
        }
        if checkdate == "NonRepeatMode_StartTime"
        {
            
            nonRptStartTime_btn.setTitle(strDate, for: UIControlState())
            var str: String = strDate
            let addTime: Int = 30  //minutes
            let formatter = DateFormatter()
            formatter.dateFormat = "hh:mm a"
            var dateInput: Foundation.Date = formatter.date(from: str)!
            
            for i in 0..<no_Of_Slots{
                nonReptimeInterval_Array1.append(str)
                dateInput = dateInput.addingTimeInterval(time_Interval*60)
                str = formatter.string(from: dateInput)
                nonReptimeInterval_Array2.append(str)
                
                
                
            }

        
        nonRepeat_collectionView.reloadData()
        }
    }
    
    func TextfieldDidChange(_ textField:UITextField)
    {
        if textField == slots_textview {
           if slots_textview.text != ""{
            no_Of_Slots = Int(slots_textview.text!)!
        }
    
    }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField == miles_textView){
            miles_textView.resignFirstResponder()
            setRate_textview.becomeFirstResponder()
        }
        
        if (textField == setRate_textview){
            setRate_textview.resignFirstResponder()
            slots_textview.becomeFirstResponder()
        }
        if (textField == slots_textview){
            slots_textview.resignFirstResponder()
                    }
    
    return true
    }
//    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
//        if(textField == miles_textView)
//        {
//        }
//    }
    
    func Add_address()
    {
        if(miles_textView.text == "") {
            theme.AlertView("\(Appname)", Message:theme.setLang("enter_ur_miles"), ButtonTitle: kOk)
        }  else if(setRate_textview.text == "") {
            theme.AlertView("\(Appname)", Message: theme.setLang("enter_ur_rate"), ButtonTitle: kOk)
        }else if(slots_textview.text == "") {
            theme.AlertView("\(Appname)", Message: theme.setLang("enter_ur_slots"), ButtonTitle: kOk)
        }
            
            //        else if(HouseNo_Field.text == "")
            //        {
            //            themes.AlertView("\(Appname)", Message: "Enter your house no", ButtonTitle: "Ok")
            //        }
            
            //        else if(Landmark_Field.text == "")
            //        {
            //            themes.AlertView("\(Appname)", Message: "Enter your landmark", ButtonTitle: "Ok")
            //        }
            //
               //        {
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! SelectMinuteTableViewCell

         cell.textLabel!.text = "\((15 )*(indexPath.row+1))"
        slotTime.append(cell.textLabel!.text!)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
       slotMin_lbl.text = "Min"+" "+slotTime[indexPath.row]
        time_Interval = Double(slotTime[indexPath.row])!
        slotMinSet_tableview.isHidden = true
        
        
    }
    
    @IBAction func segmentAction(_ sender: AnyObject) {
        let segmentIndex:NSInteger = sender.selectedSegmentIndex;
        
        if(segmentIndex == 0)
        {
            scroll_view.isScrollEnabled = true
            self.nonReapeatMode_view.isHidden=true
            self.repeatMode_view.isHidden=false
             scroll_view.contentSize = CGSize(width: scroll_view.frame.size.width, height: UIScreen.main.bounds.height+upper_View.frame.size.height)
        }
        if(segmentIndex == 1)
        {
             scroll_view.contentSize = CGSize(width: scroll_view.layer.bounds.width, height: UIScreen.main.bounds.height-upper_View.frame.size.height-confirm_btn.frame.size.height+15)
           
            self.nonReapeatMode_view.isHidden=false
            self.repeatMode_view.isHidden=true
            
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return no_Of_Slots
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
        if collectionView == nonRepeat_collectionView{
            let cell = nonRepeat_collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! TimeSlotCollectionViewCell

            cell.slottime_1.text = nonReptimeInterval_Array1[indexPath.item]
            cell.slottime_2.text = nonReptimeInterval_Array2[indexPath.item]
            cell.slot_view.layer.borderWidth  = 1
            cell.slot_view.layer.borderColor = UIColor.black.cgColor
            cell.slot_view.layer.cornerRadius = 5
            cell.slot_view.layer.masksToBounds = true
            return cell
            
        }
        if collectionView == repeatMode_collectionView{
            let Cell = repeatMode_collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! TimeSlotCollectionViewCell
            Cell.slottime_1.text = timeInterval_Array1[indexPath.item]
            Cell.slottime_2.text = timeInterval_Array2[indexPath.item]
            Cell.slot_view.layer.borderWidth  = 1
            Cell.slot_view.layer.borderColor = UIColor.black.cgColor
            Cell.slot_view.layer.cornerRadius = 5
            Cell.slot_view.layer.masksToBounds = true
            return Cell
        }
        let cell = TimeSlotCollectionViewCell()
        
        return cell
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
       func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == slots_textview{
            if slots_textview.text == ""{
            no_Of_Slots = 0
            }
            else {
                 no_Of_Slots = Int(slots_textview.text!)!
            }
        }
        return true
    }
    func disablePreviousDays()
   {
        
    
        
    }
    
    
//
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
