//
//  RegisterVC.swift
//  PlumberJJ
//
//  Created by casperon_macmini on 17/07/17.
//  Copyright © 2017 Casperon Technologies. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}


class RegisterVC: RootBaseViewController,UITextFieldDelegate {
    var selectedCode:String = String()
    @IBOutlet var countrycode_txtfiled: UITextField!
    @IBOutlet weak var citytext_line: UIView!
    @IBOutlet var city_txtField: UITextField!
    //    @IBOutlet var gender_View: UIView!
    //  @IBOutlet var Firstname_TxtFld: UITextField!
    
    @IBOutlet var topView: SetColorView!

    @IBOutlet var countryCode_Btn: UIButton!
    @IBOutlet var confirm_TxtFld: UITextField!
    @IBOutlet var password_TxtFld: UITextField!
    @IBOutlet var contactNo_TxtFld: UITextField!
    @IBOutlet var emailAddress_TxtFld: UITextField!
    @IBOutlet var usernName_TxtFld: UITextField!
    @IBOutlet var lastName_TxtFld: UITextField!
    @IBOutlet var Firstname_TxtFld: UITextField!
    @IBOutlet var continue_Btn: UITextField!
    
   
    @IBOutlet var confirmPassword_Lbl: CustomLabelGray!
    @IBOutlet var password_Lbl: CustomLabelGray!
    @IBOutlet var userName_Lbl: CustomLabelGray!
    @IBOutlet var lastName_Lbl: CustomLabelGray!
    @IBOutlet var firstName_Lbl: CustomLabelGray!
    
 
    @IBOutlet var emailAddress_Lbl: CustomLabelGray!
    
    @IBOutlet var contactNo_Lbl: CustomLabelGray!
    @IBOutlet var scroll_View: UIScrollView!
    
    var drop_Selection:String = String()
    
    @IBOutlet weak var city_view: UIView!
    @IBOutlet weak var Placesearch_tableview: UITableView!
    var firstName:NSString=NSString()
    var lastName:NSString=NSString()
    var userName:NSString=NSString()
    var emailid:NSString=NSString()
    var phoneNumber:NSString=NSString()
    var Password:NSString=NSString()
    var confrimPassword:NSString=NSString()
    var cityName:NSString=NSString()
    var URL_handler:URLhandler=URLhandler()
    
 
    
    let string = "*"
    
    struct Place
    {
        let id: String
        let description: String
    }
    
    var places = [Place]()
    
    
    var validateemail:Bool=Bool()
    var validatepasswd : Bool = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        theme.SetPaddingView(Firstname_TxtFld)
        theme.SetPaddingView(lastName_TxtFld)
        theme.SetPaddingView(usernName_TxtFld)
        theme.SetPaddingView(emailAddress_TxtFld)
        theme.SetPaddingView(contactNo_TxtFld)
        theme.SetPaddingView(password_TxtFld)
        theme.SetPaddingView(confirm_TxtFld)
        countryCode_Btn.backgroundColor = UIColor.white
        countryCode_Btn.layer.cornerRadius = 0
        countryCode_Btn.layer.borderWidth = 1
        countryCode_Btn.setTitle("+91", for: UIControlState())
        
        /*
        firstName_Lbl.text = "\(theme.setLang("first")) \(string)"
        lastName_Lbl.text = "\(theme.setLang("last")) \(string)"
        userName_Lbl.text = "\(theme.setLang("user")) \(string)"
        emailAddress_Lbl.text = "\(theme.setLang("email")) \(string)"
        contactNo_Lbl.text = "\(theme.setLang("contact")) \(string)"
        password_Lbl.text =  "\(theme.setLang("password")) \(string)"
        confirmPassword_Lbl.text = "\(theme.setLang("conformpassword")) \(string)"
       */
        
        firstName_Lbl.text = Language_handler.VJLocalizedString("first_name", comment: nil)
        lastName_Lbl.text = Language_handler.VJLocalizedString("last_name", comment: nil)
        userName_Lbl.text = Language_handler.VJLocalizedString("user_name", comment: nil)
        emailAddress_Lbl.text = Language_handler.VJLocalizedString("email_add", comment: nil)
        contactNo_Lbl.text = Language_handler.VJLocalizedString("contact_num", comment: nil)
        password_Lbl.text =  Language_handler.VJLocalizedString("password", comment: nil)
        confirmPassword_Lbl.text = Language_handler.VJLocalizedString("confirm_pass", comment: nil)
        
        Firstname_TxtFld.placeholder = " \(theme.setLang("first"))"
        lastName_TxtFld.placeholder = " \(theme.setLang("last"))"
        usernName_TxtFld.placeholder = " \(theme.setLang("user"))"
        emailAddress_TxtFld.placeholder = " \(theme.setLang("email"))"
        contactNo_TxtFld.placeholder = " \(theme.setLang("contact"))"
        password_TxtFld.placeholder = " \(theme.setLang("password"))"
        confirm_TxtFld.placeholder = " \(theme.setLang("conformpassword"))"
        
        continue_Btn.text = Language_handler.VJLocalizedString("continue", comment: nil)
        
        
    }
    @IBAction func Code_Change(_ sender: AnyObject) {
        
        let objMyJobsVc = self.storyboard!.instantiateViewController(withIdentifier: "country_code") as! CountryCodeViewController
        self.navigationController!.pushViewController(withFlip: objMyJobsVc, animated: true)
        
    }
    
    func setView(){
        //
        //        Placesearch_tableview.frame = CGRectMake(city_view.frame.minX, citytext_line.frame.maxY, city_view.frame.size.width, city_view.frame.size.height)
        //        Placesearch_tableview.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        //        scroll_View.contentOffset = CGPoint(x: 0, y: topView.frame.maxY)
        scroll_View.contentSize = CGSize(width: UIScreen.main.bounds.width,height: confirm_TxtFld.frame.maxY+confirm_TxtFld.frame.size.height+20)
        
        //         scroll_View.contentSize = CGSizeMake(UIScreen.mainScreen().bounds.width,profile_Img.frame.maxY+continue_Btn.frame.size.height+10)
        
        //        countryCode_Btn.layer.shadowColor = UIColor.lightGrayColor().cgColor
        //        countryCode_Btn.layer.shadowOffset = CGSize(width: 3, height: 2)
        //        countryCode_Btn.layer.shadowOpacity = 0.8
        //        countryCode_Btn.layer.shadowRadius = 1.0
        
        if  signup.selectedCode  != "" {
            countryCode_Btn.setTitle("\(signup.selectedCode)", for: UIControlState())
            
        }
            
        else{
            
            countryCode_Btn.setTitle("+91", for: UIControlState())
            
            
        }
        
        //         self.gender_Txt.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(RegisterVC.gender_Action(_:))))
        
        
    }
    //
    //    func gender_Action(sender: UIButton) {
    //
    //        let genderPicker_Sheet = UIAlertController(title: nil, message: "Select Gender", preferredStyle: .ActionSheet)
    //
    //        let  male_Pick = UIAlertAction(title: "Male", style: .Default, handler: {
    //            (alert: UIAlertAction!) -> Void in
    //            self.male_Pick()
    //        })
    //        let female_Pick = UIAlertAction(title: "Female", style: .Default, handler: {
    //            (alert: UIAlertAction!) -> Void in
    //            //
    //            self.female_Pick()
    //
    //        })
    //
    //        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
    //            (alert: UIAlertAction!) -> Void in
    //        })
    //
    //
    //        genderPicker_Sheet.addAction(male_Pick)
    //        genderPicker_Sheet.addAction(female_Pick)
    //        genderPicker_Sheet.addAction(cancelAction)
    //
    //        self.present(genderPicker_Sheet, animated: true, completion: nil)
    //
    //    }
    
    //    func male_Pick(){
    //
    //        gender_Txt.text = "Male"
    //
    //    }
    //
    //    func female_Pick(){
    //
    //        gender_Txt.text = "Female"
    //
    //    }
    
    @IBAction func continueBtn_Action(_ sender: CustomButton) {
        self.register_act()
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setView()
        
        // scroll_View.contentSize = CGSize(x: 0, y: 0, width: UIScreen.mainScreen().bounds.width, height: <#T##CGFloat#>)
    }
    @IBAction func bactBtn_Action(_ sender: UIButton) {
        
        self.navigationController?.popViewControllerWithFlip(animated: true)
    }
    
    func register_act()
    {
        
        firstName = Firstname_TxtFld.text! as NSString
        
        lastName = lastName_TxtFld.text! as NSString
        
        userName = usernName_TxtFld.text! as NSString
       
        emailid = emailAddress_TxtFld.text! as NSString
        
        validateemail=theme.isValidEmail(emailid as String)
        
        phoneNumber = contactNo_TxtFld.text! as NSString
        
        Password = password_TxtFld.text! as NSString
        
        confrimPassword = confirm_TxtFld.text! as NSString
        
        //        cityName = city_txtField.text!
        
        if (Firstname_TxtFld.text == ""){
            theme.AlertView("\(Appname)",Message: theme.setLang("enter_first_name"),ButtonTitle: "Ok")
        }
        else if (Firstname_TxtFld.text?.count >= 25){
            theme.AlertView("\(Appname)",Message:theme.setLang("name_below_25"),ButtonTitle: "Ok")
        }
        else if (lastName_TxtFld.text == ""){
            theme.AlertView("\(Appname)",Message: theme.setLang("enter_lastname"),ButtonTitle: "Ok")
        }
        else if (lastName_TxtFld.text?.count >= 25){
            theme.AlertView("\(Appname)",Message: theme.setLang("last_below_25"),ButtonTitle: "Ok")
        }
        else if (usernName_TxtFld.text == ""){
            theme.AlertView("\(Appname)",Message: theme.setLang("enter_Username"),ButtonTitle: "Ok")
        }
        else if (usernName_TxtFld.text?.count >= 25){
            theme.AlertView("\(Appname)",Message: theme.setLang("user_below_25"),ButtonTitle: "Ok")
        }
        else if(emailAddress_TxtFld.text == "") {
            theme.AlertView("\(Appname)",Message: theme.setLang("enter_emailid"),ButtonTitle: kOk)
        }
        else if(contactNo_TxtFld.text == "") {
            theme.AlertView("\(Appname)",Message: theme.setLang("enter_phno"),ButtonTitle: kOk)
        }else if(validateemail == false){
            theme.AlertView("\(Appname)",Message: theme.setLang("valid_email_alert"),ButtonTitle: kOk)
        }else if(Password.length < 5 ){
            theme.AlertView("\(Appname)",Message: theme.setLang("valid_password"),ButtonTitle: kOk)
        }
        else if (password_TxtFld.text != confirm_TxtFld.text){
            theme.AlertView("\(Appname)",Message: theme.setLang("passwd_match_error"),ButtonTitle: kOk)
        }
        else {
            
            registerData.firstname = firstName as String
            registerData.lastname = lastName as String
             registerData.username = userName as String
            registerData.emailAddress = emailid as String
            registerData.countryCode = "\(countryCode_Btn.titleLabel!.text)"
            registerData.mobileNumber = "\(contactNo_TxtFld.text)"
            registerData.password = "\(password_TxtFld.text!)"
            registerData.confrimPassword = "\(confirm_TxtFld.text!)"
          
            
            let registerAdd_VC = self.storyboard?.instantiateViewController(withIdentifier: "RegisterAddressViewController") as! RegisterAddressViewController
            self.navigationController?.pushViewController(withFlip: registerAdd_VC, animated: true)
           


            
            let parameter=["firstname":"\(Firstname_TxtFld.text!)","lastname":"\(lastName_TxtFld.text!)","username":"\(usernName_TxtFld.text!)","email":"\(emailAddress_TxtFld.text!)","phonenumber":"\(contactNo_TxtFld.text)","phonecode":"\(countryCode_Btn.titleLabel!.text)","password":"\(password_TxtFld.text!)","confirmpassword":"\(confirm_TxtFld.text!)"]
            print("paramsssss", parameter)
//            URL_handler.makeCall("\(reg_form1)", param: parameter, completionHandler: { (responseObject, error) -> () in
//                self.DismissProgress()
//                if(error != nil){
//                    self.view.makeToast(message: kErrorMsg, duration: 3, position: HRToastActivityPositionDefault as AnyObject, title: Appname)
//                } else {
//                    if(responseObject != nil){
//                        let dict:NSDictionary=responseObject!
//                        var status = self.theme.CheckNullValue(dict.objectForKey("status"))!
//                        if(status == "1")
//                        {
//                            
//                            
//                            print("this is: \(dict)")
//                            let registerAdd_VC = self.storyboard?.instantiateViewControllerWithIdentifier("RegisterAddressViewController") as! RegisterAddressViewController
//                            self.navigationController?.pushViewController(withFlip: registerAdd_VC, animated: true)
//                        }
//                        else{
//                            self.theme.AlertView("\(Appname)", Message: "\(self.theme.CheckNullValue(dict.objectForKey("response"))!)", ButtonTitle: "OK")
//                        }
//                        
//                        
//                    }
//                }
//                
//            })
        }
    }
    
    
    func getPlaces(_ searchString: String) {
        
        let request = requestForSearch(searchString)
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request, completionHandler: { data, response, error in
            
            self.handleResponse(data, response: response as? HTTPURLResponse, error: error as! NSError)
            
        }) 
        
        task.resume()
        
    }
    
    func requestForSearch(_ searchString: String) -> URLRequest {
        
        
        
        let params = [
            
            "input": searchString,
            
            // "type": "(\(placeType.description))",
            
            //"type": "",
            
            "key": "\(googleApiKey)"
            
        ]
        print("the url is https://maps.googleapis.com/maps/api/place/autocomplete/json?\(query(params as [String : AnyObject]))")
        
        
        
        return NSMutableURLRequest(
            
            url: URL(string: "https://maps.googleapis.com/maps/api/place/autocomplete/json?\(query(params as [String : AnyObject]))")!
            
            
        ) as URLRequest
        
    }
    
    func query(_ parameters: [String: AnyObject]) -> String {
        var components: [(String, String)] = []
        for key in  (Array(parameters.keys).sorted(by: <)) {
            let value: AnyObject! = parameters[key]
            
            components += [(escape(key), escape("\(value!)"))]
        }
        
        return components.map{"\($0)=\($1)"}.joined(separator: "&")
    }
    
    
    func escape(_ string: String) -> String {
        
        let legalURLCharactersToBeEscaped: CFString = ":/?&=;+!@#$()',*" as CFString
        
        return CFURLCreateStringByAddingPercentEscapes(nil, string as CFString, nil, legalURLCharactersToBeEscaped, CFStringBuiltInEncodings.UTF8.rawValue) as String
        
    }
    
    
    
    func handleResponse(_ data: Data!, response: HTTPURLResponse!, error: NSError!) {
        
        if let error = error {
            
            print("GooglePlacesAutocomplete Error: \(error.localizedDescription)")
            
            return
            
        }
        
        
        
        if response == nil {
            
            print("GooglePlacesAutocomplete Error: No response from API")
            
            return
            
        }
        
        if response.statusCode != 200 {
            
            print("GooglePlacesAutocomplete Error: Invalid status code \(response.statusCode) from API")
            
            return
            
        }
        
        
        do {
            
            let json: NSDictionary = try JSONSerialization.jsonObject(
                
                with: data,
                
                options: JSONSerialization.ReadingOptions.mutableContainers
                
                ) as! NSDictionary
            
            DispatchQueue.main.async(execute: {
                
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                
                
                
                if let predictions = json["predictions"] as? Array<AnyObject> {
                    
                    self.places = predictions.map { (prediction: AnyObject) -> Place in
                        
                        return Place(
                            
                            id: prediction["id"] as! String,
                            
                            description: prediction["description"] as! String
                            
                        )
                        
                    }
                    self.Placesearch_tableview.reload()
                    // self.Location_tableview.reload()
                    
                    
                }
                
            })
        }
            
            
        catch let error as NSError {
            // Catch fires here, with an NSErrro being thrown from the JSONObjectWithData method
            print("A JSON parsing error occurred, here are the details:\n \(error)")
        }
        
        
        // Perform table updates on UI thread
        
        
        
    }
    
    //
    //    func textFieldDidBeginEditing(textField: UITextField) {
    //        if textField == dateTxt_Fld{
    //            self.pickUpDate(self.dateTxt_Fld)
    //        }
    //    }
    
    //    func TextfieldDidChange(textField:UITextField)
    //    {
    //        if(textField == city_txtField)
    //        {
    //            if(city_txtField.text != "")
    //            {
    //                if(places.count == 0)
    //                {
    //                    self.Placesearch_tableview.hidden=true
    //
    //                }
    //
    //                else
    //                {
    //                    self.Placesearch_tableview.hidden=false
    //
    //                }
    //
    //
    //                getPlaces(city_txtField.text!)
    //
    //
    //
    //            }
    //            else
    //            {
    //
    //                places.removeAll()
    //                self.Placesearch_tableview.hidden=true
    //            }
    //        }
    //    }
    
    
    
    //    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool
    //    {
    //
    //        if(textField == city_txtField)
    //        {
    //            if(city_txtField.text != "")
    //            {
    //                if(places.count == 0)
    //                {
    //                    self.Placesearch_tableview.hidden=true
    //
    //                }
    //
    //                else
    //                {
    //                    self.Placesearch_tableview.hidden=false
    //                    
    //                }
    //                
    //                
    //                getPlaces(city_txtField.text!)
    //                
    //                
    //                
    //            }
    //            else
    //            {
    //                
    //                places.removeAll()
    //                self.Placesearch_tableview.hidden=true
    //            }
    //        }
    //
    //        
    //        
    //        
    //     return true
    //    }
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
