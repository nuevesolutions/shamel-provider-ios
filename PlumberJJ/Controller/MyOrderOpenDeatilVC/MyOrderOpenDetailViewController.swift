//
//  MyOrderOpenDetailViewController.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 10/29/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit
import MessageUI



protocol MyOrderOpenDetailViewControllerDelegate {
    
    func  passRequiredParametres(_ fromdate:NSString,todate: NSString,isAscendorDescend: Int,isSortby: NSString)
    
    
}

class MyOrderOpenDetailViewController: RootBaseViewController,GMSMapViewDelegate,UIActionSheetDelegate,UIAlertViewDelegate,UITextFieldDelegate,UITextViewDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,MiscellaneousVCDelegate,UIViewControllerTransitioningDelegate{
    var delegate:MyOrderOpenDetailViewControllerDelegate?
    var LocationTimer:Timer=Timer()
    var backgroundTaskIdentifier: UIBackgroundTaskIdentifier?

    @IBOutlet weak var resonForCanceling: CustomLabelGray!
    @IBOutlet weak var workFlowBtn: UIButton!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var seperatorLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var detailScrollView: UIScrollView!
    var tField: UITextField!
    @IBOutlet weak var MapView: GMSMapView!
    @IBOutlet weak var jobCancelLbl: UILabel!
    @IBOutlet weak var callBtn: UIButton!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var viewInMapsView: UIView!
    @IBOutlet weak var mapContainerView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var openInMapsBtn: UIButton!
   

    var fullAddress : String = String()
    var CancelReasonArr:NSMutableArray=[]
    var CancelTitleArr:NSMutableArray=[]
    var objDetailRec:JobDetailRecord=JobDetailRecord()
   
    var myMaterialView:MiscellaneousVC=MiscellaneousVC()
    
    // var theme:Theme=Theme()
    var jobID:String = ""
    var Getorderstatus : String = ""
    var reasonIdStr:String = ""
  
    @IBOutlet weak var singleBtn: UIButton!
    var actionButton: ActionButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var rejectBtn: UIButton!
    @IBOutlet weak var acceptBtn: UIButton!
    
    @IBOutlet weak var jobIdLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        openInMapsBtn.setTitle(Language_handler.VJLocalizedString("open_in_maps", comment: nil), for: UIControlState())
        
        // For use in foreground
        callBtn.layer.cornerRadius=callBtn.frame.size.width/2
        callBtn.layer.borderWidth=1
        callBtn.layer.borderColor=PlumberGreenColor.cgColor
        callBtn.layer.masksToBounds=true
        
        bottomView.isHidden=true
        detailScrollView.isHidden=true
        
     
        NotificationCenter.default.addObserver(self, selector: #selector(MyOrderOpenDetailViewController.methodOfReceivedNotificationNetworkDetail(_:)), name:NSNotification.Name(rawValue: kJobCancelNotif), object: nil)
       
        
        loadActionBtn()
        // Do any additional setup after loading the view.
    }
    
  

    func loadActionBtn(){
        let callImg = UIImage(named: "ImgCall")!
        let chatImg = UIImage(named: "imgChat")!
        let msgImg = UIImage(named: "imgMsg")!
        let mailImg = UIImage(named: "imgMail")!
        
        let callBtn = ActionButtonItem(title: "", image: callImg)
        callBtn.action = { item in
            self.callNumber(self.objDetailRec.jobPhone as String)
        }
        
        let chatBtn = ActionButtonItem(title: "", image: chatImg)
        chatBtn.action = { item in
            let objChatVc = self.storyboard!.instantiateViewController(withIdentifier: "ChatVCSID") as! MessageViewController
            
            NSLog("the task id =%@ and job id =%@",self.objDetailRec.jobIdentity,self.jobID )
            objChatVc.jobId=self.objDetailRec.jobIdentity
            objChatVc.Userid = self.objDetailRec.Userid
            objChatVc.username = self.objDetailRec.jobUserName
            objChatVc.Userimg = self.objDetailRec.userimage
            objChatVc.RequiredJobid = self.objDetailRec.jobIdentity
            self.navigationController!.pushViewController(withFlip: objChatVc, animated: true)
            
        }
        let messageBtn = ActionButtonItem(title: "", image: msgImg)
        messageBtn.action = { item in
            
            let messageComposeVC = self.configuredMessageComposeViewController("",number:"\(self.objDetailRec.jobPhone)")
            if MFMessageComposeViewController.canSendText() {
                self.present(messageComposeVC, animated: true, completion: nil)
                
            }
        }
        
        let mailBtn = ActionButtonItem(title: "", image: mailImg)
        mailBtn.action = { item in
            
            let mailComposeViewController = self.configuredMailComposeViewController()
            if MFMailComposeViewController.canSendMail() {
                self.present(mailComposeViewController, animated: true, completion: nil)
            } else {
                self.showSendMailErrorAlert()
            }}
        
        actionButton = ActionButton(attachedToView: self.view, items: [callBtn,messageBtn,mailBtn,chatBtn ])
        actionButton.action = { button in button.toggleMenu() }
        actionButton.setTitle("+", forState: UIControlState())
        
        actionButton.backgroundColor = UIColor(red: 51/255.0, green: 153/255.0, blue: 255/255.0, alpha:1.0)
    }
    func showSendMailErrorAlert() {
        self.view.makeToast(message:Language_handler.VJLocalizedString("not_send_email", comment: nil), duration: 3, position: HRToastPositionDefault as AnyObject, title: Language_handler.VJLocalizedString("not_send_email_title", comment: nil))
        
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["\(self.objDetailRec.jobEmail)"])
        mailComposerVC.setSubject("\(self.objDetailRec.jobTitle)")
        
        
        return mailComposerVC
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    func canSendText() -> Bool {
        return MFMessageComposeViewController.canSendText()
    }
    func configuredMessageComposeViewController(_ message:String,number:String) -> MFMessageComposeViewController {
        let messageComposeVC = MFMessageComposeViewController()
        messageComposeVC.messageComposeDelegate = self
        
        
        //  Make sure to set this property to self, so that the controller can be dismissed!
        //        messageComposeVC.recipients = textMessageRecipients
        messageComposeVC.body = "\(message)"
        messageComposeVC.recipients = [number]
        
        return messageComposeVC
    }
    
    // MFMessageComposeViewControllerDelegate callback - dismisses the view controller when the user is finished with it
    @objc func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func methodOfReceivedNotificationNetworkDetail(_ notification: Notification){
        loadDetail()
    }
    override func viewWillAppear(_ animated: Bool) {
        loadDetail()
    }
    func loadDetail(){
        self.showProgress()
        GetJobDetails()
    }
    
    func set_mapView()
    {
        let latitude = (objDetailRec.jobLat as NSString).doubleValue
        let longitude = (objDetailRec.jobLong as NSString).doubleValue
        let camera = GMSCameraPosition.camera(withLatitude: latitude,
                                                          longitude:longitude, zoom:15)
        
        MapView.camera=camera
        MapView.frame=MapView.frame
        let marker = GMSMarker()
        marker.position = camera.target
        marker.appearAnimation = .pop
        marker.icon = UIImage(named: "MapPin")
        marker.map = MapView
        // MapView.settings.setAllGesturesEnabled(false)
        
    }
    
  
    func setDatasForDescription(){
        
        callBtn.layer.cornerRadius=callBtn.frame.size.width/2
        callBtn.layer.borderWidth=1
        callBtn.layer.borderColor=PlumberThemeColor.cgColor
        callBtn.layer.masksToBounds=true
        titleLbl.numberOfLines=0
        
        
        titleLbl.sizeToFit()
        seperatorLbl.frame=CGRect(x: seperatorLbl.frame.origin.x, y: titleLbl.frame.origin.y+titleLbl.frame.size.height+3, width: seperatorLbl.frame.size.width, height: seperatorLbl.frame.size.height);
        
        let desStr : String =  "\(objDetailRec.jobDesc)"
        
        let heigth : CGFloat = desStr.height(constraintedWidth: descLbl.frame.size.width, font: UIFont.systemFont(ofSize: 16))
       var heightVal : CGFloat = heigth + 5
        descLbl.adjustsFontSizeToFitWidth = false
        descLbl.numberOfLines=0
        descLbl.frame=CGRect(x: descLbl.frame.origin.x, y: seperatorLbl.frame.origin.y+seperatorLbl.frame.size.height + 4 , width: descLbl.frame.size.width, height: heightVal);
        
        
        descLbl.sizeToFit()
        if objDetailRec.Cancelreason == ""
        {
               self.resonForCanceling.isHidden = true
              topView.frame=CGRect(x: topView.frame.origin.x, y: topView.frame.origin.y, width: topView.frame.size.width, height: descLbl.frame.size.height + descLbl.frame.origin.y + 10);
        }
        else{
             self.resonForCanceling.isHidden = false
              self.resonForCanceling.text = "Reason for cancellation:  \(objDetailRec.Cancelreason)"
            self.resonForCanceling.frame = CGRect(x: resonForCanceling.frame.origin.x, y: descLbl.frame.origin.y+descLbl.frame.size.height + 4 , width: resonForCanceling.frame.size.width, height: resonForCanceling.frame.size.height);
            
              topView.frame=CGRect(x: topView.frame.origin.x, y: topView.frame.origin.y, width: topView.frame.size.width, height: resonForCanceling.frame.size.height + resonForCanceling.frame.origin.y + 10);
        }
    
        
      
        
        
        
        addressLbl.numberOfLines=0
        
        
        contentView.frame=CGRect(x: contentView.frame.origin.x, y: topView.frame.origin.y+topView.frame.size.height, width: contentView.frame.size.width, height: contentView.frame.size.height);
        detailScrollView.contentSize=CGSize(width: self.view.frame.size.width, height: contentView.frame.size.height+contentView.frame.origin.y+40);
        MapView.frame=CGRect(x: MapView.frame.origin.x, y: MapView.frame.origin.y, width: MapView.frame.size.width, height: contentView.frame.size.height+35)
    }
    func GetJobDetails(){
        
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)",
                                 "job_id":"\(jobID)"]
        print(jobID)
        
        url_handler.makeCall(JobDetailUrl, param: Param as NSDictionary) {
            (responseObject, error) -> () in
            self.DismissProgress()
            
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
            }
            else
            {
                if(responseObject != nil && (responseObject?.count)!>0)
                {
                    let responseObject = responseObject!
                    let status=self.theme.CheckNullValue(responseObject.object(forKey: "status") as AnyObject)! as NSString
                    if(status == "1")
                    {
                        self.detailScrollView.isHidden=false
                        self.bottomView.isHidden=false
                        if(((responseObject.object(forKey: "response") as AnyObject).object(forKey: "job")! as AnyObject).count>0){
                            let dict:NSDictionary=(responseObject.object(forKey: "response") as AnyObject).object(forKey: "job") as! NSDictionary
                            
                            self.objDetailRec.jobIdentity=self.theme.CheckNullValue(dict.object(forKey: "task_id") as AnyObject)!
                            
                            self.objDetailRec.Currency=self.theme.CheckNullValue(dict.object(forKey: "currency") as AnyObject)!
                            self.objDetailRec.Submit_rating = self.theme.CheckNullValue(dict.object(forKey: "submit_ratings") as AnyObject)!
                            
                            self.objDetailRec.jobDate=self.theme.CheckNullValue(dict.object(forKey: "job_date") as AnyObject)!
                            
                            self.objDetailRec.jobTime=self.theme.CheckNullValue(dict.object(forKey: "job_time") as AnyObject)!
                            
                            self.objDetailRec.jobTitle=self.theme.CheckNullValue(dict.object(forKey: "job_type") as AnyObject)!
                            
                            self.objDetailRec.jobDesc=self.theme.CheckNullValue(dict.object(forKey: "instruction") as AnyObject)!
                            
                            self.objDetailRec.jobStatus=self.theme.CheckNullValue(dict.object(forKey: "btn_group") as AnyObject)!
                            self.objDetailRec.Cancelreason = self.theme.CheckNullValue(dict.object(forKey: "cancelreason") as AnyObject)!
                          
                            self.objDetailRec.Userid = self.theme.CheckNullValue(dict.object(forKey: "user_id") as AnyObject)!
                            self.objDetailRec.RequireJobId = self.theme.CheckNullValue(self.jobID as AnyObject)!
                            self.objDetailRec.jobUserName=self.theme.CheckNullValue(dict.object(forKey: "user_name") as AnyObject)!
                            
                            self.objDetailRec.currency_symbol = self.theme.getCurrencyCode((dict.object(forKey: "currency_code") as AnyObject) as! String)
                            
                            self.objDetailRec.jobEmail=self.theme.CheckNullValue(dict.object(forKey: "user_email") as AnyObject)!
                            
                            self.objDetailRec.jobPhone=self.theme.CheckNullValue(dict.object(forKey: "user_mobile") as AnyObject)!
                            
                            let location =  self.theme.CheckNullValue(dict.object(forKey: "job_location") as AnyObject)!.replacingOccurrences(of: ", ,", with: ",")
                            self.objDetailRec.jobLocation=location
                            self.objDetailRec.jobLat=self.theme.CheckNullValue(dict.object(forKey: "location_lat") as AnyObject)!
                            self.objDetailRec.userimage = self.theme.CheckNullValue(dict.object(forKey: "user_image") as AnyObject)!
                            //CurLaat = self.objDetailRec.job
                            self.objDetailRec.jobLong=self.theme.CheckNullValue(dict.object(forKey: "location_lon") as AnyObject)!
                            
                            self.objDetailRec.jobBtnStatus = self.theme.CheckNullValue(dict.object(forKey: "btn_group") as AnyObject)!
                            
                            self.objDetailRec.job_Status=self.theme.CheckNullValue(dict.object(forKey: "job_status") as AnyObject)!
                            self.objDetailRec.cashoption = self.theme.CheckNullValue(dict.object(forKey: "cash_option") as AnyObject)!
                            self.SetDatasToDetail()
                            
                        }else{
                            self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
                        }
                    }
                    else
                    {
                        self.view.makeToast(message:kErrorMsg, duration: 5, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
                    }
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
                }
            }
            
        }
    }
    func SetDatasToDetail(){
        
        
        dateLbl.text="\(Language_handler.VJLocalizedString("date", comment: nil)) : \(objDetailRec.jobDate)"
        timeLbl.text="\(Language_handler.VJLocalizedString("time", comment: nil)) : \(objDetailRec.jobTime)"
        titleLbl.text="\(objDetailRec.jobTitle)"
        
        var myMutableString : NSMutableAttributedString  = NSMutableAttributedString()
        myMutableString = NSMutableAttributedString(string: "\(objDetailRec.jobDesc)", attributes: nil)
        
        //myMutableString.addAttribute(NSFontAttributeName, value:UIFont.boldSystemFont(ofSize: 16) , range: NSRange(location:1,length:9))
        
       // var myMutableString1 = NSMutableAttributedString()
        //myMutableString1 = NSMutableAttributedString(string:"Bio : \(Provider_Detail.bio)", attributes: nil)
        //myMutableString1.addAttribute(NSFontAttributeName, value:UIFont.boldSystemFont(ofSize: 16) , range: NSRange(location:1,length:4))
        descLbl.numberOfLines = 0
       // descLbl.lineBreakMode = .byWordWrapping
        print("DescLbl details: \(objDetailRec.jobDesc)")
        descLbl.text =  "\(objDetailRec.jobDesc)"
        
        //"Description : bdhhdhdjdjjd \r\n Time Frame :2-3 hrs \r\n Parts Needed : belt \r\n Estimates :2 hrs \r\n Price Range :300-500 /- \r\n Parking :Yes"
        
        jobIdLbl.text="\(Language_handler.VJLocalizedString("job_id", comment: nil)) : \(jobID)"
        addressLbl.text = self.theme.CheckNullValue(theme.getAddressForLatLng( self.objDetailRec.jobLat as String, longitude: self.objDetailRec.jobLong as String, status :"") as AnyObject)
        
        
        //NSLog( "The user id =%@",objDetailRec.jobIdentity);
        //  addressLbl.text="\(objDetailRec.jobLocation)"
        setDatasForDescription()
        set_mapView()
        setBottomViewWithButton(objDetailRec.jobBtnStatus)
        
    }
    
    @IBAction func didClickBackBtn(_ sender: AnyObject) {
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "SortingNotification"), object:self,userInfo: nil )
        NotificationCenter.default.post(name: Notification.Name(rawValue: "SortingJobNotification"), object:self,userInfo: nil )
        
        
        self.navigationController?.popViewControllerWithFlip(animated: true)
        
        
    }
    
    @IBAction func didClickOpenInMapsBtn(_ sender: AnyObject) {
        moveToLocVc(false)
    }
    
    func moveToLocVc(_ isShowArriveBtn:Bool){//LocationVCSID
        let ObjLocVc=self.storyboard!.instantiateViewController(withIdentifier: "LocationVCSID")as! LocationViewController
        ObjLocVc.isShowArriveBtn=isShowArriveBtn
        ObjLocVc.jobStatus = self.objDetailRec.jobBtnStatus
        ObjLocVc.mapLaat=Double(objDetailRec.jobLat)!
        ObjLocVc.mapLong=Double(objDetailRec.jobLong)!
        trackingDetail.userLat = Double(objDetailRec.jobLat)!
        trackingDetail.userLong = Double(objDetailRec.jobLong)!
        
        ObjLocVc.addressStr=addressLbl.text!
        ObjLocVc.phoneStr=objDetailRec.jobPhone
        ObjLocVc.getUsername = objDetailRec.jobUserName
        ObjLocVc.jobId=self.objDetailRec.jobIdentity
        ObjLocVc.userId = self.objDetailRec.Userid
        self.navigationController?.pushViewController(withFlip: ObjLocVc, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setBottomViewWithButton(_ statStr:String){
        if(statStr=="3"){
            openInMapsBtn.isHidden=false
            viewInMapsView.isHidden = false
    backgroundTaskIdentifier = UIApplication.shared.beginBackgroundTask(expirationHandler:
                {
                    UIApplication.shared.endBackgroundTask(self.backgroundTaskIdentifier!)
            })
              LocationTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(MyOrderOpenDetailViewController.ReconnectMethod), userInfo: nil, repeats: true)
        }else{
            openInMapsBtn.isHidden=true
            viewInMapsView.isHidden = true
            LocationTimer.invalidate()
            LocationTimer.fire()
            
        }
        
        if((statStr as NSString).length>0){
            bottomView.isHidden=false
            if(statStr=="1"){
                
                if  self.objDetailRec.job_Status == "0"{
                    
                    
                    rejectBtn.isHidden=true
                    acceptBtn.isHidden=true
                    singleBtn.isHidden=false
                    singleBtn.setTitle(Language_handler.VJLocalizedString("expired_job", comment: nil), for: UIControlState())
                }
                else if (Getorderstatus == "Missed Job")
                {
                    rejectBtn.isHidden=true
                    acceptBtn.isHidden=true
                    singleBtn.isHidden=false
                    singleBtn.setTitle(Language_handler.VJLocalizedString("miss_job", comment: nil), for: UIControlState())
                }
                else
                {
                    rejectBtn.isHidden=false
                    acceptBtn.isHidden=false
                    singleBtn.isHidden=true
                    //rejectBtn.setTitle("Reject", forState: UIControlState.Normal)
                    acceptBtn.setTitle(Language_handler.VJLocalizedString("accept", comment: nil), for: UIControlState())
                    acceptBtn.frame=CGRect(x: rejectBtn.frame.origin.x + rejectBtn.frame.size.width, y: acceptBtn.frame.origin.y, width: rejectBtn.frame.size.width , height: acceptBtn.frame.size.height);
                    rejectBtn.setTitle(Language_handler.VJLocalizedString("reject", comment: nil), for: UIControlState())
                    
                }
                
                
            }
            else if(statStr=="2"){
                
                
                rejectBtn.isHidden=false
                acceptBtn.isHidden=false
                singleBtn.isHidden=true
                rejectBtn.setTitle(Language_handler.VJLocalizedString("cancel", comment: nil), for: UIControlState())
                acceptBtn.setTitle(Language_handler.VJLocalizedString("start_off", comment: nil), for: UIControlState())
                acceptBtn.frame=CGRect(x: rejectBtn.frame.origin.x + rejectBtn.frame.size.width, y: acceptBtn.frame.origin.y, width: rejectBtn.frame.size.width , height: acceptBtn.frame.size.height);
                
            }
            else if(statStr=="3"){
                
                rejectBtn.isHidden=false
                acceptBtn.isHidden=false
                singleBtn.isHidden=true
                rejectBtn.setTitle(Language_handler.VJLocalizedString("cancel", comment: nil), for: UIControlState())
                acceptBtn.setTitle(Language_handler.VJLocalizedString("arrived", comment: nil), for: UIControlState())
                acceptBtn.frame=CGRect(x: rejectBtn.frame.origin.x + rejectBtn.frame.size.width, y: acceptBtn.frame.origin.y, width: rejectBtn.frame.size.width , height: acceptBtn.frame.size.height);
            }
            else if(statStr=="4"){
                
                rejectBtn.isHidden=false
                acceptBtn.isHidden=false
                singleBtn.isHidden=true
                rejectBtn.setTitle(Language_handler.VJLocalizedString("cancel", comment: nil), for: UIControlState())
                acceptBtn.setTitle(Language_handler.VJLocalizedString("start_job", comment: nil), for: UIControlState())
                
                acceptBtn.frame=CGRect(x: rejectBtn.frame.origin.x + rejectBtn.frame.size.width, y: acceptBtn.frame.origin.y, width: rejectBtn.frame.size.width , height: acceptBtn.frame.size.height);
            }
            else if(statStr=="5"){
                
                rejectBtn.isHidden=true
                acceptBtn.isHidden=true
                singleBtn.isHidden=false
                singleBtn.setTitle(Language_handler.VJLocalizedString("complete_job", comment: nil), for: UIControlState())
                
            }
            else if(statStr=="6"){
                
                rejectBtn.isHidden=true
                acceptBtn.isHidden=true
                singleBtn.isHidden=false
                singleBtn.setTitle(Language_handler.VJLocalizedString("payment", comment: nil), for: UIControlState())
                
            }
            else if(statStr=="7"){
                
                jobCancelLbl.isHidden=false
                rejectBtn.isHidden=true
                acceptBtn.isHidden=true
                singleBtn.isHidden=true
                jobCancelLbl.text=Language_handler.VJLocalizedString("job_cancelled", comment: nil)
            }
            else {
                
                if self.objDetailRec.Submit_rating == "Yes"
                {
                    rejectBtn.isHidden=false
                    acceptBtn.isHidden=false
                    singleBtn.isHidden=true
                    rejectBtn.setTitle(Language_handler.VJLocalizedString("more_info", comment: nil), for: UIControlState())
                    acceptBtn.setTitle(Language_handler.VJLocalizedString("Rating", comment: nil), for: UIControlState())
                    
                    acceptBtn.frame=CGRect(x: rejectBtn.frame.origin.x + rejectBtn.frame.size.width, y: acceptBtn.frame.origin.y, width: rejectBtn.frame.size.width , height: acceptBtn.frame.size.height);
                    
                }
                else{
                    rejectBtn.isHidden=true
                    acceptBtn.isHidden=true
                    singleBtn.isHidden=false
                    singleBtn.setTitle(Language_handler.VJLocalizedString("more_info", comment: nil), for: UIControlState())
                    
                }
                
            }
            //if()
        }else{
            bottomView.isHidden=true
        }
        
    }
    func ReconnectMethod()
    {
        let objUserRecs:UserInfoRecord=theme.GetUserDetails(); SocketIOManager.sharedInstance.emitTracking(self.objDetailRec.Userid as String, tasker: objUserRecs.providerId as String , task: jobID as String, lat:"\(Currentlat)" , long: "\(Currentlng)", bearing: "\(Bearing)",lastdrive: "\(lastDriving)")
        
    }
    
    @IBAction func didClickSingleBtn(_ sender: UIButton) {
        if(sender.titleLabel?.text==Language_handler.VJLocalizedString("complete_job", comment: nil)){
            // self.displayViewController(.BottomTop)
            self.sendJobDetails()
        }else if (sender.titleLabel?.text==Language_handler.VJLocalizedString("payment", comment: nil)){
            moveToPaymentOrMoreInfoVC()
        }
        else if (sender.titleLabel?.text==Language_handler.VJLocalizedString("more_info", comment: nil)){
            moveToPaymentOrMoreInfoVC()
        }
        
    }
    func moveToPaymentOrMoreInfoVC(){
        
        
        let objFarevc = self.storyboard!.instantiateViewController(withIdentifier: "FareSummaryVCSID") as! FareSummaryViewController
        objFarevc.jobIDStr=jobID
        objFarevc.cashoption = self.objDetailRec.cashoption as String
        self.navigationController!.pushViewController(withFlip: objFarevc, animated: true)
        
        
        
    }
    @IBAction func didClickRejectBtn(_ sender: UIButton) {
        if(sender.titleLabel?.text==Language_handler.VJLocalizedString("reject", comment: nil)){
            
            getCancelReasonForJob()
        }else if (sender.titleLabel?.text==Language_handler.VJLocalizedString("cancel", comment: nil)){
            getCancelReasonForJob()
        }
        else if (sender.titleLabel?.text==Language_handler.VJLocalizedString("more_info", comment: nil))
        {
            moveToPaymentOrMoreInfoVC()
            
        }
    }
    @IBAction func didClickAcceptBtn(_ sender: UIButton) {
        if(sender.titleLabel?.text==Language_handler.VJLocalizedString("accept", comment: nil)){
            jobUpdateStatus(AcceptRideUrl as NSString)
        }
        else if (sender.titleLabel?.text==Language_handler.VJLocalizedString("start_off", comment: nil)){
            jobUpdateStatus(StartDestinationUrl as NSString)
            
        }else if (sender.titleLabel?.text==Language_handler.VJLocalizedString("arrived", comment: nil)){
            jobUpdateStatus(ArrivedDestinationUrl as NSString)
        }
        else if (sender.titleLabel?.text==Language_handler.VJLocalizedString("start_job", comment: nil)){
            jobUpdateStatus(JobStartUrl as NSString)
        }
            
        else if (sender.titleLabel?.text == Language_handler.VJLocalizedString("Rating", comment: nil))
        {
            let objFarevc = self.storyboard!.instantiateViewController(withIdentifier: "RatingsVCSID") as! RatingsViewController
            objFarevc.jobIDStr = jobID
            self.navigationController!.pushViewController(withFlip: objFarevc, animated: true)
            
        }
    }
    @IBAction func didClickWorkFlowBtn(_ sender: UIButton) {//
        
        let ObjStepsVc=self.storyboard!.instantiateViewController(withIdentifier: "StepsTreeVCSID")as! StepsTreeViewController
        ObjStepsVc.JobIdStr=jobID
        self.navigationController?.pushViewController(withFlip: ObjStepsVc, animated: false)
    }
    
    func jobUpdateStatus(_ statusType:NSString){
        if Double(Currentlat) != nil{
            self.showProgress()
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)",
                                     "job_id":"\(jobID)",
                                     "provider_lat":"\(Currentlat)",
                                     "provider_lon":"\(Currentlng)"]
            // print(Param)
            if statusType as String == StartDestinationUrl{
                trackingDetail.partnerLat = Double(Currentlat)!
                trackingDetail.partnerLong = Double(Currentlng)!
            }
            acceptBtn.isUserInteractionEnabled=false
            rejectBtn.isUserInteractionEnabled=false
            url_handler.makeCall(statusType as String, param: Param as NSDictionary) {
                (responseObject, error) -> () in
                self.DismissProgress()
                self.acceptBtn.isUserInteractionEnabled=true
                self.rejectBtn.isUserInteractionEnabled=true
                if(error != nil)
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
                }
                else
                {
                    if(responseObject != nil && (responseObject?.count)!>0)
                    {
                        let responseObject = responseObject!
                        let status=self.theme.CheckNullValue(responseObject.object(forKey: "status") as AnyObject)! as NSString
                        
                        if(status == "1")
                        {
                            self.detailScrollView.isHidden=false
                            self.bottomView.isHidden=false
                            
                            let dict:NSDictionary=(responseObject.object(forKey: "response"))! as! NSDictionary
                            self.objDetailRec.jobBtnStatus=self.theme.CheckNullValue(dict.object(forKey: "btn_group") as AnyObject)!
                            
                            
                            NSLog("Get btnGroup Response=%@", self.objDetailRec.jobBtnStatus)
                            self.view.makeToast(message:self.theme.CheckNullValue(dict.object(forKey: "message") as AnyObject)!, duration: 2, position: HRToastPositionDefault as AnyObject, title: Language_handler.VJLocalizedString("success", comment: nil))
                            var messagestr : String = self.theme.CheckNullValue(dict.object(forKey: "message") as AnyObject)!
                            self.setBottomViewWithButton(self.objDetailRec.jobBtnStatus)
                            
                            if (messagestr == "Job Rejected Successfully" )
                            {
                                self.GetJobDetails()
                                
                            }
                            
                            
                            
                        }
                        else
                        {//alertImg
                            _ = UIImage(named: "alertImg")
                            
                            self.theme.AlertView("\(appNameJJ)", Message: self.theme.CheckNullValue(responseObject.object(forKey: "response") as AnyObject)!, ButtonTitle: kOk)
                            
                        }
                    }
                    else
                    {
                        self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
                    }
                }
                
            }
        }
    }
    
    
    func jobUpdateCancelStatus(_ statusType:NSString){
        
        
        self.showProgress()
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)",
                                 "job_id":"\(jobID)",
                                 "reason":"\(reasonIdStr)"]
        // print(Param)
        
        url_handler.makeCall(statusType as String, param: Param as NSDictionary) {
            (responseObject, error) -> () in
            self.DismissProgress()
            
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject as AnyObject, title: appNameJJ)
            }
            else
            {
                if(responseObject != nil && (responseObject?.count)!>0)
                {
                    let responseObject = responseObject!
                    let status=self.theme.CheckNullValue(responseObject.object(forKey: "status") as AnyObject)! as NSString
                    if(status == "1")
                    {
                        NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadCurrentJob"), object: nil)
                        self.detailScrollView.isHidden=false
                        self.bottomView.isHidden=false
                        let dict:NSDictionary=(responseObject.object(forKey: "response"))! as! NSDictionary
                        self.objDetailRec.jobBtnStatus=self.theme.CheckNullValue(dict.object(forKey: "btn_group") as AnyObject)!
                        self.setBottomViewWithButton(self.objDetailRec.jobBtnStatus)
                        
                        
                        self.theme.AlertView( Language_handler.VJLocalizedString("success", comment: nil), Message: self.theme.CheckNullValue(dict.object(forKey: "message"))!, ButtonTitle: kOk)
                        
                        
                        
                    }
                    else
                    {//alertImg
                        
                        self.theme.AlertView("\(appNameJJ)", Message: self.theme.CheckNullValue(responseObject.object(forKey: "response"))!, ButtonTitle: kOk)
                        
                    }
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
                }
            }
            
        }
    }
    
    
    func getCancelReasonForJob(){
        self.showProgress()
        acceptBtn.isUserInteractionEnabled=false
        rejectBtn.isUserInteractionEnabled=false
        
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["provider_id":"\(objUserRecs.providerId)"]
        // print(Param)
        
        url_handler.makeCall(CancelReasonUrl, param: Param as NSDictionary) {
            (responseObject, error) -> () in
            self.DismissProgress()
            self.acceptBtn.isUserInteractionEnabled=true
            self.rejectBtn.isUserInteractionEnabled=true
            
            if(error != nil)
            {
                self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
            }
            else
            {
                if(responseObject != nil && (responseObject?.count)!>0)
                {
                    let responseObject = responseObject!
                    let status=self.theme.CheckNullValue(responseObject.object(forKey: "status") as AnyObject)! as NSString
                    if(status == "1")
                    {
                        self.detailScrollView.isHidden=false
                        self.bottomView.isHidden=false
                        if((((responseObject.object(forKey: "response") as AnyObject).object(forKey: "reason")! as AnyObject).count)!>0){
                            let  listArr:NSArray=(responseObject.object(forKey: "response") as AnyObject).object(forKey: "reason") as! NSArray
                            if(self.CancelReasonArr.count>0){
                                self.CancelReasonArr.removeAllObjects()
                                self.CancelTitleArr.removeAllObjects()
                            }
                            for (_, element) in listArr.enumerated() {
                                let objRec:CancelReasonRecord=CancelReasonRecord()
                                objRec.reasonId=self.theme.CheckNullValue((element as AnyObject).object(forKey: "id") as AnyObject)!
                                objRec.reason=self.theme.CheckNullValue((element as AnyObject).object(forKey: "reason") as AnyObject)!
                                self.CancelReasonArr.add(objRec)
                                self.CancelTitleArr.add(objRec.reason)
                            }
                            let objRec:CancelReasonRecord=CancelReasonRecord()
                            objRec.reasonId="1"
                            objRec.reason=Language_handler.VJLocalizedString("other", comment: nil)
                            self.CancelReasonArr.add(objRec)
                            self.CancelTitleArr.add(objRec.reason)
                            
                            if(self.CancelTitleArr.count==0){
                                self.theme.AlertView(Language_handler.VJLocalizedString("sorry", comment: nil), Message:Language_handler.VJLocalizedString("no_reason", comment: nil), ButtonTitle: kOk)
                                
                                
                            }else{
                                let actionSheet = UIActionSheet(title:Language_handler.VJLocalizedString("select_reason", comment: nil), delegate: self, cancelButtonTitle:Language_handler.VJLocalizedString("dont_cancel", comment: nil) , destructiveButtonTitle: nil)
                                for str in self.CancelTitleArr{
                                    actionSheet.addButton(withTitle: str as? String)
                                }
                                actionSheet.show(in: self.view)
                            }
                            
                            
                        }else{
                            self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
                        }
                    }
                    else
                    {//alertImg
                        
                        let messagestr :String = self.theme.CheckNullValue(responseObject.object(forKey: "response") as AnyObject)!
                        self.theme.AlertView("\(appNameJJ)", Message: messagestr, ButtonTitle: kOk)
                        
                    }
                }
                else
                {
                    self.view.makeToast(message:kErrorMsg, duration: 3, position: HRToastPositionDefault as AnyObject, title: appNameJJ)
                }
            }
            
        }
    }
    
    
    
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int)
    {
        
        
        if(buttonIndex>0){
            
            
            if actionSheet.buttonTitle(at: buttonIndex)! == Language_handler.VJLocalizedString("other", comment: nil)
            {
                
                
                let alert = UIAlertController(title: Language_handler.VJLocalizedString("reason", comment: nil), message: "", preferredStyle: .alert)
                
                alert.addTextField(configurationHandler: configurationTextField)
                alert.addAction(UIAlertAction(title: Language_handler.VJLocalizedString("cancel", comment: nil), style: .cancel, handler:handleCancel))
                alert.addAction(UIAlertAction(title:Language_handler.VJLocalizedString("done", comment: nil) , style: .default, handler:{ (UIAlertAction) in
                    
                    
                    self.reasonIdStr=self.tField.text!
                    self.jobUpdateCancelStatus(CancelJobUrl as NSString)
                    
                }))
                self.present(alert, animated: true, completion: {
                    print("completion block")
                })
                
            }
            else
                
            {
                
                let objReason:CancelReasonRecord=CancelReasonArr.object(at: buttonIndex-1) as! CancelReasonRecord
                reasonIdStr=objReason.reason
                let createReasonAlert: UIAlertView = UIAlertView()
                
                createReasonAlert.delegate = self
                
                createReasonAlert.title = appNameJJ
                createReasonAlert.message = Language_handler.VJLocalizedString("cancel_confirmation", comment: nil)
                createReasonAlert.addButton(withTitle: Language_handler.VJLocalizedString("no", comment: nil))
                createReasonAlert.addButton(withTitle: Language_handler.VJLocalizedString("yes", comment: nil))
                
                createReasonAlert.show()
            }
            
            
        }
        
        
    }
    
    
    
    func configurationTextField(_ textField: UITextField!)
    {
        
        textField.placeholder = Language_handler.VJLocalizedString("reason_placeholder", comment: nil)
        
        tField = textField
    }
    
    func handleCancel(_ alertView: UIAlertAction!)
    {
        print("Cancelled !!")
    }
    
    func alertView(_ View: UIAlertView, clickedButtonAt buttonIndex: Int){
        
        switch buttonIndex{
            
        case 1:
            jobUpdateCancelStatus(CancelJobUrl as NSString)
            break;
            
        default:
            
            break;
            //Some code here..
            
        }
    }
  
    func sendJobDetails(){
       
        self.displayViewController1(.bottomBottom)
    }
    
    
    func dismissPopupViewController1(_ animationType: SLpopupViewAnimationType) {
        let sourceView:UIView = self.getTopView()
        let popupView:UIView = sourceView.viewWithTag(kpopupViewTag)!
        let overlayView:UIView = sourceView.viewWithTag(kOverlayViewTag)!
        switch animationType {
        case .bottomTop, .bottomBottom, .topTop, .topBottom, .leftLeft, .leftRight, .rightLeft, .rightRight:
            self.slideViewOut(popupView, sourceView: sourceView, overlayView: overlayView, animationType: animationType)
        default:
            fadeViewOut(popupView, sourceView: sourceView, overlayView: overlayView)
        }
        
        
    }
    
    func displayViewController1(_ animationType: SLpopupViewAnimationType) {
        myMaterialView = MiscellaneousVC(nibName:"MaterialDesignView", bundle: nil)
        myMaterialView.delegate = self
        myMaterialView.jobIDStr=jobID
        myMaterialView.currency=self.objDetailRec.currency_symbol
        
        myMaterialView.transitioningDelegate = self
        
        myMaterialView.modalPresentationStyle = .custom
        self.navigationController?.present(myMaterialView, animated: true, completion: nil)
        //self.navigationController!.pushViewController(withFlip: objFarevc, animated: true)
//        self.presentpopupViewController(myMaterialView, animationType: animationType, completion: { () -> Void in
//
//        })
    }
    
    
    func pressedCancelMaterial(_ sender: MiscellaneousVC) {
       self.dismiss(animated: true, completion: nil)
        loadDetail()
    }
    
    //    #pragma mark - UIViewControllerTransitionDelegate -
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
        return PresentingAnimationController()
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
        return DismissingAnimationController()
    }
    
    
    @IBAction func didClickPhoneBtn(_ sender: AnyObject) {
        
        //        callNumber(objDetailRec.jobPhone as String)
    }
    fileprivate func callNumber(_ phoneNumber:String) {
        
        print("\(phoneNumber) get mnuber")
        
        //        let Number:String="\(phoneNumber)"
        //        let trimmedString = Number.stringByReplacingOccurrencesOfString("+ ", withString: "")
        
        
        if let phoneCallURL:URL = URL(string:"tel://"+"\(phoneNumber)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.openURL(phoneCallURL);
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}


