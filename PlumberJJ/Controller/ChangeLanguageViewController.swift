//
//  ChangeLanguageViewController.swift
//  PlumberJJ
//
//  Created by Casperon iOS on 16/10/2017.
//  Copyright © 2017 Casperon Technologies. All rights reserved.
//

import UIKit

class ChangeLanguageViewController: UIViewController {
    
    @IBOutlet weak var ChangeLanguagelable: UILabel!
    
    @IBOutlet var Tamil: UIButton!
    
    @IBOutlet weak var English: UIButton!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.Tamil.setTitle("Tamil", for: UIControlState())
        ChangeLanguagelable.text = theme.setLang("Language Change")
        
        self.Tamil.layer.cornerRadius = 25
        self.Tamil.clipsToBounds = true
        self.English.layer.cornerRadius = 25
        self.English.clipsToBounds = true
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func Tamil(_ sender: AnyObject) {
        //        kLanguage = "ta"
        theme.saveLanguage("ta")
        theme.SetLanguageToApp()

        SwapLanguage(language: theme.getAppLanguage())
    }
    
    
    @IBAction func English(_ sender: AnyObject)
    {
        kLanguage = "en"
        
        
        theme.saveLanguage("en")
        theme.SetLanguageToApp()
        SwapLanguage(language: theme.getAppLanguage())
        
    }
    @IBAction func back_btn(_ sender: AnyObject) {
        self.view.endEditing(true)
        self.frostedViewController.view.endEditing(true)
        self.frostedViewController.presentMenuViewController()
    }
    
    
    
    
    
}
