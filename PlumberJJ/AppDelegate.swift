//
//  AppDelegate.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 10/27/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit
import HockeySDK
import AVFoundation
import UserNotifications
import XMPPFramework
import XMLReader
import CocoaLumberjack

var ratingRec : RatingsRecord = RatingsRecord()

var jobDetail : JobDetailRecord = JobDetailRecord()
var trackingDetail:TrackingDetails = TrackingDetails()
var Language_handler:Languagehandler=Languagehandler()
var register:Registration = Registration()
var check_KeyBoard:String = String()
var Currentlat : String = String()
var Currentlng : String = String()
var lastDriving : Double = Double()
var Bearing : Double = Double()
var backgroundTaskIdentifier: UIBackgroundTaskIdentifier?
var LocationTimer : Timer = Timer()
let signup:Signup=Signup()
let registerData:RegistrationPageDatas = RegistrationPageDatas()

var navtransition : CATransition
{
    let trans = CATransition()
    trans.duration = 0.5
    trans.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
    trans.type = kCATransitionPush
    return trans
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate ,CLLocationManagerDelegate, XMPPRosterDelegate, XMPPStreamDelegate{
    var theme:Theme=Theme()
    var window: UIWindow?
    let xmppStream = XMPPStream()
    let xmppRosterStorage = XMPPRosterCoreDataStorage()
    var xmppRoster: XMPPRoster
    var ConnectionTimer:Timer=Timer()
    let reachability = Reachability()
    var IsInternetconnected:Bool=Bool()
    var byreachable : String = String()
    let locationManager = CLLocationManager()

    override init() {
        xmppRoster = XMPPRoster.init(rosterStorage: xmppRosterStorage)
    }
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        self.ReachabilityListener()


        
        UIApplication.shared.isIdleTimerDisabled = true
        
        UserDefaults.standard.removeObject(forKey: "Availability")
        NotificationCenter.default.addObserver(self, selector: #selector(hitMethodWhenAppActive), name:NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(hitMethodWhenAppBackground), name:NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        // Override point for customization after application launch.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            locationManager.startMonitoringSignificantLocationChanges()
        }
        
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
                // Enable or disable features based on authorization.
            }
        } else {
            // Fallback on earlier versions
            
            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
            
        }
      
        UIApplication.shared.registerForRemoteNotifications()
        
        theme.saveLanguage("en")
        theme.SetLanguageToApp()
        
        NotificationCenter.default.addObserver(self, selector: #selector(AppDelegate.methodOfReceivedNotification(_:)), name:NSNotification.Name(rawValue: "didClickProfileBtnPostNotif"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AppDelegate.methodOfReceivedNotificationNetwork(_:)), name:NSNotification.Name(rawValue: kNoNetwork), object: nil)
        
        BITHockeyManager.shared().configure(withIdentifier: "4a6d6242b4654b6c8b68c7c558b3f8e4")
        BITHockeyManager.shared().start()
        BITHockeyManager.shared().authenticator.authenticateInstallation()
        //Api Key for Google Map
        GMSServices.provideAPIKey(googleApiKey)
        
        if(theme.isUserLigin()){
            setInitialViewcontroller()
            //self.setXmppConnect()
        }
        if launchOptions != nil{
            if theme.isUserLigin(){
                let localNotif = launchOptions![UIApplicationLaunchOptionsKey.remoteNotification]
                    as? Dictionary<NSObject,AnyObject>
                
                if localNotif != nil
                {
                    
                    let objUserRecs:UserInfoRecord=theme.GetUserDetails()
                    
                    let providerid = objUserRecs.providerId
                    let checkuserid=theme.CheckNullValue((localNotif as AnyObject).value(forKey: "tasker") as AnyObject)!
                    
                    if (providerid as String == checkuserid)
                    {
                        
                        let ChatMessage:NSArray = (localNotif as AnyObject).value(forKey: "messages") as! NSArray
                        
                        var Message_Notice:NSString=NSString()
                        var taskid:NSString=NSString()
                        var messageid : NSString = NSString()
                        
                        
                        let status : NSString = theme.CheckNullValue((localNotif as AnyObject).value(forKey: "status") as AnyObject)! as NSString
                        if status == "1"
                        {
                            
                            Message_Notice = (ChatMessage[0] as AnyObject).object(forKey: "message") as! NSString
                            taskid = theme.CheckNullValue((localNotif as AnyObject).value(forKey: "task") as AnyObject)! as NSString
                            messageid = (ChatMessage[0] as AnyObject).object(forKey: "_id") as! NSString
                            
                            let userid : NSString = theme.CheckNullValue((ChatMessage[0] as AnyObject).object(forKey: "from") as AnyObject)! as NSString
                            let user_status : NSString = theme.CheckNullValue((ChatMessage[0] as AnyObject).object(forKey: "user_status") as AnyObject)! as NSString
                            let delayTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                                NotificationCenter.default.post(name: Notification.Name(rawValue: "ReceivePushChat"), object: ChatMessage, userInfo: ["message":"\(Message_Notice)","from":"\(userid)","task":"\(taskid)","msgid":"\(messageid)","usersts" : user_status])
                                NotificationCenter.default.post(name: Notification.Name(rawValue: "ReceivePushChatToRootView"), object: ChatMessage, userInfo: ["message":"\(Message_Notice)","from":"\(userid)","task":"\(taskid)"])
                            }
                            
                        }
                        
                        
                    }
                    else{
                        let delayTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                        DispatchQueue.main.asyncAfter(deadline: delayTime) {
                            self.APNSNotification(localNotif! as NSDictionary)
                        }
                        
                    }
                }
            }
        }
        
        window?.backgroundColor=UIColor.white
        window?.makeKeyAndVisible()
        
        
        return true
    }
    func hitMethodWhenAppBackground() {
        
        self.locationManager.startMonitoringSignificantLocationChanges()
        
    }
    
    func hitMethodWhenAppActive() {
        
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.distanceFilter = kCLDistanceFilterNone
        self.locationManager.activityType = .automotiveNavigation
        if #available(iOS 9.0, *) {
            self.locationManager.allowsBackgroundLocationUpdates = true
        } else {
            // Fallback on earlier versions
        }
        self.locationManager.pausesLocationUpdatesAutomatically = false
        self.locationManager.startUpdatingLocation()
        
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                
                let alertView =  UNAlertView(title:Language_handler.VJLocalizedString("location_disable_title", comment: nil), message: Language_handler.VJLocalizedString("location_disable_message", comment: nil))
                alertView.addButton(kOk, action: {
                    
                })
                
                alertView.show()
                break
                
            case .authorizedAlways, .authorizedWhenInUse: break
                
            }
        } else {
            let alertView =  UNAlertView(title:Language_handler.VJLocalizedString("location_disable_title", comment: nil), message: Language_handler.VJLocalizedString("location_disable_message", comment: nil))
            alertView.addButton(kOk, action: {
                
            })
            alertView.show()
            
            
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        Currentlat="\(locValue.latitude)"
        Currentlng="\(locValue.longitude)"
        let user : CLLocation = CLLocation.init(latitude: trackingDetail.userLat, longitude: trackingDetail.userLong)
        let partner : CLLocation = CLLocation.init(latitude: locValue.latitude, longitude: locValue.longitude)
        Bearing = self.theme.getBearingBetweenTwoPoints1(user,locationB: partner)
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: kLocationNotification), object:nil,userInfo :nil)
    }
    
    func ReconnectMethod()
    {
        Thread.detachNewThreadSelector(#selector(EstablishCohnnection), toTarget: self, with: nil)
    }
 
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
      
        let direct = newHeading.trueHeading
        lastDriving = direct
        
        
    }
    func EstablishCohnnection()
    {
         if(SocketIOManager.sharedInstance.ChatSocket.status == .notConnected || SocketIOManager.sharedInstance.ChatSocket.status ==  .disconnected)
        {
            if(theme.isUserLigin())
            {
                //Listen To server side Chat related notification
                
                SocketIOManager.sharedInstance.establishChatConnection()
        
            }
        }
        
       if(SocketIOManager.sharedInstance.socket.status == .notConnected || SocketIOManager.sharedInstance.socket.status ==  .disconnected)
        {
            
            
            //Listen To server side Job related notification
            
            if(theme.isUserLigin())
            {
                SocketIOManager.sharedInstance.establishConnection()
            }
            
        }
        
    }

    

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        print("i am not available in simulator \(error)")
        
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenChars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
        var tokenString = ""
        
        for i in 0 ..< deviceToken.count {
            tokenString += String(format: "%02.2hhx", arguments: [tokenChars[i]])
        }
        
        if tokenString == ""
        {
            tokenString = "Simulator Signup"

        }
        
        theme.SaveDeviceTokenString(tokenString as NSString)
        print("tokenString: \(tokenString)")
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
        
        
        
        
        //  if userInfo.contains("tasker_status")
        
        let userInfoDict:NSDictionary?=userInfo as NSDictionary
        

        
        print("get userinformation=\(String(describing: userInfoDict))")
        let checkuserid = theme.CheckNullValue(userInfoDict!.object(forKey: "tasker") as AnyObject)!

        if userInfoDict != nil
        {
            
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            
            let providerid = objUserRecs.providerId
            
            if (providerid as String == checkuserid)
            {
                
                var Message_Notice:NSString=NSString()
                var taskid:NSString=NSString()
                var messageid : NSString = NSString()
                  var getuserid : NSString = NSString ()
                
                
                let status : NSString = theme.CheckNullValue(userInfoDict!.object(forKey: "status") as AnyObject)! as NSString
                if status == "1"
                {
                    let ChatMessage:NSArray = userInfoDict!.object(forKey: "messages") as! NSArray
                    
                    
                    Message_Notice = (ChatMessage[0] as AnyObject).object(forKey: "message") as! NSString
                    taskid = userInfoDict!.object(forKey: "task") as! NSString
                    messageid = (ChatMessage[0] as AnyObject).object(forKey: "_id") as! NSString
                    getuserid = self.theme.CheckNullValue(userInfoDict!.object(forKey: "user") as AnyObject)! as NSString
                    
                    let userid : NSString = theme.CheckNullValue((ChatMessage[0] as AnyObject).object(forKey: "from") as AnyObject)! as NSString
                    let user_status : NSString = theme.CheckNullValue((ChatMessage[0] as AnyObject).object(forKey: "user_status") as AnyObject)! as NSString
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "ReceivePushChat"), object: ChatMessage, userInfo: ["message":"\(Message_Notice)","from":"\(userid)","task":"\(taskid)","msgid":"\(messageid)","usersts" : user_status,"user_id":getuserid])
                    
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "ReceivePushChatToRootView"), object: ChatMessage, userInfo: ["message":"\(Message_Notice)","from":"\(userid)","task":"\(taskid)"])
                    
                }
                
                
            }
            else{
                
                APNSNotification(userInfo as NSDictionary)
                
                
            }
        }
        
        
    }
    
    func APNSNotification(_ dict : NSDictionary)
    {
      
        var Message_Notice:NSString=NSString()
        var Action:NSString = NSString()
        var key : NSString = NSString()
        var key0 : NSString = NSString()
        var key3 :NSString = NSString()
        var key4 : NSString = NSString()
        let Message:NSDictionary=dict
        
        
      
        
            Message_Notice=Message.object(forKey: "message") as! NSString
            Action=Message.object(forKey: "action")as! NSString
        key = theme.CheckNullValue(Message.object(forKey: "key1") as AnyObject)! as NSString
        
        key0 = theme.CheckNullValue(Message.object(forKey: "key0") as AnyObject)! as NSString
        
        key3 = theme.CheckNullValue(Message.object(forKey: "key3") as AnyObject)! as NSString
        key4 = theme.CheckNullValue(Message.object(forKey: "key4") as AnyObject)! as NSString
        
        
            
            
            
            if (Action == "job_request") || Action=="Accept_task" || Action=="Left_job"
            {
                NotificationCenter.default.post(name: Notification.Name(rawValue: kPushNotification), object:nil,userInfo :["message":"\(Message_Notice)","key":"\(key0)","action":"\(Action)"])
                
            }
                
            else if (Action == "job_cancelled")
            {
              NotificationCenter.default.post(name: Notification.Name(rawValue: kPushNotification), object:nil,userInfo :["message":"\(Message_Notice)","key":"\(key0)","action":"\(Action)"])
            }
                
            else if(Action == "admin_notification")
            {
                
            }
                else if (Action == "receive_cash" )
            {
                NotificationCenter.default.post(name: Notification.Name(rawValue: kPushNotification), object:nil,userInfo :["message":"\(Message_Notice)","key":"\(key)","action":"\(Action)" ,"price":"\(key3)" ,"currency":"\(key4)"])

            }
                
                
            else
            {
                
                let keyval: NSString = theme.CheckNullValue(Message.object(forKey: "key0") as AnyObject)! as NSString
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: kPushNotification), object:nil,userInfo :["message":"\(Message_Notice)","key":"\(keyval)","action":"\(Action)"])
                
            }
            
            
            
        }
        
        
        
    

    
    func  socketTypeNotification(_ dict:NSDictionary)  {
        
        let Userid:NSString = dict.object(forKey: "user") as! NSString
        
        //        let chatDict : NSDictionary = dict.objectForKey("chat") as! NSDictionary!
        //        let taskid : String = self.theme.CheckNullValue(chatDict.objectForKey("task"))!
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "ReceiveTypingMessage"), object: nil, userInfo: ["userid":"\(Userid)","taskid":""])
    }
    
    
    
    func socketStopTypeNotification(_ dict:NSDictionary)
    {    let Userid:NSString = dict.object(forKey: "user") as! NSString
        //        let chatDict : NSDictionary = dict.objectForKey("chat") as! NSDictionary!
        //        let taskid : String = self.theme.CheckNullValue(chatDict.objectForKey("task"))!
        NotificationCenter.default.post(name: Notification.Name(rawValue: "ReceiveStopTypingMessage"), object: nil, userInfo: ["userid":"\(Userid)","taskid":""])
    }
    
    func socketChatNotification(_ dict:NSDictionary)
    {
        
        let Message:NSDictionary = dict.object(forKey: "message") as! NSDictionary
        var Message_Notice:NSString=NSString()
        var taskid:NSString=NSString()
        var messageid : NSString = NSString()
        var dateStr : NSString = NSString()
        var getuserid : NSString = NSString ()


      
        let status : NSString = theme.CheckNullValue(Message.object(forKey: "status") as AnyObject)! as NSString
        if status == "1"
        {
            let ChatMessage:NSArray = Message.object(forKey: "messages") as! NSArray
          
            
            Message_Notice = (ChatMessage[0] as AnyObject).object(forKey: "message") as! NSString
            taskid = Message.object(forKey: "task") as! NSString
            getuserid = self.theme.CheckNullValue(Message.object(forKey: "user") as AnyObject)! as NSString

            messageid = (ChatMessage[0] as AnyObject).object(forKey: "_id") as! NSString
            dateStr = (ChatMessage[0] as AnyObject).object(forKey: "date") as! NSString

            
              let userid : NSString = theme.CheckNullValue((ChatMessage[0] as AnyObject).object(forKey: "from") as AnyObject)! as NSString
            let user_status : NSString = theme.CheckNullValue((ChatMessage[0] as AnyObject).object(forKey: "user_status") as AnyObject)! as NSString
            NotificationCenter.default.post(name: Notification.Name(rawValue: "ReceiveChat"), object: ChatMessage, userInfo: ["message":"\(Message_Notice)","from":"\(userid)","task":"\(taskid)","msgid":"\(messageid)","usersts" : user_status,"date" : dateStr,"user":getuserid])
            

            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "ReceiveChatToRootView"), object: nil, userInfo: ["message":"\(Message_Notice)","from":"\(userid)","task":"\(taskid)"])
            
        }
        

    }

    
    func socketNotification(_ dict:NSDictionary)
    {
        var messageArray:NSMutableDictionary=NSMutableDictionary()
        var Action:NSString = NSString()
        let Message:NSDictionary?=dict["message"] as? NSDictionary
        print("the user info is \(dict)...\(String(describing: Message))")
        
        
        if(Message != nil)
        {
            messageArray=(Message?.object(forKey: "message") as? NSMutableDictionary)!
            Action=(messageArray.object(forKey: "action") as? NSString)!
            
            
            
            if Action == "job_request" || Action=="Accept_task" || Action=="Left_job"
            {
                NotificationCenter.default.post(name: Notification.Name(rawValue: kPaymentPaidNotif), object: Message)
                
            }
                
            else if (Action == "job_cancelled")
            {
                NotificationCenter.default.post(name: Notification.Name(rawValue: kPaymentPaidNotif), object: Message)
                
            }
//            
//            if(Action=="job_request" || Action=="Accept_task" || Action=="Left_job"){
//                if let activeController = navigationController?.visibleViewController {
//                    if activeController.isKindOfClass(MyLeadsViewController){
//                        
//                        let alertView = UNAlertView(title: appNameJJ, message:(dictPartner.objectForKey("message") as? NSString)! as String)
//                        alertView.addButton("\(kOk)", action: {
//                            let jobId:NSString!=dictPartner.objectForKey("key0") as! NSString
//                            let objMyOrderVc = swapLang.storyboard.instantiateViewControllerWithIdentifier("MyOrderDetailOpenVCSID") as! MyOrderOpenDetailViewController
//                            objMyOrderVc.jobID=jobId
//                            objMyOrderVc.Getorderstatus = ""
//                            self.navigationController!.pushViewController(withFlip: objMyOrderVc, animated: true)
//                        })
//                        alertView.show()
//                        
//                        
//                    }
                
                
                
                
            else
            {
                
                /*let Order_id:NSString=messageArray.objectForKey("key0") as! NSString
                 NSNotificationCenter.defaultCenter().postNotificationName("ShowNotification", object: nil, userInfo: ["Message":"\(Message_Notice)","Order_id":"\(Order_id)"])*/
                NotificationCenter.default.post(name: Notification.Name(rawValue: kPaymentPaidNotif), object: Message)
                
                
            }
            
            
            
        }

        
    }

    
    func applicationWillResignActive(_ application: UIApplication) {
        
        ConnectionTimer.invalidate()
        ConnectionTimer = Timer()
        
        
        if(theme.isUserLigin())
        {
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            
            let providerid = objUserRecs.providerId
            
            
            SocketIOManager.sharedInstance.LeaveChatRoom(providerid as String)
            SocketIOManager.sharedInstance.LeaveRoom(providerid as String)
            SocketIOManager.sharedInstance.RemoveAllListener()
            
        }
        
       

        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        if(theme.isUserLigin()){
            let URL_Handler:URLhandler=URLhandler()
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            let param=["user_type":"provider","id":"\(objUserRecs.providerId)","mode":"unavailable"]
            URL_Handler.makeCall(UserAvailableUrl , param: param as NSDictionary, completionHandler: { (responseObject, error) -> () in
                
            })
        }
        disconnect()
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        ConnectionTimer.invalidate()
        ConnectionTimer = Timer()
        
        backgroundTaskIdentifier = UIApplication.shared.beginBackgroundTask(expirationHandler:
            {
                UIApplication.shared.endBackgroundTask(backgroundTaskIdentifier!)
        })
        
        self.locationManager.startMonitoringSignificantLocationChanges()
        LocationTimer.invalidate()
        LocationTimer = Timer()
        LocationTimer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(ReconnectLocationMethod), userInfo: nil, repeats: true)
        RunLoop.current.add(LocationTimer, forMode: RunLoopMode.commonModes)

        if(theme.isUserLigin())
        {
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            
            let providerid = objUserRecs.providerId
            
           
            SocketIOManager.sharedInstance.LeaveChatRoom(providerid as String)
            SocketIOManager.sharedInstance.LeaveRoom(providerid as String)
            SocketIOManager.sharedInstance.RemoveAllListener()
            
        }

        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    func ReconnectLocationMethod()
    {
        self.locationManager.startMonitoringSignificantLocationChanges()
        self.locationManager.startUpdatingLocation()
        locationManager.startUpdatingHeading()
        print("call hitted")
    }
    func applicationWillEnterForeground(_ applßication: UIApplication) {
        
     
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
     
         LocationTimer.invalidate()
      
        
        ConnectionTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(ReconnectMethod), userInfo: nil, repeats: true)
        
     
        
        
        if(theme.isUserLigin())
        {
            SocketIOManager.sharedInstance.establishConnection()
            
            SocketIOManager.sharedInstance.establishChatConnection()

            
            
            
            
            
        }
        
        
        
        
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        ConnectionTimer.invalidate()
        ConnectionTimer = Timer()
        LocationTimer.invalidate()

        if(theme.isUserLigin())
        {
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            
            let providerid = objUserRecs.providerId
            
            
            SocketIOManager.sharedInstance.LeaveChatRoom(providerid as String)
            SocketIOManager.sharedInstance.LeaveRoom(providerid as String)
            SocketIOManager.sharedInstance.RemoveAllListener()
            
        }

        
        
        
             //SocketIOManager.sharedInstance.closeConnection()
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    
    func methodOfReceivedNotification(_ notification: Notification){
        guard let url = notification.object else {
            return // or throw
        }
        let blob = url as! NSString // or as! Sting or as! Int
        if(blob .isEqual(to: "1")){
             MakeRootVc("HomeVCSID")
        }
        else if(blob .isEqual(to: "2")){
            MakeRootVc("MyJobsVcSegue")
        }
        else if(blob .isEqual(to: "3")){
            MakeRootVc("BankingDetailSegue")
        }
        else if(blob .isEqual(to: "4")){
            MakeRootVc("MyProfileSegue")
        }
        else if(blob .isEqual(to: "5")){
            MakeRootVc("InitialVCSEGUe")
            let objDict:NSDictionary=NSDictionary()
            theme.saveUserDetail(objDict)
        }
    }
    
    func MakeRootVc(_ ViewIdStr:NSString){
        let storyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
      let initialVc = self.window!.rootViewController as! UINavigationController
        let rootViewController:UIViewController = storyboard.instantiateViewController(withIdentifier: "\(ViewIdStr)") as UIViewController
       initialVc.navigationController?.pushViewController(withFlip: rootViewController, animated: true)
        
//        let sb: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let rootView:UIViewController = sb.instantiateViewControllerWithIdentifier("\(ViewIdStr)")
//        
//         self.window!.rootViewController = rootView
   
    }
    
    func setInitialViewcontroller(){
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let objLoginVc:DEMORootViewController = mainStoryboard.instantiateViewController(withIdentifier: "rootController") as! DEMORootViewController
        
        let navigationController: UINavigationController = UINavigationController(rootViewController: objLoginVc)
        
        self.window!.rootViewController = navigationController
        self.window!.backgroundColor = UIColor.white
        navigationController.setNavigationBarHidden(true, animated: true)
        self.window!.makeKeyAndVisible()
    }
    
    func setInitailLogOut(){
        disconnect()
        let sb: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let appDel: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let rootView: UINavigationController = sb.instantiateViewController(withIdentifier: "StarterNavSid" as String) as! UINavigationController
        UIView.transition(with: self.window!, duration: 0.2, options: UIViewAnimationOptions.curveEaseIn, animations: {
            appDel.window?.rootViewController=rootView
            }, completion: nil)
         //[loginController.navigationController setNavigationBarHidden:YES animated:YES];
    }
    
    
  
    
    func methodOfReceivedNotificationNetwork(_ notification: Notification){
        let navigationController = window?.rootViewController as? UINavigationController
        if let activeController = navigationController!.visibleViewController {
        
            let image = UIImage(named: "NoNetworkConn")
            activeController.view.makeToast(message:kErrorMsg, duration: 5, position:HRToastActivityPositionDefault as AnyObject, title: "Oops !!!!", image: image!)
        }
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
  /////////////// XMPP
    
    
    func setXmppConnect(){
        DDLog.add(DDTTYLogger.sharedInstance)
        setupStream()
    }
    
    func ReachabilityListener()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged),name: ReachabilityChangedNotification,object: reachability)
        do{
            try reachability?.startNotifier()
        }catch{
            print("could not start reachability notifier")
        }
        
    }
    func reachabilityChanged(note: NSNotification) {
        
        let reachability = note.object as! Reachability
        if reachability.isReachable {
            IsInternetconnected=true
            if reachability.isReachableViaWiFi {
                print("Reachable via WiFi")
                byreachable = "1"
            } else {
                print("Reachable via Cellular")
                byreachable = "2"
            }
        } else {
            IsInternetconnected=false
            print("Network not reachable")
            byreachable = ""
        }
    }
    
    //MARK: Private Methods
    fileprivate func setupStream() {
        //		xmppRoster = XMPPRoster(rosterStorage: xmppRosterStorage)
        xmppRoster.activate(xmppStream)
        xmppStream?.addDelegate(self, delegateQueue: DispatchQueue.main)
        xmppRoster.addDelegate(self, delegateQueue: DispatchQueue.main)
        xmppStream?.hostName=xmppHostName //192.168.1.150  //67.219.149.186
        xmppStream?.hostPort=5222
        
    }
    
    fileprivate func goOnline() {
        let presence = XMPPPresence()
        let domain = xmppStream?.myJID.domain
        
        print("the domain is is \(String(describing: domain)).....")
        
        let priority = DDXMLElement.element(withName: "priority", stringValue: "24") as! DDXMLElement
        presence?.addChild(priority)
        
        // 			let priority = DDXMLElement.elementWithName("192.168.1.148", stringValue: "5222") as! DDXMLElement
        //			presence.addChild(priority)
        xmppStream?.send(presence)
    }
    
    fileprivate func goOffline() {
        let presence = XMPPPresence(type: "unavailable")
        xmppStream?.send(presence)
    }
    
    func connect() -> Bool {
        
        
        if !(xmppStream?.isConnected())! {
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            
             var myPassword:String? = String()
            let jabberID:String? = "\(objUserRecs.providerId)@\(xmppJabberId)" //messaging.dectar.com // casp83
            if(theme.getJaberPassword() != nil)
            {
           myPassword = "\(theme.getJaberPassword()!)"
            }
            
            
           
            //            xmppStream.hostName="192.168.1.148"
            //            xmppStream.hostPort=5222
            print("the username is \(String(describing: jabberID)).....\(String(describing: jabberID))")
            if !(xmppStream?.isDisconnected())! {
                return true
            }
            if jabberID == nil && myPassword == nil {
                return false
            }
            xmppStream?.myJID = XMPPJID(string: jabberID!)
            //            let domain = xmppStream.myJID.domain
            do {
                try xmppStream?.connect(withTimeout: XMPPStreamTimeoutNone)
                print("Connection success")
                return true
            } catch {
                print("Something went wrong!")
                return false
            }
        } else {
            return true
        }
    }
    
    func disconnect() {
        goOffline()
        xmppStream?.disconnect()
    }
    
    //MARK: XMPP Delegates
    func xmppStreamDidConnect(_ sender: XMPPStream!) {
        
        do {
            try	xmppStream?.authenticate(withPassword: "\(theme.getJaberPassword()!)")
        } catch {
            print("Could not authenticate")
        }
    }
    
    func xmppStreamDidAuthenticate(_ sender: XMPPStream!) {
        goOnline()
    }
    
    func xmppStream(_ sender: XMPPStream!, didReceive iq: XMPPIQ!) -> Bool {
        print("Did receive IQ")
        return false
    }
    
    func xmppStream(_ sender: XMPPStream!, didReceive message: XMPPMessage!) {
        
        //        var parseError: NSErrorPointer? = nil
        //        var xmlDictionary: [NSObject : AnyObject] = XMLReader.dictionaryForXMLString(testXMLString, error: &parseError)
        
        //        var ParseError:NSErrorPointer?=nil
        //        var xmlDictionary:NSDictionary=
        print("Did receive message \(message)")
        var Job_id:NSString?=""

        
        var MessageString="\(message)"
        var errorStr:NSString=""
        // Parse the XML into a dictionary
        do
        {
            
            let xmlDictionary:NSDictionary = try XMLReader.dictionary(forXMLString: MessageString) as NSDictionary
            
            print("The parsed message is  \(xmlDictionary)")
            errorStr=((xmlDictionary.object(forKey: "message") as AnyObject).object(forKey: "type"))! as! NSString
            
            Job_id=(xmlDictionary.object(forKey: "message") as AnyObject).object(forKey: "jobid") as? NSString
            
            if(Job_id == nil)
            {
                MessageString=((xmlDictionary.object(forKey: "message") as AnyObject).object(forKey: "body") as AnyObject).object(forKey: "text") as! NSString as String

                
                let xmppRecMsg:NSString = MessageString.replacingOccurrences(of: "+", with: " ").removingPercentEncoding! as NSString
                
                
                let data:Data = xmppRecMsg.data(using: String.Encoding.utf8.rawValue)!
                var Dictionay:NSDictionary?=NSDictionary()
                do {
                    
                    Dictionay = try JSONSerialization.jsonObject(
                        
                        with: data,
                        
                        options: JSONSerialization.ReadingOptions.mutableContainers
                        
                        ) as? NSDictionary
                    
                    
                    if(Dictionay != nil && (Dictionay?.count)!>0)
                    {
                        
                        let Message_Notice:NSString?=Dictionay?.object(forKey: "message") as? NSString
                        let Action:NSString?=Dictionay?.object(forKey: "action") as? NSString
                        let alertView = UNAlertView(title: appNameJJ, message: "\(String(describing: Message_Notice))")
                        alertView.addButton(self.theme.setLang("ok"), action: {
                            
                        })
                        AudioServicesPlayAlertSound(1315);

                        alertView.show()

                        
                        if(Action! == "job_request")
                        {
                            NotificationCenter.default.post(name: Notification.Name(rawValue: kPaymentPaidNotif), object: Dictionay)
                        }
                        else  if(Action! == "receive_cash")          {
                            
                            NotificationCenter.default.post(name: Notification.Name(rawValue: kPaymentPaidNotif), object: Dictionay)
                            
                        }
                        else  if(Action! == "payment_paid")          {
                            
                            NotificationCenter.default.post(name: Notification.Name(rawValue: kPaymentPaidNotif), object: Dictionay)
                            
                        }
                        else  if(Action! == "job_cancelled")          {
                            NotificationCenter.default.post(name: Notification.Name(rawValue: kPaymentPaidNotif), object: Dictionay)
                        }else  if(Action! == "job_assigned")          {
                            NotificationCenter.default.post(name: Notification.Name(rawValue: kPaymentPaidNotif), object: Dictionay)
                        }
                        else{
                            let alertView = UNAlertView(title: appNameJJ, message: "\(String(describing: Message_Notice))")
                            alertView.addButton(self.theme.setLang("ok"), action: {
                                
                            })
                            AudioServicesPlayAlertSound(1315);

                            alertView.show()
                        }
                        
                        
                    }
                    else{
                        
                    }
                    

                    
                }
                    
                    
                catch let error as NSError {
                    
                    
                    // Catch fires here, with an NSErrro being thrown from the JSONObjectWithData method
                    print("A JSON parsing error occurred, here are the details:\n \(error)")
                    Dictionay=nil
                    
                }
                
                
                
                
                
             }
                else
            {
                
                if(errorStr=="cancel"){
                    print("The parsed message is  \(xmlDictionary).....\(MessageString)")
                }
                
              else  if(MessageString != "Type" && MessageString != "StopType" && errorStr == "chat")
            {
                MessageString=((xmlDictionary.object(forKey: "message") as AnyObject).object(forKey: "body") as AnyObject).object(forKey: "text") as! NSString as String
                
                if(errorStr=="error"){
                    print("The parsed message is  \(xmlDictionary).....\(MessageString)")
                }else{
                    MessageString=((xmlDictionary.object(forKey: "message") as AnyObject).object(forKey: "body") as AnyObject).object(forKey: "text") as! NSString as String
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: kChatFromOthers), object: MessageString)
                }
            }

            else if(errorStr == "Typing")
            {
                
                Job_id=((xmlDictionary.object(forKey: "message") as AnyObject).object(forKey: "jobid"))! as? NSString
                
                
                    
                MessageString=((xmlDictionary.object(forKey: "message") as AnyObject).object(forKey: "body") as AnyObject).object(forKey: "text") as! NSString as String
                    //   var Job_Id=xmlDictionary.objectForKey("message")?.objectForKey("jobid")! as! NSString
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: kTypeStatus), object: MessageString)
             }
            }

            
        }
        catch {
           
        }
        
        
        
    }

    func xmppStream(_ sender: XMPPStream!, didSend message: XMPPMessage!) {
        print("Did send message \(message)")
    }
    
    func xmppStream(_ sender: XMPPStream!, didReceive presence: XMPPPresence!) {
        let presenceType = presence.type()
        let myUsername = sender.myJID.user
        let presenceFromUser = presence.from().user
        
        if presenceFromUser != myUsername {
            print("Did receive presence from \(String(describing: presenceFromUser))")
            if presenceType == "available" {
            } else if presenceType == "unavailable" {
            }
        }
    }
    
    func xmppRoster(_ sender: XMPPRoster!, didReceiveRosterItem item: DDXMLElement!) {
        print("Did receive Roster item")
    }


}

