
//
//  URLhandler.swift
//  Plumbal
//
//  Created by Casperon Tech on 07/10/15.
//  Copyright © 2015 Casperon Tech. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration
import Alamofire


var Dictionay:NSDictionary!=NSDictionary()


class URLhandler: NSObject
{
     func isConnectedToNetwork() -> Bool {
        
        return (UIApplication.shared.delegate as! AppDelegate).IsInternetconnected
    }
    
    func makeCall(_ url: String,param:NSDictionary, completionHandler: @escaping (_ responseObject: NSDictionary?,_ error:NSError?  ) -> ())
    
    
    {
        var user_id:String=""
       
        if(theme.isUserLigin()){
            let objUserRecs:UserInfoRecord=theme.GetUserDetails()
            user_id=objUserRecs.providerId
          
        }
        if isConnectedToNetwork() == true {
            
            Alamofire.request("\(url)", method: .post, parameters: param as? Parameters, encoding: JSONEncoding.default, headers: ["apptype": "ios", "apptoken":"\(theme.GetDeviceToken())", "providerid":"\(user_id)"])
                .responseJSON { response in
                    
                    do {
                        
                        Dictionay = try JSONSerialization.jsonObject(
                            
                            with: response.data!,
                            
                            options: JSONSerialization.ReadingOptions.mutableContainers
                            
                            ) as! NSDictionary
                        
                        let status:NSString?=Dictionay.object(forKey: "is_dead") as? NSString
                        if(status == nil)
                        {
                            print("the dictionary is \(url)........>>>\(param)>>>>>.......\(Dictionay)")
                            
                            completionHandler(Dictionay as NSDictionary?, response.result.error as NSError? )
                            
                        }
                        else
                        {
                            print("the dictionary is \(url)........>>>\(param)>>>>>.......\(Dictionay)")
                            
                            let alertView = UNAlertView(title: appNameJJ, message: "\(Language_handler.VJLocalizedString("log_out1", comment: nil))\n \(Language_handler.VJLocalizedString("log_out2", comment: nil))")
                            alertView.addButton(kOk, action: {
                                let dict: [AnyHashable: Any] = [NSObject : AnyObject]()
                                
                                theme.saveUserDetail(dict as NSDictionary)
                                let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.setInitailLogOut()
                            })
                            alertView.show()
                            
                        }
                        
                    }
                        
                    catch let error as NSError {
                        print("the dictionary is \(param)....\(url)...\(Dictionay)")
                        Dictionay=nil
                        completionHandler(Dictionay as NSDictionary?, error )
                        print("A JSON parsing error occurred, here are the details:\n \(error)")
                    }
                    
                    
                    
                    Dictionay=nil
            }
                    
            
        } else {
            NotificationCenter.default.post(name: Notification.Name(rawValue: kNoNetwork), object: nil)
        }
    }
    
    
    
    func makeGetCall(_ url: NSString, completionHandler: @escaping (_ responseObject: NSDictionary? ) -> ())
    {
        Alamofire.request("\(url)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            
            if(response.result.error == nil)
            {
                
                do {
                    
                    let Dictionary = try JSONSerialization.jsonObject(
                        with: response.data!,
                        
                        options: JSONSerialization.ReadingOptions.mutableContainers
                        
                        ) as? NSDictionary
                    
                    completionHandler(Dictionary as NSDictionary?)

                    
                }
                catch let error as NSError {
                    
                    // Catch fires here, with an NSErrro being thrown from the JSONObjectWithData method
                    print("A JSON parsing error occurred, here are the details:\n \(error)")
                    completionHandler(nil)
                }
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
