//
//  Theme.swift
//  PlumberJJ
//
//  Created by Casperon Technologies on 11/17/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

import UIKit

extension String {
    func height(constraintedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.font = font
        label.sizeToFit()
        let attributeString : NSMutableAttributedString = NSMutableAttributedString(string: self)
        let style = NSMutableParagraphStyle()
        attributeString.addAttribute(NSParagraphStyleAttributeName, value: style, range: NSMakeRange(0, self.characters.count))
        attributeString.addAttribute(NSFontAttributeName, value: font as? UIFont, range: NSMakeRange(0, self.characters.count))
        label.attributedText = attributeString
        let size = label.sizeThatFits(label.frame.size)
        //label.text = self
        return size.height
    }
}

class Theme: NSObject {
     var url_handler : URLhandler = URLhandler()


    
        
    let  codename:NSArray=["Afghanistan(+93)", "Albania(+355)","Algeria(+213)","American Samoa(+1684)","Andorra(+376)","Angola(+244)","Anguilla(+1264)","Antarctica(+672)","Antigua and Barbuda(+1268)","Argentina(+54)","Armenia(+374)","Aruba(+297)","Australia(+61)","Austria(+43)","Azerbaijan(+994)","Bahamas(+1242)","Bahrain(+973)","Bangladesh(+880)","Barbados(+1246)","Belarus(+375)","Belgium(+32)","Belize(+501)","Benin(+229)","Bermuda(+1441)","Bhutan(+975)","Bolivia(+591)","Bosnia and Herzegovina(+387)","Botswana(+267)","Brazil(+55)","British Virgin Islands(+1284)","Brunei(+673)","Bulgaria(+359)","Burkina Faso(+226)","Burma (Myanmar)(+95)","Burundi(+257)","Cambodia(+855)","Cameroon(+237)","Canada(+1)","Cape Verde(+238)","Cayman Islands(+1345)","Central African Republic(+236)","Chad(+235)","Chile(+56)","China(+86)","Christmas Island(+61)","Cocos (Keeling) Islands(+61)","Colombia(+57)","Comoros(+269)","Cook Islands(+682)","Costa Rica(+506)","Croatia(+385)","Cuba(+53)","Cyprus(+357)","Czech Republic(+420)","Democratic Republic of the Congo(+243)","Denmark(+45)","Djibouti(+253)","Dominica(+1767)","Dominican Republic(+1809)","Ecuador(+593)","Egypt(+20)","El Salvador(+503)","Equatorial Guinea(+240)","Eritrea(+291)","Estonia(+372)","Ethiopia(+251)","Falkland Islands(+500)","Faroe Islands(+298)","Fiji(+679)","Finland(+358)","France (+33)","French Polynesia(+689)","Gabon(+241)","Gambia(+220)","Gaza Strip(+970)","Georgia(+995)","Germany(+49)","Ghana(+233)","Gibraltar(+350)","Greece(+30)","Greenland(+299)","Grenada(+1473)","Guam(+1671)","Guatemala(+502)","Guinea(+224)","Guinea-Bissau(+245)","Guyana(+592)","Haiti(+509)","Holy See (Vatican City)(+39)","Honduras(+504)","Hong Kong(+852)","Hungary(+36)","Iceland(+354)","India(+91)","Indonesia(+62)","Iran(+98)","Iraq(+964)","Ireland(+353)","Isle of Man(+44)","Israel(+972)","Italy(+39)","Ivory Coast(+225)","Jamaica(+1876)","Japan(+81)","Jordan(+962)","Kazakhstan(+7)","Kenya(+254)","Kiribati(+686)","Kosovo(+381)","Kuwait(+965)","Kyrgyzstan(+996)","Laos(+856)","Latvia(+371)","Lebanon(+961)","Lesotho(+266)","Liberia(+231)","Libya(+218)","Liechtenstein(+423)","Lithuania(+370)","Luxembourg(+352)","Macau(+853)","Macedonia(+389)","Madagascar(+261)","Malawi(+265)","Malaysia(+60)","Maldives(+960)","Mali(+223)","Malta(+356)","MarshallIslands(+692)","Mauritania(+222)","Mauritius(+230)","Mayotte(+262)","Mexico(+52)","Micronesia(+691)","Moldova(+373)","Monaco(+377)","Mongolia(+976)","Montenegro(+382)","Montserrat(+1664)","Morocco(+212)","Mozambique(+258)","Namibia(+264)","Nauru(+674)","Nepal(+977)","Netherlands(+31)","Netherlands Antilles(+599)","New Caledonia(+687)","New Zealand(+64)","Nicaragua(+505)","Niger(+227)","Nigeria(+234)","Niue(+683)","Norfolk Island(+672)","North Korea (+850)","Northern Mariana Islands(+1670)","Norway(+47)","Oman(+968)","Pakistan(+92)","Palau(+680)","Panama(+507)","Papua New Guinea(+675)","Paraguay(+595)","Peru(+51)","Philippines(+63)","Pitcairn Islands(+870)","Poland(+48)","Portugal(+351)","Puerto Rico(+1)","Qatar(+974)","Republic of the Congo(+242)","Romania(+40)","Russia(+7)","Rwanda(+250)","Saint Barthelemy(+590)","Saint Helena(+290)","Saint Kitts and Nevis(+1869)","Saint Lucia(+1758)","Saint Martin(+1599)","Saint Pierre and Miquelon(+508)","Saint Vincent and the Grenadines(+1784)","Samoa(+685)","San Marino(+378)","Sao Tome and Principe(+239)","Saudi Arabia(+966)","Senegal(+221)","Serbia(+381)","Seychelles(+248)","Sierra Leone(+232)","Singapore(+65)","Slovakia(+421)","Slovenia(+386)","Solomon Islands(+677)","Somalia(+252)","South Africa(+27)","South Korea(+82)","Spain(+34)","Sri Lanka(+94)","Sudan(+249)","Suriname(+597)","Swaziland(+268)","Sweden(+46)","Switzerland(+41)","Syria(+963)","Taiwan(+886)","Tajikistan(+992)","Tanzania(+255)","Thailand(+66)","Timor-Leste(+670)","Togo(+228)","Tokelau(+690)","Tonga(+676)","Trinidad and Tobago(+1868)","Tunisia(+216)","Turkey(+90)","Turkmenistan(+993)","Turks and Caicos Islands(+1649)","Tuvalu(+688)","Uganda(+256)","Ukraine(+380)","United Arab Emirates(+971)","United Kingdom(+44)","United States(+1)","Uruguay(+598)","US Virgin Islands(+1340)","Uzbekistan(+998)","Vanuatu(+678)","Venezuela(+58)","Vietnam(+84)","Wallis and Futuna(+681)","West Bank(970)","Yemen(+967)","Zambia(+260)","Zimbabwe(+263)"];
    let code:NSArray=["+93", "+355","+213","+1684","+376","+244","+1264","+672","+1268","+54","+374","+297","+61","+43","+994","+1242","+973","+880","+1246","+375","+32","+501","+229","+1441","+975","+591"," +387","+267","+55","+1284","+673","+359","+226","+95","+257","+855","+237","+1","+238","+1345","+236","+235","+56","+86","+61","+61","+57","+269","+682","+506","+385","+53","+357","+420","+243","+45","+253","+1767","+1809","+593","+20","+503","+240","+291","+372","+251"," +500","+298","+679","+358","+33","+689","+241","+220"," +970","+995","+49","+233","+350","+30","+299","+1473","+1671","+502","+224","+245","+592","+509","+39","+504","+852","+36","+354","+91","+62","+98","+964","+353","+44","+972","+39","+225","+1876","+81","+962","+7","+254","+686","+381","+965","+996","+856","+371","+961","+266","+231","+218","+423","+370","+352","+853","+389","+261","+265","+60","+960","+223","+356","+692","+222","+230","+262","+52","+691","+373","+377","+976","+382","+1664","+212","+258","+264","+674","+977","+31","+599","+687","+64","+505","+227","+234","+683","+672","+850","+1670","+47","+968","+92","+680","+507","+675","+595","+51","+63","+870","+48","+351","+1","+974","+242","+40","+7","+250","+590","+290","+1869","+1758","+1599","+508","+1784","+685","+378","+239","+966","+221","+381","+248","+232","+65","+421","+386","+677","+252","+27","+82","+34","+94","+249","+597","+268","+46","+41","+963","+886","+992","+255","+66","+670","+228","+690","+676","+1868","+216","+90","+993","+1649","+688","+256","+380","+971","+44","+1","+598","+1340","+998","+678","+58","+84","+681","+970","+967","+260","+263"];
    

    func getBearingBetweenTwoPoints1( _ A: CLLocation, locationB B: CLLocation) -> Double {
        var dlon: Double = self.toRad((B.coordinate.longitude - A.coordinate.longitude))
        let dPhi: Double = log(tan(self.toRad(B.coordinate.latitude) / 2 + .pi / 4) / tan(self.toRad(A.coordinate.latitude) / 2 + .pi / 4))
        if fabs(dlon) > .pi {
            dlon = (dlon > 0) ? (dlon - 2 * .pi) : (2 * .pi + dlon)
        }
        return self.toBearing(atan2(dlon, dPhi))
    }
    
    func toRad(_ degrees: Double) -> Double {
        return degrees * (.pi / 180)
    }
    
    func toBearing(_ radians: Double) -> Double {
        return self.toDegrees(radians) + 360.truncatingRemainder(dividingBy: 360)
    }
    
    func toDegrees(_ radians: Double) -> Double {
        return radians * 180 / .pi
    }

    func CheckNullValue(_ value : Any?) -> String? {
        var value = value
        var  pureStr:String=""
        if value is NSNull{
            return pureStr
        }
        else {
            if (value == nil){
                value="" as AnyObject
            }
            pureStr = String(describing: value! )
            return pureStr
        }
    }

    func setLang(_ str:String)->String {
        return Language_handler.VJLocalizedString(str, comment: nil)
    }
    func Lightgray()->UIColor{
        return UIColor(red:216.0/255.0, green:216.0/255.0 ,blue:216.0/255.0, alpha:1.0)
    }
    func appName() -> NSString{
        return "\(Appname)" as NSString
        
    }
    func ThemeColour()->UIColor
    {
        var Theme_Color:UIColor!=UIColor()
        Theme_Color=nil
        
        
        Theme_Color = UIColor(red:0.14, green:0.12, blue:0.13, alpha:0.8)
        
        return Theme_Color
        
    }

    func additionalThemeColour()->UIColor
    {
        var Theme_Color:UIColor!=UIColor()
        Theme_Color=nil
        
        
        Theme_Color = UIColor(red:0.14, green:0.12, blue:0.13, alpha:0.8)
        
        return Theme_Color
        
    }
    

    
    func removeRotation( _ image: UIImage) -> UIImage {
        if image.imageOrientation == .up {
            return image
        }
        UIGraphicsBeginImageContextWithOptions(image.size, false, image.scale)
    //[self drawInRect:(CGRect){0, 0, self.size}];
        image.draw(in: CGRect.init(origin:CGPoint(x: 10, y: 20), size:image.size ))

        let normalizedImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return normalizedImage
    }
    func isValidEmail(_ testStr:String) -> Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    func SaveDeviceTokenString(_ dToken:NSString) {
        let userDefaults = UserDefaults.standard
        
        let akey = "deviceTokenKey"
        userDefaults.setValue(dToken, forKey: akey)
        userDefaults.synchronize()
    }
    
    func getAddressForLatLng(_ latitude: String, longitude: String , status : String)->String {
        var fullAddress : NSString = NSString()
        let url = URL(string: "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(latitude),\(longitude)&key=\(googleApiKey)")
        print(url!)
        let data = try? Data(contentsOf: url!)
        if data != nil{
            let json = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
            if let result = json["results"] as? NSArray {
                if(result.count != 0){
                    if let address : NSArray = (result.object(at: 0) as AnyObject).value(forKey: "address_components") as? NSArray {
                        
                        print("get current location \(address)")
                        var street = ""
                        var city = ""
                        var locality = ""
                        var state = ""
                        var country = ""
                        var zipcode = ""
                        
                        let streetNameStr : NSMutableString = NSMutableString()
                        
                        for  item in address {
                            
                            let item1 = (item as AnyObject).value(forKey: "types") as! NSArray
                            
                            if((item1.object(at: 0) as! String == "street_number") || (item1.object(at: 0) as! String == "premise") || (item1.object(at: 0) as! String == "route")) {
                                let number1 = (item as AnyObject).value(forKey:"long_name")
                                streetNameStr.append("\(String(describing: number1))")
                                street = streetNameStr  as String
                                
                            }else if(item1.object(at: 0) as! String == "locality"){
                                let city1 = (item as AnyObject).value(forKey: "long_name")
                                city = city1 as! String
                                locality = ""
                            }else if(item1.object(at: 0) as! String == "administrative_area_level_2" || item1.object(at: 0) as! String == "political") {
                                let city1 = (item as AnyObject).value(forKey:"long_name")
                                locality = city1 as! String
                            }else if(item1.object(at: 0) as! String == "administrative_area_level_1" || item1.object(at: 0) as! String == "political") {
                                let city1 = (item as AnyObject).value(forKey: "long_name")
                                state = city1 as! String
                            }else if(item1.object(at: 0) as! String == "country")  {
                                let city1 = (item as AnyObject).value(forKey: "long_name")
                                country = city1 as! String
                            }else if(item1.object(at: 0) as! String == "postal_code" ) {
                                let city1 = (item as AnyObject).value(forKey: "long_name")
                                zipcode = city1 as! String
                            }
                            
                            if status == "short"
                            {
                                fullAddress = "\(street)$\(locality)" as NSString
                            }
                            else
                            {
                                 fullAddress = "\(street)$\(city)$\(locality)$\(state)$\(country)$\(zipcode)" as NSString
                                
                            }
                            
                           
                            

                            if let address = (result.object(at: 0) as AnyObject).value(forKey: "formatted_address") as? String{
                                return address
                            }else{
                                return fullAddress as String
                            }
                        }
                    }
                }
            }
        }
        return ""
    }
    
    
    
   
    func saveLanguage(_ str:NSString) {
        var str = str
        
      //  str = "en"
        if(str.isEqual(to: "ta"))      {
            str="ta"
        }
        if(str.isEqual(to: "en")) {
            str="en"
        }
        if(str.isEqual(to: "ar")) {
            str="ar"
        }
        UserDefaults.standard.set(str, forKey: "LanguageName")
        UserDefaults.standard.synchronize()
    }
    
    func getAppLanguage() -> String {
        if UserDefaults.standard.object(forKey: "LanguageName") as! String == "en"{
            
            return UserDefaults.standard.object(forKey: "LanguageName") as! String
        } else if UserDefaults.standard.object(forKey: "LanguageName") as! String == "ta" {
            return UserDefaults.standard.object(forKey: "LanguageName") as! String
        }
        else if UserDefaults.standard.object(forKey: "LanguageName") as! String == "ar" {
            return UserDefaults.standard.object(forKey: "LanguageName") as! String
        }
        else{
            return "en"
        }
    }
    func getAppLang() -> String {
        if UserDefaults.standard.object(forKey: "LanguageName") as! String == "en"{
            
            return "en"
        } else if UserDefaults.standard.object(forKey: "LanguageName") as! String == "ta" {
            return "ta"
        } else if UserDefaults.standard.object(forKey: "LanguageName") as! String == "ar" {
            return "ar"
        }
        else{
            return "en"
        }
    }

    
    func SetLanguageToApp(){
        let savedLang=UserDefaults.standard.object(forKey: "LanguageName") as! NSString
        if(savedLang == "ta") {
            Language_handler.setApplicationLanguage(Language_handler.TamilLanguageShortName)
        }
        if(savedLang == "en"){
            
            Language_handler.setApplicationLanguage(Language_handler.EnglishUSLanguageShortName)
        }
        if(savedLang == "ar"){
            
            Language_handler.setApplicationLanguage(Language_handler.ArabicLanguageShortname)
        }
    }
    
    func AlertView(_ title:String,Message:String,ButtonTitle:String){
        if(Message as String == kErrorMsg){
            RKDropdownAlert.title(appNameJJ, message: Message as String, backgroundColor: UIColor.white , textColor: UIColor.red)
            return
        }
        RKDropdownAlert.dismissAllAlert()
        RKDropdownAlert.title(appNameJJ, message: Message as String, backgroundColor: UIColor.white , textColor: PlumberThemeColor)
    }
    
    func SetPaddingView (_ text:UITextField) -> UIView
    {
        let paddingView = UIView(frame:CGRect(x: 0, y: 0, width: 20, height: 20))
        text.leftView=paddingView;
        text.leftViewMode = UITextFieldViewMode.always
        return paddingView
        
    }
    func GetDeviceToken()->NSString{
        
        let akey = "deviceTokenKey"
        let userDefaults = UserDefaults.standard
        let data0 = userDefaults.value(forKey: akey)
        let deviceTokenStr = data0 as! NSString!
        return CheckNullValue(deviceTokenStr)! as NSString
        
    }
    func saveUserDetail(_ userDict:NSDictionary) {
        let userDefaults = UserDefaults.standard
        
        let akey = "userInfoDictKey"
        userDefaults.set(userDict, forKey: akey)
        userDefaults.synchronize()
        if(userDict.count>0){
            saveUserImage(userDict.object(forKey: "provider_image") as! String as NSString)
            
            
        }
    }
    
    func saveHidestatus(_ status: String) {
        UserDefaults.standard.set(status, forKey: "dismiss")
        UserDefaults.standard.synchronize()
    }
    
    func getHideStatus() -> String {
        if UserDefaults.standard.object(forKey: "dismiss") != nil{
            return UserDefaults.standard.object(forKey: "dismiss") as! String
        }else{
            return ""
        }
    }


    func saveUserImage(_ imgStr:NSString){
        var imgStr = imgStr
        if(imgStr.length==0){
            imgStr=""
        }
        let userDefaults = UserDefaults.standard
        
        let akey = "userInfoImgDictKey"
        userDefaults.set(imgStr, forKey: akey)
        userDefaults.synchronize()
    }
    
    func GetUserImage()->NSString{
        
        let akey = "userInfoImgDictKey"
        let userDefaults = UserDefaults.standard
        var data0 = userDefaults.string(forKey: akey)
        if(data0==nil){
            data0=""
        }
        let userDict:String = data0!
        return userDict as NSString
    }
    
    func isUserLigin()->Bool{
        let akey = "userInfoDictKey"
        let userDefaults = UserDefaults.standard
        let data0 = userDefaults.dictionary(forKey: akey)
        if(data0==nil || data0?.count==0){
            return false
        }else{
            return true
            
        }
    }
    func GetUserDetails()->UserInfoRecord{
        
        let akey = "userInfoDictKey"
        let userDefaults = UserDefaults.standard
        let data0 = userDefaults.dictionary(forKey: akey)
        let userDict = data0 as NSDictionary!
        
       
        
        let objUserRec:UserInfoRecord=UserInfoRecord(prov_name:self.CheckNullValue(userDict?.object(forKey: "provider_name") as AnyObject)! , prov_ID: self.CheckNullValue(userDict?.object(forKey: "provider_id") as AnyObject)!, prov_Image: self.CheckNullValue(userDict?.object(forKey: "provider_image") as AnyObject)!, prov_Email: self.CheckNullValue(userDict?.object(forKey: "email") as AnyObject)!, prov_mob: self.CheckNullValue(userDict?.object(forKey: "email") as AnyObject)!)
        
        return objUserRec
    }
    func rotateImage(_ image: UIImage) -> UIImage {
        if (image.imageOrientation == UIImageOrientation.up ) {
            return image
        }
        UIGraphicsBeginImageContext(image.size)
        image.draw(in: CGRect(origin: CGPoint.zero, size: image.size))
        let copy = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return copy!
    }
    func UpdateAvailability (_ availblesta:String){
        
        let objUserRecs:UserInfoRecord=theme.GetUserDetails()
        let Param: Dictionary = ["tasker":"\(objUserRecs.providerId)","availability" :"\(availblesta)"]
        
        url_handler.makeCall(EnableAvailabilty, param: Param as NSDictionary) {
            (responseObject, error) -> () in
            
            
            if(error != nil)
            {
            }
            else
            {
                if(responseObject != nil && (responseObject?.count)!>0)
                {
                    let responseObject = responseObject!
                    let status:NSString = theme.CheckNullValue(responseObject.object(forKey: "status") as AnyObject)! as NSString
                    if(status == "1")
                    {
                        let resDict: NSDictionary = responseObject.object(forKey: "response") as! NSDictionary
                        let tasker_status : String = theme.CheckNullValue(resDict.object(forKey: "tasker_status") as AnyObject)!
                        
                        if (tasker_status == "1")
                        {
                        }
                        else
                        {
                        }
                        
                    }
                    else
                    {
                    }
                    
                    
                }
            }
        }
        
        
        
    }
    func getCurrencyCode(_ code:String)->String{
        
        
//        let localeComponents = [NSLocaleCurrencyCode: code]
//        let localeIdentifier = NSLocale.localeIdentifierFromComponents(localeComponents)
//        let locale = NSLocale(localeIdentifier: localeIdentifier)
//        var currencySymbol = locale.objectForKey(NSLocaleCurrencySymbol) as! String
////        
////
////        
////        
//        var fmtr = NSNumberFormatter()
////        var locale = self.findLocaleByCurrencyCode(currencyCode)
////        var currencySymbol = ""
////        if locale {
////
////        }
//        fmtr.locale = locale
//        fmtr.numberStyle = .CurrencyStyle
//        currencySymbol = fmtr.currencySymbol!
//        if currencySymbol.count > 1 {
//            //currencySymbol = currencySymbol.substringToIndex(currencySymbol.characters.last)
//            currencySymbol = currencySymbol.substringFromIndex(currencySymbol.startIndex.advancedBy(currencySymbol.count - 1))
//
//        }
//        return currencySymbol
        
        
        
        let currencyCode = code
        
        let currencySymbols  = Locale.availableIdentifiers
            .map { Locale(identifier: $0) }
            .filter {
                if let localeCurrencyCode = ($0 as NSLocale).object(forKey: NSLocale.Key.currencyCode) as? String {
                    return localeCurrencyCode == currencyCode
                } else {
                    return false
                }
            }
            .map {
                ($0.identifier, ($0 as NSLocale).object(forKey: NSLocale.Key.currencySymbol)!)
                
                
        }
        
        print( currencySymbols.last?.1)
        return CheckNullValue((currencySymbols.first?.1 as AnyObject))!

    }
    func saveJaberPassword(_ JaberPassword: String) {
        UserDefaults.standard.set(JaberPassword, forKey: "JaberPassword")
        UserDefaults.standard.synchronize()
    }
    
    func getJaberPassword() -> String? {
        return (UserDefaults.standard.object(forKey: "JaberPassword") as? String)
        
    }
    
    func getAppName() -> String {
        return Bundle.main.infoDictionary!["CFBundleName"] as! String

        
    }
    func convertIntToString(_ integer : Int) -> NSString {
        var str:NSString = NSString()
        str = "\(integer)" as NSString
        return str
    }
    
    func saveAvailable_satus(_ Available : String) {
        UserDefaults.standard.set(Available, forKey: "Availability")
        UserDefaults.standard.synchronize()

    }
    
    func getAvailable_satus() -> String {
        
        if UserDefaults.standard.object(forKey: "Availability") != nil
        {
        return (UserDefaults.standard.object(forKey: "Availability") as? String)!
        }
        else
        {
            return ""
        }
    }
    
    
    func saveappCurrencycode(_ code: String) {
        UserDefaults.standard.set(code, forKey: "currencyCode")
        UserDefaults.standard.synchronize()
    }
    func getappCurrencycode() -> String {
        
        if UserDefaults.standard.object(forKey: "currencyCode") != nil
        {
            return UserDefaults.standard.object(forKey: "currencyCode") as! String
        }
        else
        {
            return ""
        }
    }


    func MakeAnimation(view : CSAnimationView, animation_type : String)
    {
        view.type = animation_type
        view.duration = 0.5
        view.delay = 0
        view.startCanvasAnimation()
    }

}

//    extension UINavigationController
//    {
//        func pushViewController(withFlip controller: UIViewController, animated : Bool) {
//            UIView.animate(withDuration: 0.50, animations: {() -> Void in
//                if(animated)
//                {
//                    UIView.setAnimationCurve(.easeInOut)
//                    self.pushViewController(controller, animated: false)
//                    UIView.setAnimationTransition(.flipFromRight, for: self.view, cache: false)
//                }
//                else
//                {
//                    self.pushViewController(controller, animated: false)
//                }
//
//            })
//        }
//        func popViewControllerWithFlip(animated : Bool) {
//            UIView.animate(withDuration: 0.50, animations: {() -> Void in
//                if(animated)
//                {
//                    UIView.setAnimationCurve(.easeInOut)
//                    self.popViewController(animated: false)
//                    UIView.setAnimationTransition(.flipFromLeft, for: self.view, cache: false)
//                }
//                else
//                {
//                    self.popViewController(animated: false)
//                }
//            })
//        }
//    }

extension UINavigationController
{
    func pushViewController(withFlip controller: UIViewController, animated : Bool) {
       self.addPushAnimation(controller: controller, animated: animated)
    }
    func popViewControllerWithFlip(animated : Bool) {
        self.addPopAnimation(animated: animated)
    }
    
    func poptoViewControllerWithFlip(controller : UIViewController, animated : Bool) {
        self.addPopToViewAnimation(controller: controller, animated: animated)
    }
    
    func perfromSegueWithFlip(controller : UIViewController, animated : Bool)
    {
        self.addPushAnimation(controller: controller, animated: animated)
    }
    
    func addPushAnimation(controller : UIViewController, animated : Bool)
    {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromBottom;
        self.view.layer.add(transition, forKey: kCATransition)
        self.pushViewController(controller, animated: animated)
    }
    
    func addPopAnimation(animated : Bool) {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromBottom
        self.view.layer.add(transition, forKey:kCATransition)
        self.popViewController(animated: animated)
    }
    
    func addPopToViewAnimation(controller : UIViewController, animated : Bool) {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromBottom
        self.view.layer.add(transition, forKey:kCATransition)
        self.popToViewController(controller, animated: animated)
    }
 
    
}



extension UIButton
{
    func buttonBouncing(){
self.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
    UIView.animate(withDuration: 2.0,delay: 0,usingSpringWithDamping: 0.2,initialSpringVelocity: 6.0,options: .allowUserInteraction,animations: {
        [weak self] in
                    self?.transform = .identity
        },completion: nil)
    }
}

extension UITableView {
    
    func reload() {
        self.reloadData()
        let cells = self.visibleCells
        let indexPaths = self.indexPathsForVisibleRows
        let size = UIScreen.main.bounds.size
        for i in cells {
            let cell = self.cellForRow(at: indexPaths![cells.index(of: i)!])
            cell?.transform = CGAffineTransform(translationX: -size.width, y: 0)
        }
        var index = 0
        for a in cells {
            let cell = self.cellForRow(at: indexPaths![cells.index(of: a)!])
            UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: UIViewAnimationOptions(), animations: {
                cell?.transform = CGAffineTransform(translationX: 0, y: 0)
            }, completion: nil)
            
            
            
            index += 1
        }
    }
    
    func reloadTable(){
        self.reloadData()
        let cells = self.visibleCells
        let indexPaths = self.indexPathsForVisibleRows
        let size = UIScreen.main.bounds.size
        for i in cells {
            let cell = self.cellForRow(at: indexPaths![cells.index(of: i)!])
            cell?.transform = CGAffineTransform(translationX: -size.width, y: 0)
        }
        var index = 0
        for a in cells {
            let cell = self.cellForRow(at: indexPaths![cells.index(of: a)!])
                        cell?.layer.transform = CATransform3DMakeScale(0.1,0.1,1)
                        UIView.animate(withDuration: 0.3, animations: {
                            cell?.layer.transform = CATransform3DMakeScale(1.05,1.05,1)
                        },completion: { finished in
                            UIView.animate(withDuration: 0.1, animations: {
                                cell?.layer.transform = CATransform3DMakeScale(1,1,1)
                            })
                        })
            
            index += 1
        }
    }
    
    
    
}

extension UIView
{
    func SpringAnimations(){
        self.transform = CGAffineTransform(scaleX: 0, y: 0);
        UIView.animate(withDuration: 1.5, delay: 0.05, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
            self.transform = CGAffineTransform(scaleX: 1, y: 1);
        }) { (finished) in
            print("animation completed")
        }
}
}
extension UILabel
{
    func Spring(){
        self.transform = CGAffineTransform(scaleX: 0, y: 0);
        UIView.animate(withDuration: 1.5, delay: 0.05, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
            self.transform = CGAffineTransform(scaleX: 1, y: 1);
        }) { (finished) in
            print("animation completed")
        }
    }
}


